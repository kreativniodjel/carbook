CARBOOK.json = {
	login          : CARBOOK.site_url + '/json/login',
	register       : CARBOOK.site_url + '/json/register',
	addvehicle     : CARBOOK.site_url + '/json/addvehicle',
	sellvehicle    : CARBOOK.site_url + '/json/sellvehicle',
	addcost        : CARBOOK.site_url + '/json/addcost',
	getcost        : CARBOOK.site_url + '/json/getcost',
	editcost       : CARBOOK.site_url + '/json/editcost',
	changepassword : CARBOOK.site_url + '/json/changepassword'
}

/*
CARBOOK.partials = {
	floater_user          : CARBOOK.site_url + '/partials/floater_user',
	floater_vehicle       : CARBOOK.site_url + '/partials/floater_vehicle',
	floater_vehicle_ex    : CARBOOK.site_url + '/partials/floater_vehicle_ex',
	floater_vehicle_photo : CARBOOK.site_url + '/partials/floater_vehicle_photo',
	floater_costs_note    : CARBOOK.site_url + '/partials/floater_costs_note',
	floater_costs_fuel    : CARBOOK.site_url + '/partials/floater_costs_fuel',
	modal_login           : CARBOOK.site_url + '/partials/modal_login',
	modal_register        : CARBOOK.site_url + '/partials/modal_register',
	modal_vehicle_add     : CARBOOK.site_url + '/partials/modal_vehicle_add',
	modal_costs_add       : CARBOOK.site_url + '/partials/modal_costs_add',
	modal_change_password : CARBOOK.site_url + '/partials/modal_change_password'
}
*/

require([
	'modules/floater-user',
	'modules/floater-vehicle',
	'modules/floater-vehicle-ex',
	'modules/floater-vehicle-photo',
	'modules/floater-cost',
	'modules/floater-cost-note',
	'modules/floater-cost-alarm',
	'modules/modal-login',
	'modules/modal-register',
	'modules/modal-vehicle-add',
	'modules/modal-vehicle-sell',
	'modules/modal-cost',
	'modules/modal-change-password',
	'modules/vehicle-gallery'
	], function() {
		$('#templates_placeholder').css('opacity','1');
});