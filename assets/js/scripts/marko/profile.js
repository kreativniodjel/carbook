/* 
 * Script for profile  
 * 
 * Date created: 14.10.2012.
 * Created by Marko Vučak
 * 
 */
$(document).ready(function()
{
     $( "#birthday" ).datepicker({
        changeDay: true,
        changeMonth: true,
        changeYear: true,
        showButtonPanel: false
    });
    
     $( "#valid_thru" ).datepicker({
        changeDay: true,
        changeMonth: true,
        changeYear: true,
        showButtonPanel: false
    });
   
     $("#dialog-change-psw").dialog({autoOpen : false, modal : true, show : "blind", hide : "blind"});
     $("#dialog-change-psw").dialog( "option", "buttons", [
            {
                text: "POTVRDI",
                id: 'dlg_submit_1',
                click: function() 
                { 
                    $(this).dialog("close"); 
                    $("#form_dlg_change_psw").submit();
                }
            },
            {
                text: "ODUSTANI",
                click: function() 
                { 
                    $(this).dialog("close"); 
                }
            }
        ] );
    $("#submit_psw_profile").click(function()
    {
       $("#dialog-change-psw").dialog("open"); 
    });
   
    $('#dlg_submit_1').button('disable');
    
    $('#old_password').change(function()
    {
         $('#dlg_submit_1').button('enable');
    });
       
   /****************************************************************************/
   // NOTIFY
   var show = $('#show_message').length > 0 ? $('#show_message').val() : "";
   if( show.length > 0 )
    {
        var type    = 0;
        var message = '';
        switch(show)
        {
            case '-3':
                message = 'POGREŠKA! Niste uspiješno ponovili novu lozinku. Lozinka nije promijenjena.';
                break;
            case '-2':
                message = 'POGREŠKA! Došlo je do pogreške prilikom izmjene lozinke. Lozinka nije promijenjena.';
            case '-1':
                message = 'POGREŠKA! Lozinka nije promijenjena jer ste upisali netočnu trenutnu lozinku!';
                break;
            case '0':
                message = 'POGREŠKA! Pogreška prilikom promijene lozinke. Ukoliko se problem ponovi kontaktirajte administratora.';
                break;
            case '1':
                type = 1;
                message = 'Lozinka uspiješno promijenjena!';
                break;

                break;
        }
        if(type == 0)
        {
            jError(message,
            {
                autoHide : false, // added in v2.0
                clickOverlay : true, // added in v2.0
                MinWidth : 250,
                TimeShown : 3000,
                ShowTimeEffect : 200,
                HideTimeEffect : 200,
                LongTrip :20,
                HorizontalPosition : 'center',
                VerticalPosition : 'top',
                ShowOverlay : true,
                ColorOverlay : '#000',
                OpacityOverlay : 0.3,
                onClosed : function()
                { 
                },
                onCompleted : function()
                { 
                }
            });
        }
        else
        {
            jSuccess(message,
            {
		  autoHide : false, // added in v2.0
		  clickOverlay : true, // added in v2.0
		  MinWidth : 250,
		  TimeShown : 3000,
		  ShowTimeEffect : 200,
		  HideTimeEffect : 200,
		  LongTrip :20,
		  HorizontalPosition : 'center',
		  VerticalPosition : 'top',
		  ShowOverlay : true,
   		  ColorOverlay : '#000',
		  OpacityOverlay : 0.3,
		  onClosed : function()
                  { 
		  },
		  onCompleted : function()
                  { 
		  }
            });
        }
    }
});

