/*
 * Script for costs Table manipulation
 *
 * Date created: 29.09.2012.
 * Created by Marko Vučak
 **/
$(document).ready(function(){
    
    /**********************************************************************************************************************************************************/
    /* Dialog on vehicles screen */
    $("#dialog").dialog({autoOpen : false, modal : true, show : "blind", hide : "blind"}); 
    $('tr').find("#popup-on-1,#popup-on-2,#popup-on-3,#popup-on-4,#popup-on-5,#popup-on-6,#popup-on-7,#popup-on-8,#popup-on-9,#popup-on-10").
        click( function() { 
            var veh_name = $(this).parents('tr').attr('data-name');
            var id_value = $(this).parents('tr').attr('data-clickable-costs');
            var id_details = $(this).parents('tr').attr('data-costs-details');
            
            $('#nameLabel').text(veh_name);
            $('#idcost').text(id_value);
            $('#iddetails').text(id_details);
            $("#dialog").dialog("open"); 
        });
   
        
    $(".dialog-button").click(function(){
       var id_button = $(this).attr('id'); 
       var location  = "edit";
       if(id_button == 2)
           location  = "delete";
           
       $("#dialog").dialog("close");
       var id_value = $("#idcost").text();
       var id_details = $("#iddetails").text();
       
       varurl = window.location.pathname +"/"+location+"/"+id_value+"/"+id_details;
   
       if(id_button == 2)
           {
            if(confirm('Jeste sigurni da želite obrisati trošak?'))
                window.location.replace(varurl);
           }
       else
           window.location.replace(varurl);
    });
   
});         

