$(document).ready(function()
{
    /* Registration confirmation dialog */
    var $dialog = $('<div></div>')
    .html('Poslan Vam je mail za potvrdu registracije. Slijedite upute u e-mailu!')
    .dialog({
            autoOpen: false,
            title: 'Potvrda registracije'
            } 
        );
    $dialog.dialog( "option", "buttons", [
            {
                text: "Zatvori",
                click: function() { 
                    $(this).dialog("close"); 
                    $("#my_form").submit();}
            }
        ] );

    /* Open dialog when mydiv is shown*/
    if ( $('#myDiv')[0] )  
    {
            $dialog.dialog('open');
            // prevent the default action, e.g., following a link
            return false;
    };
}); 


