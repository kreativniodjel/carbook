/*
 * Script for vehicles Table manipulation
 *
 * Date created: 27.09.2012.
 * Created by Marko Vučak
 * 
 **/
$(document).ready(function(){
    
    /**********************************************************************************************************************************************************/
    /* Dialog on vehicles screen */
    $("#dialog").dialog({autoOpen : false, modal : true, show : "blind", hide : "blind"}); 
    $("#dialog-sell").dialog({autoOpen : false, modal : true, show : "blind", hide : "blind"}); 
    $('tr').find("#popup-on-1,#popup-on-2,#popup-on-3,#popup-on-4,#popup-on-5,#popup-on-6").click( function() { 
            // get values about vehicles
            var veh_name        = $(this).parents('tr').attr('data-name');
            var id_value        = $(this).parents('tr').attr('data-clickable-vehicle');
            var veh_brand       = $(this).parents('tr').attr('data-brand');
            var veh_model       = $(this).parents('tr').attr('data-model');
            var veh_year        = $(this).parents('tr').attr('data-year');
            var veh_km          = $(this).parents('tr').attr('data-km');
            var veh_gasoline    = $(this).parents('tr').attr('data-gasoline');
            var veh_diesel      = $(this).parents('tr').attr('data-diesel');
            var veh_gas         = $(this).parents('tr').attr('data-gas');
            var veh_electricity = $(this).parents('tr').attr('data-electricity');
            var dialog_to_open  = $(this).parents('tr').attr('data-veh');
            
            //- dialog options
            $('#nameLabel').text(veh_name);
            $('#idveh').text(id_value);
            //- dialog sell
            $('#nameLabel_sell').text(veh_name);
            $('#idveh_sell').text(id_value);
            $('#brand').val(veh_brand);
            $('#model').val(veh_model);
            $('#year').val(veh_year);
            $('#km').val(veh_km);
            
            var fuel = '';
            if(veh_gasoline == true)
                fuel = fuel + 'BENZIN ';
            if(veh_diesel == true)
                fuel += 'DIZEL ';
            if(veh_gas == true)
                fuel += 'PLIN ';
            if(veh_electricity == true)
                fuel += 'STRUJA ';
            $('#fuel').val(fuel);
            if(dialog_to_open == 'ex')
            {
                $('#1').hide();
                $('#3').hide();
                $("#deltype").text("1");
            }
            else
            {
                $('#1').show();
                $('#3').show();
                $("#deltype").text("0");
            }
            $("#dialog").dialog("open"); 
        });
           
    // clicked button on dialog     
    $(".dialog-button").click(function()
    {
       var id_button    = $(this).attr('id'); 
       var id_value     = $("#idveh").text();
       var delete_type  = $("#deltype").text();
       
       //close dialog!
       $("#dialog").dialog("close");
      
       var location  = "sell";
       //- if we sell wehicle then show sell dialog!
       if(id_button == 1)
       {
           $("#dialog-sell").dialog("open");
           return;
       }
       else if(id_button == 2)
       {
            if(confirm('Jeste sigurni da želite obrisati vozilo?'))
            {
                location  = "delete"+"/"+id_value+"/"+delete_type;
            }
            else
            {
                return;
            }
       }
       else
           location  = "switch_to_ex"+"/"+id_value;
        
       varurl = window.location.pathname +"/"+location;
       window.location.replace(varurl);
    });
    
    // clicked button on SELL dialog     
    $(".dialog-sell-button").click(function()
    {
       // get values
       var id_button    = $(this).attr('id'); 
       var id_value     = $("#idveh").text();
       var price        = $("#price").val();
       //close dialog!
       $("#dialog-sell").dialog("close");
      
       var location  = "sell";
       if(id_button == 1) // if cancel just return
       {   
           return;
       }
       //- if we confirm sell wehicle then sell dialog! :)
       else if(id_button == 2)
       {
           location     = "sell";
           varurl       = window.location.pathname +"/"+location+"/"+id_value+"/"+price;
           window.location.replace(varurl);
       }
    });
});         