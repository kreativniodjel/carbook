/* 
 * Script for gallery 
 * 
 * Date created: 30.09.2012.
 * Created by Marko Vučak
 * 
 */
$(document).ready(function()
{
    $("#dialog_gallery").dialog({autoOpen : false, modal : true, width : 960, height : 700});
    $("#dialog_add_picture").dialog({autoOpen : false, modal : true, show : "blind", hide : "blind"});
    $("#dialog_edit_picture").dialog({autoOpen : false, modal : true, show : "blind", hide : "blind"});
    
    /************************************************************************/
    /* ADD IMAGE */
    // On new_image show browse file dialog
    $('div').find("#new_image").click( function() 
    { 
        jQuery('#file').click();
    });
   
    // when file is selected in browse file dialog add it!
    $("input:file").change(function ()
    {
        var fileName = $(this).val();
        $("#dialog_add_picture").dialog("open");
    });
    /************************************************************************/ 
    // clicked button on ADD PICTURE dialog     
    $(".dialog-pic-button").click(function()
    {
       var id_button = $(this).attr('id'); 
       
       //close dialog!
       var name = $("#name_dlg_add").val();
       var desc = $("#desc_dlg_add").val();
       
       $("#file_add_name").val(name);
       $("#file_add_desc").val(desc);
       $("#dialog_add_picture").dialog("close");
      
       //- if clicked cancel
       if(id_button == 1)
       {
           return;
       }
       else 
       {
           $('#file_form').submit();    
       }
    }); 
    
    
    $(".btn_pic_edit").click(function()
    {
       var pciid = $(this).parents('div').attr('data-pciid');
       var name = $(this).parents('div').attr('data-picname');
       var desc = $(this).parents('div').attr('data-picdes');
       $("#picid_edit").val(pciid);
       $("#name_edit").val(name);
       $("#desc_edit").val(desc);
       
       
       $("#dialog_edit_picture").dialog("open");
    });
    
    // SHOW DIALOG GALLERY WITH PICTURES GALLERY
    $(".image_click").click(function()
    {
        var image   = $(this).attr('src');
        var id      = $(this).attr('data-id');
        var name    = $(this).attr('data-name');
        var desc    = $(this).attr('data-desc');
        
        $(".dlg_gallery_image").attr("src", image);
        $(".dlg_gallery_image").attr("data-id", id);
        $(".gallery_dlg_desc").attr("data-id", id);
        $('#par_gallery_dlg_desc').text(desc);
        
        hide_show_btns(id);
        $( "#dialog_gallery" ).dialog( "option", "title", name);
        $( "#dialog_gallery" ).dialog( "option", "resizable", false ); 
        $( "#dialog_gallery" ).dialog( "open" );
    });
    
    // BUTTON LEFT PRESSED ON DIALOG GALLERY
    $(".gallery_dlg_btn_left").click(function()
    {
        var atribut = 'data-';
        var curpos = parseInt($(".dlg_gallery_image").attr("data-id"));
        var newpos = curpos - 1;
        var new_attribut = atribut+newpos;
        // find if there is a pic before current
        if (($('#all_gallery_data').attr(new_attribut)) != undefined)
        {
            var image   = $('#all_gallery_data').attr(new_attribut);
            var name   = $('#all_gallery_data').attr('data-name-'+newpos);
            var desc   = $('#all_gallery_data').attr('data-desc-'+newpos);
            $('#par_gallery_dlg_desc').text(desc);
            $( "#dialog_gallery" ).dialog( "option", "title", name);
            $(".dlg_gallery_image").attr("data-id", newpos);
            $(".dlg_gallery_image").attr("src", image);
        }   
        hide_show_btns(newpos);
    });
    // BUTTON RIGHT PRESSED ON DIALOG GALLERY
    $(".gallery_dlg_btn_right").click(function()
    {
        var atribut = 'data-';
        var curpos = parseInt($(".dlg_gallery_image").attr("data-id"));
        var newpos = curpos + 1;
        var new_attribut = atribut+newpos;
        // find if there is a pic before current
        if (($('#all_gallery_data').attr(new_attribut)) != undefined)
        {
            var image   = $('#all_gallery_data').attr(new_attribut);
            var name   = $('#all_gallery_data').attr('data-name-'+newpos);
            var desc   = $('#all_gallery_data').attr('data-desc-'+newpos);
            $('#par_gallery_dlg_desc').text(desc);
            $( "#dialog_gallery" ).dialog( "option", "title", name);
            $(".dlg_gallery_image").attr("data-id", newpos);
            $(".dlg_gallery_image").attr("src", image);
        }
        
        hide_show_btns(newpos);
    });
    
    function hide_show_btns(pos)
    {
        pos = parseInt(pos);
        var check_attribute = 'data-'+(pos+1);
        if (($('#all_gallery_data').attr(check_attribute)) == undefined)
            $('.gallery_dlg_btn_right').hide();
        else
            $('.gallery_dlg_btn_right').show();
        
        check_attribute = 'data-'+(pos-1);
        if (($('#all_gallery_data').attr(check_attribute)) == undefined)
            $('.gallery_dlg_btn_left').hide();
        else
            $('.gallery_dlg_btn_left').show();
    }
});

