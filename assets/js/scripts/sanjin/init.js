
/*
 * Datepicker helpers
 *
*/

function update_calendar_fields(date_text, $date_field, $year_field) {
    if (date_text==='' || date_text===undefined) return;

    var date_parsed = date_parts(date_text),
        date = date_parsed.day + '.' + date_parsed.month + '.',
        year = date_parsed.year;

    $date_field.val(date);
    $year_field.val(year);
}

function position_datepicker(width, top, left) {
    setTimeout(function(){
        $('#ui-datepicker-div').css({
            width : width,
            top   : top,
            left  : left
        });
    },0);
}

function date_parts(date_text) {
    var date_parts = date_text.split("-");
    return {
        day   : date_parts[2],
        month : date_parts[1],
        year  : date_parts[0]
    }
}


$(document).ready(function(){


    $('html').addClass( getVendorPrefix() );

    // absolute position fix for table cells
    $("th.abs-pos-content, td.abs-pos-content").iWouldLikeToAbsolutelyPositionThingsInsideOfFrickingTableCellsPlease();

    // customized checboxes
    $('input[type="checkbox"]').ezMark();

    // customized dropdowns
    $('select.customized').customSelect();

    // toggler
    $('[cb-toggle-trigger]').on('click', function(){
        var $container = $(this).closest('[cb-toggle-container]').find('[cb-toggle-content]');
        $container.toggle();
    });



    /*
     * HAK on profile page
     *
    */

    // datepicker for HAK
    $("#table-user-hak input[name=valid_thru]").datepicker({
        firstDay: 1,
        dateFormat: "yy-mm-dd",
        showOtherMonths: true,
        changeMonth: true,
        changeYear: true,
        beforeShow: function(textbox, instance){
            var $el   = $('#table-user-hak'),
                width = $el.find(".datepicker-hak-date")[0].offsetWidth + $el.find(".datepicker-hak-year")[0].offsetWidth - 4,
                top   = $el.find(".datepicker-hak-date").offset().top + $el.find(".datepicker-hak-date")[0].offsetHeight,
                left  = $el.find(".datepicker-hak-date").offset().left - 1;
            position_datepicker(width, top, left);
        },
        onChangeMonthYear: function(year, month, inst){
            var $el   = $('#table-user-hak'),
                width = $el.find(".datepicker-hak-date")[0].offsetWidth + $el.find(".datepicker-hak-year")[0].offsetWidth - 4,
                top   = $el.find(".datepicker-hak-date").offset().top + $el.find(".datepicker-hak-date")[0].offsetHeight,
                left  = $el.find(".datepicker-hak-date").offset().left - 1;
            position_datepicker(width, top, left);
        },
        onSelect: function(date_text){
            var $el = $('#table-user-hak');
            update_calendar_fields(date_text, $el.find('.datepicker-hak-date'), $el.find('.datepicker-hak-year'));
        }
    });
    // datepicker trigger for vehicle first registration day and month
    $("#table-user-hak .datepicker-hak-date").on('click', function(){
        $("#table-user-hak input[name=valid_thru]").datepicker("show");
    });
    // datepicker trigger for vehicle first registration year
    $("#table-user-hak .datepicker-hak-year").on('click', function(){
        $("#table-user-hak input[name=valid_thru]").datepicker("show");
    });
    // onload update for datepicker vehicle first registration
    update_calendar_fields($('#table-user-hak input[name=valid_thru]').val(), $('#table-user-hak .datepicker-hak-date'), $('#table-user-hak .datepicker-hak-year'));
    // clear alarm date in HAK table
    $('#table-user-hak [data-trigger=clear-date]').on('click', function(){
        $('#table-user-hak input[name=valid_thru]').val('');
        $('#table-user-hak .datepicker-hak-date').val('');
        $('#table-user-hak .datepicker-hak-year').val('');
    });



    /*
     * Birthday on profile page
     *
    */

    // datepicker for birthday
    $("#table-personal-data input[name=birthday]").datepicker({
        firstDay: 1,
        dateFormat: "yy-mm-dd",
        showOtherMonths: true,
        changeMonth: true,
        changeYear: true,
        beforeShow: function(textbox, instance){
            var $el   = $('#table-personal-data'),
                width = $el.find(".datepicker-birthday-date")[0].offsetWidth + $el.find(".datepicker-birthday-year")[0].offsetWidth - 4,
                top   = $el.find(".datepicker-birthday-date").offset().top + $el.find(".datepicker-birthday-date")[0].offsetHeight,
                left  = $el.find(".datepicker-birthday-date").offset().left - 1;
            position_datepicker(width, top, left);
        },
        onChangeMonthYear: function(year, month, inst){
            var $el   = $('#table-personal-data'),
                width = $el.find(".datepicker-birthday-date")[0].offsetWidth + $el.find(".datepicker-birthday-year")[0].offsetWidth - 4,
                top   = $el.find(".datepicker-birthday-date").offset().top + $el.find(".datepicker-birthday-date")[0].offsetHeight,
                left  = $el.find(".datepicker-birthday-date").offset().left - 1;
            position_datepicker(width, top, left);
        },
        onSelect: function(date_text){
            var $el = $('#table-personal-data');
            update_calendar_fields(date_text, $el.find('.datepicker-birthday-date'), $el.find('.datepicker-birthday-year'));
        }
    });
    // datepicker trigger for birthday day and month
    $("#table-personal-data .datepicker-birthday-date").on('click', function(){
        $("#table-personal-data input[name=birthday]").datepicker("show");
    });
    // datepicker trigger for birthday year
    $("#table-personal-data .datepicker-birthday-year").on('click', function(){
        $("#table-personal-data input[name=birthday]").datepicker("show");
    });
    // onload update for datepicker birthday
    update_calendar_fields($('#table-personal-data input[name=birthday]').val(), $('#table-personal-data .datepicker-birthday-date'), $('#table-personal-data .datepicker-birthday-year'));



    /*
     * Date of the first registration on the vehicle page
     *
    */

    // datepicker for vehicle first registration
    $('#table-vehicle-license input[name=date_first_reg]').datepicker({
        firstDay: 1,
        dateFormat: "yy-mm-dd",
        showOtherMonths: true,
        changeMonth: true,
        changeYear: true,
        beforeShow: function(textbox, instance){
            var $el   = $('#table-vehicle-license'),
                width = $el.find(".datepicker-first-reg-date")[0].offsetWidth + $el.find(".datepicker-first-reg-year")[0].offsetWidth - 4,
                top   = $el.find(".datepicker-first-reg-date").offset().top + $el.find(".datepicker-first-reg-date")[0].offsetHeight,
                left  = $el.find(".datepicker-first-reg-date").offset().left - 1;
            position_datepicker(width, top, left);
        },
        onChangeMonthYear: function(year, month, inst){
            var $el   = $('#table-vehicle-license'),
                width = $el.find(".datepicker-first-reg-date")[0].offsetWidth + $el.find(".datepicker-first-reg-year")[0].offsetWidth - 4,
                top   = $el.find(".datepicker-first-reg-date").offset().top + $el.find(".datepicker-first-reg-date")[0].offsetHeight,
                left  = $el.find(".datepicker-first-reg-date").offset().left - 1;
            position_datepicker(width, top, left);
        },
        onSelect: function(date_text){
            var $el = $('#table-vehicle-license');
            update_calendar_fields(date_text, $el.find('.datepicker-first-reg-date'), $el.find('.datepicker-first-reg-year'));
        }
    });
    // datepicker trigger for vehicle first registration day and month
    $("#table-vehicle-license .datepicker-first-reg-date").on('click', function(){
        $("#table-vehicle-license input[name=date_first_reg]").datepicker("show");
    });
    // datepicker trigger for vehicle first registration year
    $("#table-vehicle-license .datepicker-first-reg-year").on('click', function(){
        $("#table-vehicle-license input[name=date_first_reg]").datepicker("show");
    });
    // onload update for datepicker vehicle first registration
    update_calendar_fields($('#table-vehicle-license input[name=date_first_reg]').val(), $('#table-vehicle-license .datepicker-first-reg-date'), $('#table-vehicle-license .datepicker-first-reg-year'));


});