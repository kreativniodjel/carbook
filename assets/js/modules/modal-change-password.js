define(function () {


	var elemTarget = "#modal-change-password",
	elemTrigger    = "[cb-click='open change password modal']",
	isVisible      = false;


	function wrap() {
		$(elemTarget).wrap( $('<table class="modal-wrap"><tr><td>') );
	}

	function toggle() {
		if (isVisible === false) {
			open();
		} else {
			close();
		}
	}

	function open() {
		$(elemTarget).find('form')[0].reset();
		$(elemTarget).removeClass('hidden').closest('.modal-wrap').show();
		isVisible = true;
	}

	function close() {
		$(elemTarget).addClass('hidden');
		$(elemTarget).addClass('hidden').closest('.modal-wrap').hide();
		isVisible = false;
	}

	function check_required_fields() {
		var is_valid = true;

		$(elemTarget).find('form [required]').each(function(){
			if( $(this).val() === "" ) is_valid = false;
		});

		return is_valid;
	}

	function check_repeated_password() {
		var is_valid = true,
			new_pass = $(elemTarget).find('input[name=new_password]').val(),
			repeated_pass = $(elemTarget).find('input[name=repeat_password]').val();

		if( new_pass !== repeated_pass ) is_valid = false;

		return is_valid;
	}

	function submit() {
		var data = $(elemTarget).find('form').serialize();

		$.ajax ({
	        type: "POST",
	        url: CARBOOK.json.changepassword,
	        dataType: 'json',
	        async: false,
	        data: data,
	        success: function (json) {

	        	switch(json.STATUS) {

	        		case "OK":
					alert(json.MESSAGE);
					close();
	        		break;

	        		case "ERROR":
	        		alert(json.MESSAGE);
	        		break;

	        	}

	        }
	    });

	}


	/*
	 * Binding
	 */

	 function binding() {

		// Toggle modul
		$(elemTrigger).on('click', function(e){
			e.preventDefault();
			open();
		});

		// Close modul
		$(elemTarget).find('[data-trigger=close]').on('click', function(e){
			e.preventDefault();
			close();
		});

		// Post data
		$(elemTarget).find('form').on('submit', function(e){
			e.preventDefault();
			if( check_required_fields() === true ) {
				if( check_repeated_password() === false ) {
					alert("Ponovljena zaporka nije ista!");
				} else {
					submit();
				}
			}
		});

	 }


	/*
	 * Initialize
	 */

	 function init() {
		//$.get(CARBOOK.partials.modal_change_password, function(result){
			//$('body').append(result);
			wrap();
			binding();
			//$(elemTarget).find('th.abs-pos-content, td.abs-pos-content').iWouldLikeToAbsolutelyPositionThingsInsideOfFrickingTableCellsPlease();
		//});	 	
	 }

	 init();

});