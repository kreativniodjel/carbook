define(function () {

	var $el = $("#vehicle-gallery");

	function submit() {
		return;
		var data = $(elemTarget).find('form').serialize();

		$.ajax ({
	        type: "POST",
	        url: CARBOOK.json.addvehicle,
	        dataType: 'json',
	        async: false,
	        data: data,
	        success: function (json) {

	        	switch(json.STATUS) {

	        		case "OK":
        	        var gotourl = CARBOOK.site_url + "/vehicles_licences/";
					window.location.replace(gotourl);
	        		break;

	        		case "ERROR":
	        		alert(json.MESSAGE);
	        		break;

	        	}

	        }
	    });

	}


	/*
	 * Binding
	 */

	 function binding() {

		// upload
		$el.find('[data-trigger=upload]').on('click', function(e){
			e.preventDefault();
			$el.find('input[type=file]').click();
		});
		$el.find('[type=file]').on('change', function(e){
			e.preventDefault();
			$el.find('form')[0].submit();
		});

		// featured
		$el.find('[data-trigger=featured]').on('click', function(e){
			e.preventDefault();
			var id = $(this).parent().parent().find('img').attr('data-id'),
                gotourl = CARBOOK.site_url + "/vehicles_licences/set_pic_as_thumbnail/" + id;
			window.location.replace(gotourl);
		});

		// delete
		$el.find('[data-trigger=delete]').on('click', function(e){
			e.preventDefault();
			var id = $(this).parent().parent().find('img').attr('data-id');
            if(confirm('Jeste sigurni da želite obrisati fotografiju?')) {
                var gotourl = CARBOOK.site_url + "/vehicles_licences/delete_picture/" + id;
				window.location.replace(gotourl);
            }
		});

	 }


	/*
	 * Initialize
	 */

	 function init() {
		binding();
	 }

	 init();

});