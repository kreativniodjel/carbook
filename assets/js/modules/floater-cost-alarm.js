define(function () {


	var $modulElement = undefined,
		$modulToggler = undefined,
		isVisible     = false;


	function elements() {
		$modulElement = $("#floater-cost-alarm");
		$modulToggler = $("#table-costs-all [data-trigger=show-alarm-floater], #table-costs-vehicle [data-trigger=show-alarm-floater]");
	}


	function toggleModul() {
		if (isVisible === false) {
			$modulElement.removeClass('hidden');
			isVisible = true;
		} else {
			$modulElement.addClass('hidden');
			isVisible = false;
		}
	}


	function open() {
		$modulElement.removeClass('hidden');
		isVisible = true;
	}


	function close() {
		$modulElement.addClass('hidden');
		isVisible = false;
	}


	function modulPosition(e) {

		var x = $(e.target).offset().left - ($modulElement.width() / 2) - 7,
			y = $(e.target).offset().top  + $(e.target).height() + 9;

		$modulElement.css({
			top  : y,
			left : x
		});

	}


	function updateBinding($el) {

		var $row = $el.closest('tr'),
			data = $row.data();

		$modulElement.find('a[href=#promjeni]').on('click', function(e){
			e.preventDefault();
			$('#modal-cost form input[name=idcost]').val(data.clickableCosts);
			$('#modal-cost form input[name=iddetails]').val(data.costsDetails);
		});

		$modulElement.find('a[href=#obrisi]').on('click', function(e){
			e.preventDefault();
			alert("Sanjin: ne znam koji alarm treba brisati (datum ili km).");
            /*if(confirm('Obriši?')) {
                var gotourl = CARBOOK.site_url + "/costs/delete/" + data.clickableCosts + "/" + data.costsDetails;
				window.location.replace(gotourl);
            }*/
		});

	}


	function binding() {

		$('body').on('click', function(e){
			if( $(e.target).not($modulToggler)[0] ) {
				close();
			}
		});

		$modulToggler.on('click', function(e){
			e.preventDefault();
			if(isVisible === false) {
				updateBinding( $(this) );
				open();
				modulPosition(e);
			} else {
				close();
			}
		});

	}


	/*
	 * Initialize
	 */

	function init() {
		//$.get(CARBOOK.partials.floater_costs_fuel, function(result){
			//$('body').append(result);
			elements();
			binding();
		//});
	}

	init();

});