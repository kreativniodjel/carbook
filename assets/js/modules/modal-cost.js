define(function () {


	var elemTarget = "#modal-cost",
		elemTrigger    = "[data-trigger='open-cost-modal']",
		noteMaxChars   = 150,
		isVisible      = false;


	function wrap() {
		$(elemTarget).wrap( $('<table class="modal-wrap"><tr><td>') );
	}

	function open() {
		$(elemTarget).removeClass('hidden').closest('.modal-wrap').show();
		$('#modal-cost-note-counter').text(noteMaxChars);
		isVisible = true;
	}

	function close() {
		$(elemTarget).addClass('hidden').closest('.modal-wrap').hide();
		$(elemTarget).find('form')[0].reset();
		isVisible = false;
	}

	function check_required_fields() {
		var is_valid = true;

		$(elemTarget).find('form [required]').each(function(){
			if( $(this).val() === "" ) is_valid = false;
		});

		return is_valid;
	}

	function updateData() {

		var data = {
			idcost    : $(elemTarget).find('form input[name=idcost]').val(),
			iddetails : $(elemTarget).find('form input[name=iddetails]').val()
		}

		if(data.idcost != "" && data.iddetails != "") {

			$.ajax({
		        type: "GET",
		        url: CARBOOK.json.getcost,
		        dataType: 'json',
		        async: false,
		        data: data,
		        success: function (json) {

		        	switch(json.STATUS) {

		        		case "OK":
		        			// update fields
			        		$(elemTarget).find('form [name=date]').val( json.DATA.db_date );
			        		$(elemTarget).find('form [name=km]').val( json.DATA.db_km );
			        		$(elemTarget).find('form [name=place_of_cost]').val( json.DATA.db_place_of_cost );
			        		$(elemTarget).find('form [name=note]').val( json.DATA.db_note );
			        		$(elemTarget).find('form [name=quantity]').val( json.DATA.db_quantity );
			        		$(elemTarget).find('form [name=price]').val( json.DATA.db_price );
			        		$(elemTarget).find('form [name=alarm_km]').val( json.DATA.db_alarm_km );
			        		$(elemTarget).find('form [name=alarm_date]').val( json.DATA.db_alarm_date );
			        		$(elemTarget).find('form [name=type_options]').val( json.DATA.curr_type_options ).change();
			        		// update calendars
				      		update_calendar_fields(json.DATA.db_date, ".datepicker-cost-date", ".datepicker-cost-year");
				      		if( json.DATA.db_alarm_date !== null ) update_calendar_fields(json.DATA.db_alarm_date, ".datepicker-alarm-date", ".datepicker-alarm-year");
				      		// update note counter
				      		note_counter();
		        			break;

		        		case "ERROR":
			        		alert(json.MESSAGE);
			        		close();
		    	    		break;

		        	}

		        }
		    });

		}

	}

	function submit() {
		var post_data = $(elemTarget).find('form').serialize(),
			idcost    = $(elemTarget).find('form input[name=idcost]').val(),
			iddetails = $(elemTarget).find('form input[name=iddetails]').val(),
			location  = ( idcost == "" && iddetails == "" ) ? CARBOOK.json.addcost : CARBOOK.json.editcost;

		$.ajax({
	        type: "POST",
	        url: location,
	        dataType: 'json',
	        async: false,
	        data: post_data,
	        success: function (json) {

	        	switch(json.STATUS) {

	        		case "OK":
					window.location.reload();
	        		break;

	        		case "ERROR":
	        		alert(json.MESSAGE);
	        		break;

	        	}

	        }
	    });

	}

	 function note_counter() {
		var max_char = noteMaxChars,
			count = $("#modal-cost-note").val().length,
			chars_left = max_char - count;

		$('#modal-cost-note-counter').text(chars_left);
	 }

	 function clear_alarm_date() {
		$(elemTarget).find('input[name=alarm_date]').val('');
		$(elemTarget).find('input.datepicker-alarm-date').val('');
		$(elemTarget).find('input.datepicker-alarm-year').val('');
	 }


	/*
	 * Binding
	 */

	 function binding() {

		// Open modul
		$(elemTrigger).on('click', function(e){
			e.preventDefault();
			setTimeout(updateData, 0);
			open();
		});

		// Close modul
		$(elemTarget).find('[data-trigger=close]').on('click', function(e){
			e.preventDefault();
			close();
		});

		// Clear alarm date
		$(elemTarget).find('[data-trigger=clear-alarm-date]').on('click', function(){
			clear_alarm_date();
		});

		// Post data
		$(elemTarget).find('form').on('submit', function(e){
			e.preventDefault();
			if( check_required_fields() === true ) {
				submit();
			}
		});

		// Character counter
		$('#modal-cost-note').on('keydown', note_counter);
		$('#modal-cost-note').on('keyup', note_counter);
		$('#modal-cost-note').on('change', note_counter);

		// Datepickers
		$(elemTarget).find(".datepicker-cost-date").on('click', function(){
			$(elemTarget).find("input[name=date]").datepicker("show");
		});
		$(elemTarget).find(".datepicker-cost-year").on('click', function(){
			$(elemTarget).find("input[name=date]").datepicker("show");
		});
		$(elemTarget).find(".datepicker-alarm-date").on('click', function(){
			$(elemTarget).find("input[name=alarm_date]").datepicker("show");
		});
		$(elemTarget).find(".datepicker-alarm-year").on('click', function(){
			$(elemTarget).find("input[name=alarm_date]").datepicker("show");
		});



	 }



	/*
	 * Initialize
	 */

	 function init() {
		//$.get(CARBOOK.partials.modal_cost_add, function(result){

			//$('body').append(result);

			wrap();
			binding();

			//open();
			//$(elemTarget).find('th.abs-pos-content, td.abs-pos-content').iWouldLikeToAbsolutelyPositionThingsInsideOfFrickingTableCellsPlease();
			//$(elemTarget).find("select").customSelect();
			//close();

			$(elemTarget).find("input[name=date]").datepicker({
				firstDay: 1,
				dateFormat: "yy-mm-dd",
				showOtherMonths: true,
		        changeMonth: true,
		        changeYear: true,
				beforeShow: function(textbox, instance){
					var width = $(elemTarget).find(".datepicker-cost-date")[0].offsetWidth + $(elemTarget).find(".datepicker-cost-year")[0].offsetWidth - 4,
						top   = $(elemTarget).find(".datepicker-cost-date").offset().top + $(elemTarget).find(".datepicker-cost-date")[0].offsetHeight,
						left  = $(elemTarget).find(".datepicker-cost-date").offset().left - 1;
					position_datepicker(width, top, left);
				},
				onChangeMonthYear: function(year, month, inst){
					var width = $(elemTarget).find(".datepicker-cost-date")[0].offsetWidth + $(elemTarget).find(".datepicker-cost-year")[0].offsetWidth - 4,
						top   = $(elemTarget).find(".datepicker-cost-date").offset().top + $(elemTarget).find(".datepicker-cost-date")[0].offsetHeight,
						left  = $(elemTarget).find(".datepicker-cost-date").offset().left - 1;
					position_datepicker(width, top, left);
				},
		      	onSelect: function(date_text){
		      		update_calendar_fields(date_text, $(elemTarget).find(".datepicker-cost-date"), $(elemTarget).find(".datepicker-cost-year"));
		      	}
			});

			$(elemTarget).find("input[name=alarm_date]").datepicker({
				firstDay: 1,
				dateFormat: "yy-mm-dd",
				showOtherMonths: true,
		        changeMonth: true,
		        changeYear: true,
				beforeShow: function(textbox, instance){
					var width = $(elemTarget).find(".datepicker-alarm-date")[0].offsetWidth + $(elemTarget).find(".datepicker-alarm-year")[0].offsetWidth - 4,
						top   = $(elemTarget).find(".datepicker-alarm-date").offset().top + $(elemTarget).find(".datepicker-alarm-date")[0].offsetHeight,
						left  = $(elemTarget).find(".datepicker-alarm-date").offset().left - 1;
					position_datepicker(width, top, left);
				},
				onChangeMonthYear: function(year, month, inst){
					var width = $(elemTarget).find(".datepicker-alarm-date")[0].offsetWidth + $(elemTarget).find(".datepicker-alarm-year")[0].offsetWidth - 4,
						top   = $(elemTarget).find(".datepicker-alarm-date").offset().top + $(elemTarget).find(".datepicker-alarm-date")[0].offsetHeight,
						left  = $(elemTarget).find(".datepicker-alarm-date").offset().left - 1;
					position_datepicker(width, top, left);
				},
		      	onSelect: function(date_text){
		      		update_calendar_fields(date_text, $(elemTarget).find(".datepicker-alarm-date"), $(elemTarget).find(".datepicker-alarm-year"));
		      	}
			});

		//});	 	
	 }

	 init();

});