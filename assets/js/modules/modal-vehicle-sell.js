define(function () {


	var elemTarget = "#modal-vehicle-sell",
	elemTrigger    = "[data-trigger=open-sell-vehicle-modal]",
	isVisible      = false;


	function wrap() {
		$(elemTarget).wrap( $('<table class="modal-wrap"><tr><td>') );
	}

	function toggle() {
		if (isVisible === false) {
			open();
		} else {
			close();
		}
	}

	function open() {
		$(elemTarget).removeClass('hidden').closest('.modal-wrap').show();
		isVisible = true;
	}

	function close() {
		$(elemTarget).addClass('hidden').closest('.modal-wrap').hide();
		$(elemTarget).find('form')[0].reset();
		isVisible = false;
	}

	function check_required_fields() {
		var is_valid = true;

		$(elemTarget).find('form [required]').each(function(){
			if( $(this).val() === "" ) is_valid = false;
		});

		return is_valid;
	}

	function updateData() {

		var id   = $(elemTarget).find("input[name=idveh_sell]").val(),
			$row = $('#table-vehicles tr[data-clickable-vehicle='+id+']'),
			data = $row.data();

		$(elemTarget).find('input[name=brand]').val(data.brand);
		$(elemTarget).find('input[name=model]').val(data.model);
		$(elemTarget).find('input[name=type]').val(data.type);
		$(elemTarget).find('input[name=year]').val(data.year);
		$(elemTarget).find('input[name=km]').val(data.km);
		$(elemTarget).find('input[name=fuel]').val(data.fuel);

	}

	function submit() {
		var data = $(elemTarget).find('form').serialize();

		$.ajax ({
	        type: "POST",
	        url: CARBOOK.json.sellvehicle,
	        dataType: 'json',
	        async: false,
	        data: data,
	        success: function (json) {

	        	switch(json.STATUS) {

	        		case "OK":
        	        var gotourl = CARBOOK.site_url + "/ads/";
					window.location.replace(gotourl);
	        		break;

	        		case "ERROR":
	        		alert(json.MESSAGE);
	        		break;

	        	}

	        }
	    });

	}


	/*
	 * Binding
	 */

	 function binding() {

		// Toggle modul
		$(elemTrigger).on('click', function(e){
			e.preventDefault();
			setTimeout(function(){
				updateData();
				open();
			},0);
		});

		// Close modul
		$(elemTarget).find('[data-trigger=close]').on('click', function(e){
			e.preventDefault();
			close();
		});

		// Post data
		$(elemTarget).find('form').on('submit', function(e){
			e.preventDefault();
			if( check_required_fields() === true ) {
				submit();
			}
		});

	 }


	/*
	 * Initialize
	 */

	 function init() {
		//$.get(CARBOOK.partials.modal_vehicle_add, function(result){
			//$('body').append(result);
			wrap();
			binding();
			//$(elemTarget).find('th.abs-pos-content, td.abs-pos-content').iWouldLikeToAbsolutelyPositionThingsInsideOfFrickingTableCellsPlease();
		//});	 	
	 }

	 init();

});