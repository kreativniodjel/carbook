define(function() {


	var elemTarget = "#modal-login",
	elemTrigger    = "[data-trigger=modal-login]",
	isVisible      = false;


	function wrap() {
		$(elemTarget).wrap( $('<table class="modal-wrap"><tr><td>') );
	}

	function toggle() {
		if (isVisible === false) {
			open();
		} else {
			close();
		}
	}

	function open() {
		$(elemTarget).find('form')[0].reset();
		$(elemTarget).removeClass('hidden').closest('.modal-wrap').show();
		setTimeout(function(){
			$(elemTarget).find('input[name=username]').focus();
		}, 200 );
		isVisible = true;
	}

	function close() {
		$(elemTarget).addClass('hidden');
		$(elemTarget).addClass('hidden').closest('.modal-wrap').hide();
		isVisible = false;
	}

	function submit() {
		var data = $(elemTarget + ' form').serialize();

		$.ajax ({
	        type: "POST",
	        url: CARBOOK.json.login,
	        dataType: 'json',
	        async: false,
	        data: data,
	        success: function (json) {

	        	switch(json.STATUS) {

	        		case "OK":
	        		location.reload();
	        		break;

	        		case "ERROR":
	        		alert(json.MESSAGE);
	        		break;

	        	}

	        }
	    });

	}


	/*
	 * Binding
	 */

	 function binding() {

		// Toggle modul
		$(elemTrigger).on('click', function(e){
			e.preventDefault();
			open();
		});

		// Close modul
		$(elemTarget + ' [data-trigger=close]').on('click', function(e){
			e.preventDefault();
			close();
		});

		// Post data
		$(elemTarget + ' form').on('submit', function(e){
			e.preventDefault();
			submit();
		});

	 }


	/*
	 * Initialize
	 */

	 function init() {
		//$.get(CARBOOK.partials.modal_login, function(result){
			//$('body').append(result);
			wrap();
			binding();
		//});	 	
	 }

	 init();

});