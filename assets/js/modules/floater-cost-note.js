define(function () {


	var $modulElement = undefined,
		$modulToggler = undefined,
		isVisible     = false;


	function elements() {
		$modulElement = $("#floater-cost-note");
		$modulToggler = $("#table-costs-all [data-trigger=show-cost-note-floater], #table-costs-vehicle [data-trigger=show-cost-note-floater]");
	}


	function show() {
		$modulElement.removeClass('hidden');
	}


	function hide() {
		$modulElement.addClass('hidden');
	}


	function modulPosition(e) {
		var x = $(e.target).offset().left + $(e.target).width() - ($modulElement.width() / 2) - 11,
			y = $(e.target).offset().top + 12;

		$modulElement.css({
			top  : y,
			left : x
		});
	}


	function updateData($el) {

		var $row = $el.closest('tr'),
			data = $row.data(),
			note = data.note;

		$modulElement.text(note);

	}


	function binding() {

		$('body').on('click', function(e){
			if( $(e.target).not($modulToggler)[0] ) {
				hide();
			}
		});

		$modulToggler.on('click', function(e){
			e.preventDefault();
			if(isVisible === false) {
				updateData( $(this) );
				show();
				modulPosition(e);
			} else {
				hide();
			}
		});

	}


	/*
	 * Initialize
	 */

	function init() {
		//$.get(CARBOOK.partials.floater_costs_note, function(result){
			//$('body').append(result);
			elements();
			binding();
		//});
	}

	init();


});