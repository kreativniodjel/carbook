define(function () {


	var elemTarget = "#modal-vehicle-add",
	elemTrigger    = "#table-vehicles button",
	isVisible      = false;


	function wrap() {
		$(elemTarget).wrap( $('<table class="modal-wrap"><tr><td>') );
	}

	function toggle() {
		if (isVisible === false) {
			open();
		} else {
			close();
		}
	}

	function open() {
		$(elemTarget).find('form')[0].reset();
		$(elemTarget).removeClass('hidden').closest('.modal-wrap').show();
		isVisible = true;
	}

	function close() {
		$(elemTarget).addClass('hidden');
		$(elemTarget).addClass('hidden').closest('.modal-wrap').hide();
		isVisible = false;
	}

	function check_required_fields() {
		var is_valid = true;

		$(elemTarget).find('form [required]').each(function(){
			if( $(this).val() === "" ) is_valid = false;
		});

		return is_valid;
	}

	function submit() {
		var data = $(elemTarget).find('form').serialize();

		$.ajax ({
	        type: "POST",
	        url: CARBOOK.json.addvehicle,
	        dataType: 'json',
	        async: false,
	        data: data,
	        success: function (json) {

	        	switch(json.STATUS) {

	        		case "OK":
        	        var gotourl = CARBOOK.site_url + "/vehicles_licences/";
					window.location.replace(gotourl);
	        		break;

	        		case "ERROR":
	        		alert(json.MESSAGE);
	        		break;

	        	}

	        }
	    });

	}


	/*
	 * Binding
	 */

	 function binding() {

		// Toggle modul
		$(elemTrigger).on('click', function(e){
			e.preventDefault();
			open();
		});

		// Close modul
		$(elemTarget).find('[data-trigger=close]').on('click', function(e){
			e.preventDefault();
			close();
		});

		// Post data
		$(elemTarget).find('form').on('submit', function(e){
			e.preventDefault();
			if( check_required_fields() === true ) {
				submit();
			}
		});

	 }


	/*
	 * Initialize
	 */

	 function init() {
		//$.get(CARBOOK.partials.modal_vehicle_add, function(result){
			//$('body').append(result);
			wrap();
			binding();
			//$(elemTarget).find('th.abs-pos-content, td.abs-pos-content').iWouldLikeToAbsolutelyPositionThingsInsideOfFrickingTableCellsPlease();
		//});	 	
	 }

	 init();

});