define(function () {


	var $modulElement = undefined,
		$modulToggler = undefined;


	function elements() {
		$modulElement = $("#floater-vehicle-photo");
		$modulToggler = $("#table-vehicles [data-trigger=show-vehicle-photo], #table-vehicles-ex [data-trigger=show-vehicle-photo]");
	}


	function show() {
		$modulElement.removeClass('hidden');
	}


	function hide() {
		$modulElement.addClass('hidden');
	}


	function modulPosition(e) {

		var x = $(e.target).offset().left + $(e.target).width() + 4,
			y = $(e.target).offset().top  + ( $(e.target).height() / 2 ) - ( $modulElement.height() / 2 ) - 6;

		$modulElement.css({
			top  : y,
			left : x
		});
	}


	function updateData(e) {

		var img_src = $(e.target).attr('src');

		$modulElement.find('img').attr('src', img_src);

	}


	function binding() {

		$modulToggler.on('mouseover', function(e){
			updateData(e);
			show();
			modulPosition(e);
		});

		$modulToggler.on('mouseout', function(){
			hide();
		});
	}


	/*
	 * Initialize
	 */

	function init() {
		//$.get(CARBOOK.partials.floater_vehicle_photo, function(result){
			//$('body').append(result);
			elements();
			binding();
		//});
	}

	init();


});