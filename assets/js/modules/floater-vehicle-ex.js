define(function () {


	var $modulElement = undefined,
		$modulToggler = undefined,
		isVisible = false;


	function elements() {
		$modulElement = $("#ex-vehicle-modul");
		$modulToggler = $("#table-vehicles-ex [data-trigger=show-vehicle-options]");
	}


	function toggleModul() {
		if (isVisible === false) {
			$modulElement.removeClass('hidden');
			isVisible = true;
		} else {
			$modulElement.addClass('hidden');
			isVisible = false;
		}
	}


	function open() {
		$modulElement.removeClass('hidden');
		isVisible = true;
	}


	function close() {
		$modulElement.addClass('hidden');
		isVisible = false;
	}


	function modulPosition(e) {

		var x = $(e.target).offset().left - ($modulElement.width() / 2) - 7,
			y = $(e.target).offset().top  + $(e.target).height() + 9;

		$modulElement.css({
			top  : y,
			left : x
		});

	}


	function updateData($el) {

		var $row    = $el.closest('tr'),
			data    = $row.data(),
			img_src = $row.find('img').attr('src'),
			title   = data.name,
			action  = $row.attr('href');

		$modulElement.find('img').attr('src',img_src);
		$modulElement.find('.title').text(title);

	}

	function updateBinding($el) {

		var $row = $el.closest('tr'),
			data = $row.data();

		$modulElement.find('a[href=#obrisi]').on('click', function(e){
			e.preventDefault();
            if(confirm('Jeste sigurni da želite obrisati vozilo?')) {
                var gotourl = CARBOOK.site_url + "/vehicles/delete/" + data.clickableVehicle + "/1";
				window.location.replace(gotourl);
            }
		});

		$modulElement.find('a[href=#premjesti-u-sadasnja]').on('click', function(e){
			e.preventDefault();
            if(confirm('Potvrdi premještanje.')) {
                var gotourl = CARBOOK.site_url + "/vehicles/switch_to_current/" + data.clickableVehicle;
				window.location.replace(gotourl);
            }
		});

	}


	function binding() {

		$('body').on('click', function(e){
			if( $(e.target).not($modulToggler)[0] ) {
				close();
			}
		});

		$modulToggler.on('click', function(e){
			e.preventDefault();
			if(isVisible === false) {
				updateData( $(this) );
				updateBinding( $(this) );
				modulPosition(e);
				open();
			} else {
				close();
			}
		});

	}


	/*
	 * Initialize
	 */

	function init() {
		//$.get(CARBOOK.partials.floater_vehicle_ex, function(result){
			//$('body').append(result);
			elements();
			binding();
		//});
	}

	init();

});