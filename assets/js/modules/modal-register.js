define(function () {


	var elementTarget = "#modal-register",
	elementTrigger    = "[data-trigger=modal-register]",
	isVisible         = false;


	function wrap() {
		$(elementTarget).wrap( $('<table class="modal-wrap"><tr><td>') );
	}

	function toggle() {
		if (isVisible === false) {
			open();
		} else {
			close();
		}
	}

	function open() {
		$(elementTarget).removeClass('hidden').closest('.modal-wrap').show();
		isVisible = true;
	}

	function close() {
		$(elementTarget).addClass('hidden').closest('.modal-wrap').hide();
		isVisible = false;
	}

	function submit() {
		var data = $(elementTarget + ' form').serialize();

		$.ajax ({
	        type: "POST",
	        url: CARBOOK.json.register,
	        dataType: 'json',
	        async: false,
	        data: data,
	        success: function (json) {

	        	switch(json.STATUS) {

	        		case "OK":
	        		alert(json.MESSAGE);
	        		close();
	        		break;

	        		case "ERROR":
	        		alert(json.MESSAGE);
	        		break;

	        	}

	        }
	    });

	}


	/*
	 * Binding
	 */

	 function binding() {

		// Toggle modul
		$(elementTrigger).on('click', function(e){
			e.preventDefault();
			open();
		});

		// Close modul
		$(elementTarget + ' [data-trigger=close]').on('click', function(e){
			e.preventDefault();
			close();
		});

		// Post data
		$(elementTarget + ' form').on('submit', function(e){
			e.preventDefault();
			submit();
		});

	 }


	/*
	 * Initialize
	 */

	 function init() {
		//$.get(CARBOOK.partials.modal_register, function(result){
			//$('body').append(result);
			wrap();
			binding();
		//});	 	
	 }

	 init();

});