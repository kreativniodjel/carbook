define(function () {


	var modulElement = "#user-modul",
	modulToggler     = "#user-toggler",
	isVisible        = false;


	function toggleModul() {
		if (isVisible === false) {
			$(modulElement).removeClass('hidden');
			isVisible = true;
		} else {
			$(modulElement).addClass('hidden');
			isVisible = false;
		}
	}


	function open() {
		$(modulElement).removeClass('hidden');
		isVisible = true;
	}


	function close() {
		$(modulElement).addClass('hidden');
		isVisible = false;
	}


	function modulPosition() {

		var togglerTop = $(modulToggler).offset().top,
		togglerLeft    = $(modulToggler).offset().left,
		togglerHeight  = $(modulToggler).height(),
		modulWidth     = $(modulElement).width();
		offsetX        = 25,
		offsetY        = 5;

		$(modulElement).css({
			top  : togglerTop + togglerHeight + offsetY,
			left : togglerLeft - modulWidth + offsetX
		});

	}


	 function binding() {

		$('body').on('click', function(e){
			if( $(e.target).is(':not('+modulToggler+')') ) {
				close();
			}
		});

		$(modulToggler).on('click', function(e){
			e.preventDefault();
			modulPosition();
			toggleModul();
		});

	 }


	/*
	 * Initialize
	 */

	function init() {
		//$.get(CARBOOK.partials.floater_user, function(result){
			//$('body').append(result);
			binding();
		//});
	}

	init();

});