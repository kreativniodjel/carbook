define(function () {


	var $modulElement = undefined,
		$modulToggler = undefined,
		isVisible = false;


	function elements() {
		$modulElement = $("#vehicle-modul");
		$modulToggler = $("#table-vehicles [data-trigger=show-vehicle-options]");
	}


	function toggleModul() {
		if (isVisible === false) {
			$modulElement.removeClass('hidden');
			isVisible = true;
		} else {
			$modulElement.addClass('hidden');
			isVisible = false;
		}
	}


	function open() {
		$modulElement.removeClass('hidden');
		isVisible = true;
	}


	function close() {
		$modulElement.addClass('hidden');
		isVisible = false;
	}


	function modulPosition(e) {

		var x = $(e.target).offset().left - ($modulElement.width() / 2) - 7,
			y = $(e.target).offset().top  + $(e.target).height() + 9;

		$modulElement.css({
			top  : y,
			left : x
		});

	}


	function updateData($el) {

		var $row    = $el.closest('tr'),
			data    = $row.data(),
			img_src = $row.find('img').attr('src'),
			title   = data.name,
			action  = $row.attr('href');

		$modulElement.find('img').attr('src',img_src);
		$modulElement.find('.title').text(title);

	}


	function updateBinding($el) {

		var $row = $el.closest('tr'),
			data = $row.data();

		$modulElement.find('a[href=#prodaj]').on('click', function(e){
			$('#modal-vehicle-sell form input[name=idveh_sell]').val(data.clickableVehicle);
			e.preventDefault();
		});

		$modulElement.find('a[href=#obrisi]').on('click', function(e){
			e.preventDefault();
            if(confirm('Jeste sigurni da želite obrisati vozilo?')) {
                var gotourl = CARBOOK.site_url + "/vehicles/delete/" + data.clickableVehicle + "/0";
				window.location.replace(gotourl);
            }
		});

		$modulElement.find('a[href=#premjesti-u-bivsa]').on('click', function(e){
			e.preventDefault();
            if(confirm('Potvrdi premještanje.')) {
                var gotourl = CARBOOK.site_url + "/vehicles/switch_to_ex/" + data.clickableVehicle;
				window.location.replace(gotourl);
            }
		});

	}


	function binding() {

		$('body').on('click', function(e){
			if( $(e.target).not($modulToggler)[0] ) {
				close();
			}
		});

		$modulToggler.on('click', function(e){
			e.preventDefault();
			if(isVisible === false) {
				updateData( $(this) );
				updateBinding( $(this) );
				open();
				modulPosition(e);
			} else {
				close();
			}
		});

	}


	/*
	 * Initialize
	 */

	function init() {
		//$.get(CARBOOK.partials.floater_vehicle, function(result){
			//$('body').append(result);
			elements();
			binding();
		//});
	}

	init();

});