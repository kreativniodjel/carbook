<?php


// Main menu

$lang['nav-login']      = "PRIJAVI SE";
$lang['nav-register']   = "REGISTRIRAJ SE";
$lang['nav-vehicles']   = "VOZNI PARK";
$lang['nav-ads']        = "OGLASI";
$lang['nav-statistics'] = "STATISTIKA";
$lang['nav-currency']   = "kn";
$lang['nav-profile']    = "PROFIL";
$lang['nav-settings']   = "POSTAVKE";
$lang['nav-logout']     = "ODJAVA";


// Register

$lang['modal-register-title']    = "REGISTRACIJA";
$lang['modal-register-username'] = "Korisničko ime";
$lang['modal-register-password'] = "Lozinka";
$lang['modal-register-email']    = "e-mail adresa";
$lang['modal-register-ok']       = "POTVRDI";


// Login

$lang['modal-login-title']         = "PRIJAVA";
$lang['modal-login-username']      = "Korisničko ime";
$lang['modal-login-password']      = "Lozinka";
$lang['modal-login-lost-password'] = "Izgubljena lozinka?";
$lang['modal-login-ok']            = "POTVRDI";


?>