<?php


// Main menu

$lang['nav-login']      = "LOGIN";
$lang['nav-register']   = "REGISTER";
$lang['nav-vehicles']   = "VEHICLES";
$lang['nav-ads']        = "ADS";
$lang['nav-statistics'] = "STATISTICS";
$lang['nav-currency']   = "kn";
$lang['nav-profile']    = "PROFILE";
$lang['nav-settings']   = "SETTINGS";
$lang['nav-logout']     = "LOGOUT";


// Register

$lang['modal-register-title']    = "REGISTER";
$lang['modal-register-username'] = "Username";
$lang['modal-register-password'] = "Password";
$lang['modal-register-email']    = "e-mail address";
$lang['modal-register-ok']       = "OK";


// Login

$lang['modal-login-title']         = "LOGIN";
$lang['modal-login-username']      = "Username";
$lang['modal-login-password']      = "Password";
$lang['modal-login-lost-password'] = "Lost password?";
$lang['modal-login-ok']            = "OK";


?>