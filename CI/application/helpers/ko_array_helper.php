<?php

/**
* Check if all fields in an array are not empty, if some is empty it will be set to NULL. 
* It is important to check all fields before entering data to database. Otherwise we will have zero values in fields.
*
* @access	public
* @param	array
* @return	bool
*/
function KO_check_array_values($data)
{
   foreach ($data as $key => $value)
   {
       if(empty($data[$key]))
            $data[$key] = NULL;
   }
    
}

/**
* Create random string in sended size 
*
* @access	public
* @param	array
* @return	bool
*/
function create_random_string($nStringSize)
{
    //- Create large string of available chars
    $string = "";
    for($i = 0; $i < $nStringSize; $i++)
        $string = $string.'ABCDEFGHIJKLMNOPRSTUVZ1234567890';
    $arr = str_split($string); 
    shuffle($arr);
    $arr = array_slice($arr, 0, $nStringSize);
    $str = implode('', $arr); // smush them back into a string

    return $str;
}

/*
 * Return true if string contain date (for example if we read form date filed from database)
 * @access	public
 * @param	string
 * @return	bool 
 */
function KO_date_exist($date)
{
    if(($date == '')||($date == '0000-00-00')||(empty($date)))
        return FALSE;
    return TRUE;
}
/* End of file KO_array_helper.php */
