<?php

/**
* Format number for kilometers
*
* @access	public
* @param	int
* @return	string
*/
function KO_number_format_km($number)
{
    return str_replace(',', '.', number_format($number));
}

/**
* Format number for cost
*
* @access public
* @param  int
* @return string
*/
function KO_number_format_cost($number)
{
    return number_format($number, 2, ',', '.'); 
}

/**
* Format number for total cost in header
*
* @access public
* @param  int
* @return string
*/
function KO_number_format_total_cost($number)
{
    return number_format($number, 0, ',', '.'); 
}

/* End of file KO_number_helper.php */
