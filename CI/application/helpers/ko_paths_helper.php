<?php

$url = base_url() . index_page();

define('URL_BASE',        base_url() );
define('URL_HOME',        $url . '/homepage' );
define('URL_LOGIN',       $url . '/login' );
define('URL_REGISTER',    $url . '/register' );
define('URL_LOGOUT',      $url . '/homepage/logout' );
define('URL_VEHICLES',    $url . '/vehicles' );
define('URL_ADS',         $url . '/ads' );
define('URL_YELLOWPAGES', $url . '/yellow_pages' );
define('URL_PROFILE',     $url . '/profile' );
define('URL_SETTINGS',    $url . '/settings' );
define('URL_COSTS',       $url . '/costs' );
define('URL_STATISTICS',  $url . '/statistics' );

/* End of file ko_paths_helper.php */