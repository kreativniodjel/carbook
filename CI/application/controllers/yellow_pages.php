<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Yellow_pages extends CI_Controller
{
    function __construct() 
    {
        parent::__construct();
    }
    
    function index()
    {
        $data = array (
            'title' => 'Yellow pages'
        );
        $this -> load -> view('header_view', $data);
        
        $this -> load -> model('yellow_pages_model');
        $yp = $this -> yellow_pages_model -> get_yp();
        $data['yp'] = $yp;

        $service = $this -> yellow_pages_model -> get_yp_service();
        $data['service'] = $service;
        
        //$this -> load -> view('yellow_pages_view', $data);
        $this->render_template($data);

        $this -> load -> view('footer_view');
    }

    function render_template($data)
    {
        $data['img_path'] = base_url('/ypages') . '/';

        $view = $this->load->view('templates/table-yp', '', true);
        $template = $this->mustache->render($view, $data);
        $this->output->append_output( $template );
    }

}
/* End of file yellow_pages.php */