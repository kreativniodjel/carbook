<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Ads extends CI_Controller
{
    function __construct() 
    {
        parent::__construct();
    }
    
    function index()
    {
        $data = array (
            'title' => 'Mali oglasi'
        );
        $view = $this -> load -> view('header_view', $data, true);
        $this->output->append_output( $view );

        $this -> load -> model('ads_model');
        $ads = $this -> ads_model -> get_ads();
        if($ads === FALSE)
            $data['error'] = 'Trenutno ne postoji nijedan oglas.';

        $data['ads'] = $ads;
        $this->render_template($data);
        $view = $this -> load -> view('footer_view', '', true);
        $this->output->append_output( $view );
    }

    function render_template($data)
    {
        if( @$data['error'] )
            echo $data['error'];

        $ads = array();
        foreach($data['ads'] as $ad)
        {
            $ad['km']    = KO_number_format_km($ad['km']);
            $ad['price'] = KO_number_format_cost($ad['price']);
            array_push($ads, $ad);
        }
        $data['ads'] = $ads;

        $data['img_path'] = base_url('/gallery') . '/';

        $view = $this->load->view('templates/table-ads', '', true);
        $template = $this->mustache->render($view, $data);
        $this->output->append_output( $template );
    }

}
/* End of file ads.php */
