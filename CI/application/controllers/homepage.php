<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Homepage extends CI_Controller 
{
    function __construct() 
    {
        parent::__construct();
    }
    
    public function index()
    {
        $data = array(
            'title'             => 'Carbook'
        );
        $this -> load -> view('header_view', $data);
        $this -> load -> view('homepage_view');
        $this -> load -> view('footer_view');
    }
    
    public function logout()
    {
       $this -> session -> sess_destroy();
       redirect(site_url());  // redirect(base_url() . 'index.php/' . 'homepage'); 
    }
    
    public function register($id = FALSE, $username = FALSE)
    {
        $this -> load -> model('homepage_model');
        $result = $this -> homepage_model -> get_user($id, $username);
        
        // Redirect to clear url
        redirect(site_url());  // redirect(base_url() . 'index.php/' . 'homepage'); 
    }
}

/* End of file homepage.php */



