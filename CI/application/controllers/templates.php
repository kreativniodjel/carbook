<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Templates extends CI_Controller
{
    function __construct() 
    {
        parent::__construct();
    }
    
    function index()
    {
        $view = $this->load->view('templates/page-header', '', true);
        $this->output->append_output( $view );


        $file = 'modal-change-password';
        $data = array();
        $this->render_template($data, $file);


        $file = 'modal-register';
        $data = array();
        $this->render_template($data, $file);


        $file = 'modal-login';
        $data = array();
        $this->render_template($data, $file);


        $file = 'table-ads';
        $data = array(
            'ads' => array(
                array(
                    'thumb' => base_url('assets/img/placeholders/plavi-auto.jpg'),
                    'brand' => 'Nissan',
                    'type' => 'GTR',
                    'model' => '3.8 V6',
                    'year' => '2012',
                    'km' => '2.320',
                    'fuel' => 'BENZIN',
                    'location' => 'Rijeka',
                    'price' => '970.000'
                ),
                array(
                    'thumb' => base_url('assets/img/placeholders/plavi-auto.jpg'),
                    'brand' => 'Citroen',
                    'type' => 'Berlingo',
                    'model' => '1.6 HDI',
                    'year' => '2009',
                    'km' => '25.356',
                    'fuel' => 'DIZEL',
                    'location' => 'Rijeka',
                    'price' => '105.000'
                ),
                array(
                    'thumb' => base_url('assets/img/placeholders/plavi-auto.jpg'),
                    'brand' => 'Citroen',
                    'type' => 'Xsara',
                    'model' => '2.0 HDI',
                    'year' => '2003',
                    'km' => '152.617',
                    'fuel' => 'DIZEL',
                    'location' => 'Rijeka',
                    'price' => '37.500'
                )
            )
        );
        $this->render_template($data, $file);


        $file = 'table-costs-all';
        $data = array(
            'have_any_costs' => TRUE,
            'costs' => array(
                array(
                    'thumb' => base_url('assets/img/placeholders/plavi-auto.jpg'),
                    'name' => 'Citroen Xsara 2.0 HDI',
                    'date' => '14.05.2012.',
                    'km' => '153.212',
                    'type' => 'Ljetne gume',
                    'place_of_cost' => 'Vukal',
                    'note' => 'Lorem ipsum dolos amet',
                    'quantity' => '4',
                    'price' => '3.932',
                    'alarm_km' => '01.06.2012.'
                ),
                array(
                    'thumb' => base_url('assets/img/placeholders/plavi-auto.jpg'),
                    'name' => 'Citroen Berlingo 1.6 TDI',
                    'date' => '10.05.2012.',
                    'km' => '26.128',
                    'type' => 'Redovni servis',
                    'place_of_cost' => 'Ark Mihelić',
                    'note' => '',
                    'quantity' => '1',
                    'price' => '2.426,56',
                    'alarm_km' => '40.000 km'
                ),
                array(
                    'thumb' => base_url('assets/img/placeholders/plavi-auto.jpg'),
                    'name' => 'Nissan GTR',
                    'date' => '02.05.2012.',
                    'km' => '2.745',
                    'type' => 'Gorivo',
                    'place_of_cost' => 'OMV',
                    'note' => '',
                    'quantity' => '55',
                    'price' => '605',
                    'alarm_km' => '-'
                ),
                array(
                    'year_row' => TRUE,
                    'row_month' => '04',
                    'row_year' => '2012'
                ),
                array(
                    'thumb' => base_url('assets/img/placeholders/plavi-auto.jpg'),
                    'name' => 'Citroen Xsara 2.0 HDI',
                    'date' => '14.05.2012.',
                    'km' => '153.212',
                    'type' => 'Ljetne gume',
                    'place_of_cost' => 'Vukal',
                    'note' => 'Lorem ipsum dolos amet',
                    'quantity' => '4',
                    'price' => '3.932',
                    'alarm_km' => '01.06.2012.'
                ),
                array(
                    'thumb' => base_url('assets/img/placeholders/plavi-auto.jpg'),
                    'name' => 'Citroen Berlingo 1.6 TDI',
                    'date' => '10.05.2012.',
                    'km' => '26.128',
                    'type' => 'Redovni servis',
                    'place_of_cost' => 'Ark Mihelić',
                    'note' => '',
                    'quantity' => '1',
                    'price' => '2.426,56',
                    'alarm_km' => '40.000 km'
                ),
                array(
                    'thumb' => base_url('assets/img/placeholders/plavi-auto.jpg'),
                    'name' => 'Nissan GTR',
                    'date' => '02.05.2012.',
                    'km' => '2.745',
                    'type' => 'Gorivo',
                    'place_of_cost' => 'OMV',
                    'note' => '',
                    'quantity' => '55',
                    'price' => '605',
                    'alarm_km' => '-'
                )
            )
        );
        $this->render_template($data, $file);


        $file = 'table-costs-vehicle';
        $data = array(
            'have_any_costs' => TRUE,
            'costs' => array(
                array(
                    'thumb' => base_url('assets/img/placeholders/plavi-auto.jpg'),
                    'name' => 'Citroen Xsara 2.0 HDI',
                    'date' => '14.05.2012.',
                    'km' => '153.212',
                    'type' => 'Ljetne gume',
                    'place_of_cost' => 'Vukal',
                    'note' => 'Lorem ipsum dolos amet',
                    'quantity' => '4',
                    'price' => '3.932',
                    'alarm_km' => '01.06.2012.'
                ),
                array(
                    'thumb' => base_url('assets/img/placeholders/plavi-auto.jpg'),
                    'name' => 'Citroen Berlingo 1.6 TDI',
                    'date' => '10.05.2012.',
                    'km' => '26.128',
                    'type' => 'Redovni servis',
                    'place_of_cost' => 'Ark Mihelić',
                    'note' => '',
                    'quantity' => '1',
                    'price' => '2.426,56',
                    'alarm_km' => '40.000 km'
                ),
                array(
                    'thumb' => base_url('assets/img/placeholders/plavi-auto.jpg'),
                    'name' => 'Nissan GTR',
                    'date' => '02.05.2012.',
                    'km' => '2.745',
                    'type' => 'Gorivo',
                    'place_of_cost' => 'OMV',
                    'note' => '',
                    'quantity' => '55',
                    'price' => '605',
                    'alarm_km' => '-'
                )
            )
        );
        $this->render_template($data, $file);


        $file = 'modal-cost';
        $data = array(
            'cost-date' => '2012-05-25',
            'km' => '27.000',
            'type' => array(
                array('key' => '1', 'value' => 'Gorivo'),
                array('key' => '2', 'value' => 'Redovni servis'),
                array('key' => '3', 'value' => 'Ljetne gume'),
                array('key' => '4', 'value' => 'Zimske gume')
            ),
            'place' => 'OMV',
            'note' => 'Lorem ipsum dolor sit amet',
            'quantity' => '50',
            'price' => '500',
            'alarm-km' => '28.000',
            'alarm-date' => '2013-02-08'
        );
        $this->render_template($data, $file);


        $file = 'table-vehicles-ex';
        $data = array(
            'ex_vehicles' => array(
                array(
                    'thumbnail_name' => base_url('assets/img/placeholders/plavi-auto.jpg'),
                    'name' => 'Zastava 101',
                    'manuf_year' => '1987',
                    'licence_plate' => 'RI 101 AB',
                    'km' => '457.320',
                    'costs' => '187.429,38'
                ),
                array(
                    'thumbnail_name' => base_url('assets/img/placeholders/plavi-auto.jpg'),
                    'name' => 'Zator 16245 ZTS',
                    'manuf_year' => '1993',
                    'licence_plate' => 'RI 162 ZT',
                    'km' => '226.427',
                    'costs' => '55.114,27'
                )
            )
        );
        $this->render_template($data, $file);


        $file = 'table-vehicles-active';
        $data = array(
            'have_any_vehicle' => TRUE,
            'vehicles' => array(
                array(
                    'thumbnail_name' => base_url('assets/img/placeholders/plavi-auto.jpg'),
                    'name' => 'Nissan GTR',
                    'manuf_year' => '2012',
                    'licence_plate' => 'RI 767 FF',
                    'km' => '2.320',
                    'costs' => '24.750,12',
                    'alarms' => array()
                ),
                array(
                    'thumbnail_name' => base_url('assets/img/placeholders/plavi-auto.jpg'),
                    'name' => 'Citroen Berlingo 1.6 HDI',
                    'manuf_year' => '2009',
                    'licence_plate' => 'RI 456 RR',
                    'km' => '25.356',
                    'costs' => '12.456,57',
                    'alarms' => array(
                        'alarm' => 'alarmas'
                    )
                ),
                array(
                    'thumbnail_name' => base_url('assets/img/placeholders/plavi-auto.jpg'),
                    'name' => 'Citroen Xsara',
                    'manuf_year' => '2003',
                    'licence_plate' => 'RI 647 LC',
                    'km' => '152.617',
                    'costs' => '35.283,25',
                    'alarms' => array(
                        'alarm' => 'alarmas'
                    )
                )
            )
        );
        $this->render_template($data, $file);


        $file = 'modal-vehicle-add';
        $data = array(
            //'brand' => 'Seat',
            //'type' => 'Cordoba',
            //'model' => '1.9 HDI',
            'year' => '2000',
            //'km' => '165.327',
            //'plate' => 'RI524IC',
        );
        $this->render_template($data, $file);


        $file = 'modal-vehicle-sell';
        $data = array(
            'brand' => 'Seat',
            'type' => 'Cordoba',
            'model' => '1.9 HDI',
            'year' => '2000',
            'km' => '165.327',
            'plate' => 'RI524IC',
            'fuel' => 'Benzin',
            'location' => 'Rijeka',
            'price' => '850.000'
        );
        $this->render_template($data, $file);


        $file = 'floater-cost';
        $data = array(
            'title' => 'Gorivo: OMV'
        );
        $this->render_template($data, $file);


        $file = 'floater-cost-alarm';
        $data = array();
        $this->render_template($data, $file);


        $file = 'floater-cost-note';
        $data = array(
            'text' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi feugiat purus vel urna elementum ac ornare ante dictum. Mauris tortor orci, pharetra a molestie eget, vehicula id nisi. Donec dignissim lacinia molestie. Donec sagittis accumsan mauris quis lacinia. Sed posuere scelerisqegestas.'
        );
        $this->render_template($data, $file);


        $file = 'floater-user';
        $data = array(
            'img' => base_url() . 'assets/img/placeholders/user.png',
            'full-name' => 'Fabrizio Mascalzone'
        );
        $this->render_template($data, $file);


        $file = 'floater-vehicle';
        $data = array(
            'img' => base_url() . 'assets/img/placeholders/zastava.jpg',
            'title' => 'Zastava 101'
        );
        $this->render_template($data, $file);


        $file = 'floater-vehicle-ex';
        $data = array(
            'img' => base_url() . 'assets/img/placeholders/zastava.jpg',
            'title' => 'Zastava 101'
        );
        $this->render_template($data, $file);


        $file = 'floater-vehicle-photo';
        $data = array(
            'img' => base_url() . 'assets/img/placeholders/plavi-auto.jpg'
        );
        $this->render_template($data, $file);


        $file = 'table-user-personal-data';
        $data = array(
            'user_img' => base_url() . 'assets/img/placeholders/user.png',
            'db_username' => 'Fannullone',
            'password' => 'Fannullone',
            'db_name' => 'Fabrizio',
            'db_lastname' => 'Mascalzone',
            'db_email' => 'fabrizio.mascalzone@gmail.com',
            'db_address' => 'Via Franco Farfalla 42',
            'db_city' => 'Milano',
            'db_postal_code' => '20121',
            'country_list' => array(
                    array('key' => '1', 'value' => 'USA', 'selected' => ''),
                    array('key' => '0', 'value' => 'Croatia (Hrvatska)', 'selected' => 'selected'),
                    array('key' => '2', 'value' => 'United Kingdom', 'selected' => '')
                ),
            'db_phone' => '+44 21 7894 365',
            'db_birthday' => '1977-07-26'
        );
        $this->render_template($data, $file);


        $file = 'table-user-hak';
        $data = array(
            'db_number' => '361557000729',
            'db_valid_thru' => '2012-09-17',
            'db_type' => 'TIP #1'
        );
        $this->render_template($data, $file);


        $file = 'table-user-driver-license';
        $data = array();
        $this->render_template($data, $file);


        $file = 'table-vehicle';
        $data = array(
            'db_thumbnail_name' => base_url() . 'assets/img/placeholders/plavi-auto.jpg',
            'db_name' => 'Nissan GTR',
            'db_licence_plate' => 'RI767FF'
        );
        $this->render_template($data, $file);


        $file = 'table-vehicle-license';
        $data = array(
            'db_class' => 'OSOBNI AUTOMOBIL',
            'db_brand' => 'NISSAN',
            'db_type' => 'GTR',
            'db_model' => '3.8 V6',
            'db_color' => 'DEEP BLUE PEARL',
            'db_num_of_chassis' => 'FAW75200001052669',
            'db_body_shape' => 'ZATVORENI',
            'db_manuf_country' => 'JAPAN',
            'db_manuf_year' => '2012',
            'db_date_first_reg' => '2012-03-15',
            'db_seat_places' => 'OSOBNI AUTOMOBIL',
            'db_carrying_capacity' => '-',
            'db_mass' => '1730',
            'db_max_speed' => '320',
            'db_engine_type' => 'BENZIN (TWIN TURBO)',
            'db_power' => '420',
            'db_cubic_capacity' => '3800',
            'db_length' => '467',
            'db_width' => '190',
            'db_height' => '137',
            'db_lowest_point' => '-',
            'db_num_of_wheels' => '4',
            'db_tyres_front' => '255/40ZRF20',
            'db_tyres_back' => '285/35ZRF20',
            'db_brake_type' => '-',
            'db_num_of_axles' => '2',
            'db_driving_axles' => '2'
        );
        $this->render_template($data, $file);


        $file = 'table-vehicle-lights';
        $data = array(
            'db_short_lights' => 'HID',
            'db_high_lights' => 'HID',
            'db_stop_lights' => 'LED'
        );
        $this->render_template($data, $file);


        $file = 'table-vehicle-fuel';
        $data = array();
        $this->render_template($data, $file);


        $file = 'table-vehicle-gallery';
        $data = array(
            'pictures' => array(
                array(
                    'path' => base_url() . 'assets/img/placeholders/plavi-auto-gallery-1.png',
                    'caption' => 'Sed aliquet gravida sem, in tristique nisl elefend newc.'
                ),
                array(
                    'path' => base_url() . 'assets/img/placeholders/plavi-auto-gallery-1.png',
                    'caption' => 'Sed aliquet gravida sem, in tristique nisl elefend newc.'
                ),
                array(
                    'path' => base_url() . 'assets/img/placeholders/plavi-auto-gallery-1.png',
                    'caption' => 'Sed aliquet gravida sem, in tristique nisl elefend newc.'
                )
            )
        );
        $this->render_template($data, $file);


        $file = 'table-yp';
        $data = array(
            'yp' => array(
                array(
                    'thumbnail_path' => base_url('assets/img/placeholders/yp-ac.jpg'),
                    'name' => 'Autocommerce, Adresa 42, 51000 Rijeka • 051 556 556 • autocommerce@fiat.hr',
                    'text' => 'Fiat, Alfa Romeo, Lancia. Gospodarska vozila. Zastupstvo i servis.'
                ),
                array(
                    'thumbnail_path' => base_url('assets/img/placeholders/yp-renault.jpg'),
                    'name' => 'Renault Hrvatska, Izmišljena Adresa 58, 51000 Rijeka • 01 3365 777 • info@renault.hr',
                    'text' => 'Prodaja Renault vozila, servis.'
                ),
                array(
                    'thumbnail_path' => base_url('assets/img/placeholders/yp-vw.jpg'),
                    'name' => 'Volkswagen Hrvatska, Tajna lokacija 51, 10000 Zagreb • 01 5783 496 • prodaja@volkswagen.hr',
                    'text' => 'Postanite i vi dijelom Volkswagen obitelji!'
                ),
                array(
                    'banner_path' => base_url('assets/img/placeholders/yp-nissan-banner.jpg')
                ),
                array(
                    'thumbnail_path' => base_url('assets/img/placeholders/yp-nissan.jpg'),
                    'name' => 'AK Lenac, Ulica 22, 51000 Rijeka • 051 541 778 • auto.kuca.lenac@ri.t-com.hr',
                    'text' => 'Shift expectations'
                ),
                array(
                    'thumbnail_path' => base_url('assets/img/placeholders/yp-mazda.jpg'),
                    'name' => 'Mazda Hrvatska, Dr. Franje Tuđmana 91, 10000 Zagreb • 01 4384 834 • mazda.info@mazda.hr',
                    'text' => 'Zoom Zoom'
                )
            )
        );
        $this->render_template($data, $file);


        $view = $this->load->view('templates/page-footer', '', true);
        $this->output->append_output( $view );
    }


    function render_template($data, $file)
    {
        $this->output->append_output( "<div name='$file'>" );

        $view = $this->load->view('templates/'.$file, '', true);
        $template = $this->mustache->render($view, $data);
        $this->output->append_output( $template );

        $this->output->append_output( "</div>" );
    }


}

/* End of file Templates.php */