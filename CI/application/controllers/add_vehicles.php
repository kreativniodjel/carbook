<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Add_vehicles extends CI_Controller
{
    function __construct() 
    {
        parent::__construct();
    }
    
    function index()
    {
        if ( $this -> input -> post('cancel_button') )
        {
            redirect(base_url() . 'index.php/' . 'vehicles'); 
        }    
        
        
        $data = array(
            'title'                 => 'Dodaj vozilo',
            'name_text'             => 'Naziv:',
            'year_text'             => 'God. proizvodnje:',
            'licence_plate_text'    => 'Reg. oznaka:',
            'km_text'               => 'Kilometri:',
            'add_button'            => 'POTVRDI',
            'cancel_button'         => 'ODUSTANI'
        );
        
        // Form validation and custom error messages
        $this -> form_validation -> set_rules('name',           'naziv vozila',     'strip_tags|trim|required|xss_clean');
        $this -> form_validation -> set_rules('year',           'god. proizvodnje', 'strip_tags|trim|required|xss_clean|exact_length[4]');
        $this -> form_validation -> set_rules('licence_plate',  'reg. oznaka',      'strip_tags|trim|required|xss_clean|alpha_numeric');
        $this -> form_validation -> set_rules('km',             'kilometri',        'strip_tags|trim|required|xss_clean|integer');
        $this -> form_validation -> set_error_delimiters('<div class="error-form-valid">*', '</div>');
        // If there is a error on form validation show it
        if ($this-> form_validation -> run() === FALSE)
        {
            $this -> load -> view('header_view', $data);
            $this -> load -> view('add_vehicles_view');
            $this -> load -> view('footer_view');
        }
        else
        {
            
            $post_data = array(
                'name'          => $this -> input -> post('name'),
                'year'          => $this -> input -> post('year'),
                'licence_plate' => $this -> input -> post('licence_plate'),
                'km'            => $this -> input -> post('km'),
                'user_id'       => $this -> session -> userdata ('user_id')
            );
            $error = 'Pogreška pri dodavanju vozila!';
            
            // Check if manuf. year is proper value
            if($post_data['year'] >= 1950 && $post_data['year'] <= date("Y"))
            {
                $this -> load -> model('add_vehicles_model');
                $result = $this -> add_vehicles_model -> add_vehicles($post_data);
            
                // If user successfully login redirect back to homepage
                if($result != FALSE)
                {
                    $this -> session -> set_userdata('cur_vehicle_id', $result);
                    redirect(base_url() . 'index.php/' . 'vehicles_licences'); 
                }
            }
            else 
            {
                $error = 'Godina proizvodnje nije ispravna!';
            }
            echo $error;
                
            $this -> load -> view('header_view', $data);
            $this -> load -> view('add_vehicles_view');
            $this -> load -> view('footer_view');
            
        }
    }
}

/* End of file add_vehicles.php */