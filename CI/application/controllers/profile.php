<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Profile extends CI_Controller
{
    function __construct() 
    {
        parent::__construct();
    }
    
    function index()
    {
        $data_header = array (
            'title'         => 'Korisnički profil',
            'profile_js'    => TRUE
        );

        $view = $this -> load -> view('header_view', $data_header, true);
        $this->output->append_output( $view );

        $data = array (
            'title'             => 'Korisnički profil',
            'submit_button'     => 'Promijeni',
            'username_text'     => 'USERNAME',
            'password_text'     => 'LOZINKA',
            'name_text'         => 'IME',
            'lastname_text'     => 'PREZIME',
            'email_text'        => 'E-MAIL',
            'phone_text'        => 'TELEFON',
            'address_text'      => 'ULICA I KUĆNI BROJ',
            'city_text'         => 'GRAD',
            'postal_code_text'  => 'ZIP',
            'country_text'      => 'DRŽAVA',
            'birthday_text'     => 'DATUM ROĐENJA',
            'gender_text'       => 'SPOL',
            'a1_text'           => 'A1',
            'a2_text'           => 'A2',
            'a_text'            => 'A',
            'b_text'            => 'B',
            'be_text'           => 'B+E',
            'ci_text'           => 'CI',
            'cie_text'          => 'CI+E',
            'c_text'            => 'C',
            'ce_text'           => 'C+E',
            'd1_text'           => 'D1',
            'd1e_text'          => 'D1+E',
            'd_text'            => 'D',
            'de_text'           => 'D+E',
            'f_text'            => 'F',
            'g_text'            => 'G',
            'h_text'            => 'H',
            'am_text'           => 'AM',
            'number_text'       => 'ČLANSKI BROJ',
            'type_text'         => 'TIP ČLANSTVA',
            'valid_thru_text'   => 'VRIJEDI DO',
            'submit_psw_button' => 'Promijeni lozinku',
            
            'db_username'       => '',
            'db_password'       => '',
            'db_name'           => '',
            'db_lastname'       => '',
            'db_email'          => '',
            'db_phone'          => '',
            'db_address'        => '',
            'db_city'           => '',
            'db_postal_code'    => '',
            'db_country'        => '',
            'db_birthday'       => '',
            'db_gender'         => '',
               
            'db_a1'             => '',
            'db_a2'             => '',
            'db_a'              => '',
            'db_b'              => '',
            'db_be'             => '',
            'db_ci'             => '',
            'db_cie'            => '',
            'db_c'              => '',
            'db_ce'             => '',
            'db_d1'             => '',
            'db_d1e'            => '',
            'db_d'              => '',
            'db_de'             => '',
            'db_f'              => '',
            'db_g'              => '',
            'db_h'              => '',
            'db_am'             => '',
            
            'db_number'         => '',
            'db_type'           => '',
            'db_valid_thru'     => '',
            'show_message'      => FALSE
            
        );


        $user_id = $this -> session -> userdata('user_id');
        $this -> load -> model('profile_model');
        $this -> form_validation -> set_error_delimiters('<div class="error-form-valid">*', '</div>');
        //- First check if form is submited
        if ($this -> input -> post('submit_psw_profile')) 
        {
            echo "1";
        }
        else if($this -> input -> post('submit_form_profile'))
        {
            // Form validation and custom error messages
            $this -> form_validation -> set_rules('name',               'name',                 'strip_tags|trim|xss_clean');
            $this -> form_validation -> set_rules('lastname',           'lastname',             'strip_tags|trim|xss_clean');
            $this -> form_validation -> set_rules('email',              'email',                'strip_tags|trim|required|xss_clean|valid_email|min_length[5]');
            $this -> form_validation -> set_rules('phone',              'phone',                'strip_tags|trim|xss_clean');
            $this -> form_validation -> set_rules('address',            'address',              'strip_tags|trim|xss_clean');
            $this -> form_validation -> set_rules('city',               'city',                 'strip_tags|trim|xss_clean');
            $this -> form_validation -> set_rules('postal_code',        'postal_code',          'strip_tags|trim|xss_clean|alpha_numeric');
            $this -> form_validation -> set_rules('country',            'country',              'strip_tags|trim|xss_clean');
            $this -> form_validation -> set_rules('birthday',           'birthday',             'strip_tags|trim|xss_clean');
            $this -> form_validation -> set_rules('gender',             'gender',               'strip_tags|trim|xss_clean');
            
            
            // If all data is proper add it to database
            if ($this-> form_validation -> run() === TRUE)
            {
                $data['id_user']            = $user_id;
                $data['db_name']            = $this -> input -> post('name');
                $data['db_lastname']        = $this -> input -> post('lastname');
                $data['db_email']           = $this -> input -> post('email');
                $data['db_phone']           = $this -> input -> post('phone');
                $data['db_address']         = $this -> input -> post('address');
                $data['db_city']            = $this -> input -> post('city');
                $data['db_postal_code']     = $this -> input -> post('postal_code');
                $data['db_country']         = $this -> input -> post('country');
                $data['db_birthday']        = $this -> input -> post('birthday');
                $data['db_gender']          = $this -> input -> post('gender');
                
                $this -> profile_model -> update_user_profile($data);
            }

        }
        else if($this -> input -> post('submit_form_driver_licence'))
        {
            // If all data is proper add it to database
            $driver_lic                       = array();
            $driver_lic['id_user']            = $user_id;
            $driver_lic['db_a1']              = $this -> input -> post('a1');
            $driver_lic['db_a2']              = $this -> input -> post('a2');
            $driver_lic['db_a']               = $this -> input -> post('a');
            $driver_lic['db_b']               = $this -> input -> post('b');
            $driver_lic['db_be']              = $this -> input -> post('be');
            $driver_lic['db_ci']              = $this -> input -> post('ci');
            $driver_lic['db_cie']             = $this -> input -> post('cie');
            $driver_lic['db_c']               = $this -> input -> post('c');
            $driver_lic['db_ce']              = $this -> input -> post('ce');
            $driver_lic['db_d1']              = $this -> input -> post('d1');
            $driver_lic['db_d1e']             = $this -> input -> post('d1e');
            $driver_lic['db_d']               = $this -> input -> post('d');
            $driver_lic['db_de']              = $this -> input -> post('de');
            $driver_lic['db_f']               = $this -> input -> post('f');
            $driver_lic['db_g']               = $this -> input -> post('g');
            $driver_lic['db_h']               = $this -> input -> post('h');
            $driver_lic['db_am']              = $this -> input -> post('am');

            $this -> profile_model -> update_licence($driver_lic);
        }
        else if($this -> input -> post('submit_form_hak'))
        {
            $hak                            = array();
            $hak['id_user']                 = $user_id;
            $hak['number']                  = $this -> input -> post('number');
            $hak['type']                    = $this -> input -> post('type');
            $hak['valid_thru']              = $this -> input -> post('valid_thru');

            $this -> profile_model -> update_hak($hak);
        }
        else if($this -> input -> post('old_password'))
        {
            $this -> form_validation -> set_rules('old_password',       'old password',     'trim|required|min_length[5]|md5|xss_clean');
            $this -> form_validation -> set_rules('new_password',       'new password',     'trim|required|md5|min_length[5]|xss_clean');
            $this -> form_validation -> set_rules('repeat_password',    'repeat password',  'trim|required|matches[new_password]|md5|min_length[5]|xss_clean'); 
            
            if ($this-> form_validation -> run() === TRUE)
            {
                $reset_password                         = array();
                $reset_password['id_user']              = $user_id;
                $reset_password['old_password']         = $this -> input -> post('old_password');
                $reset_password['new_password']         = $this -> input -> post('new_password');
                $reset_password['repeat_password']      = $this -> input -> post('repeat_password');
            
                $ret_val = $this -> profile_model -> go_reset_password($reset_password);
                $data['show_message'] = TRUE;
                $data['show_message_val'] = $ret_val;
            }
            else
            {
                $data['show_message'] = TRUE;
                $data['show_message_val'] = -3;
            }
        }
       
        $profile = $this -> profile_model -> get_profile($user_id);
        $gender  = $this -> profile_model -> get_gender();
        $country = $this -> profile_model -> get_country();
        if($profile === FALSE)
        {
            $data['error'] = 'Pogreška.';
        }
        else
        {
            $data['gender']             = $gender;
            $data['country']            = $country;
            //- Set data from database
            $data['db_username']        = $profile[0]['username'];      
            $data['db_password']        = $profile[0]['password'];   
            $data['db_name']            = $profile[0]['name'];   
            $data['db_lastname']        = $profile[0]['lastname'];   
            $data['db_email']           = $profile[0]['email'];   
            $data['db_phone']           = $profile[0]['phone'];   
            $data['db_address']         = $profile[0]['address'];   
            $data['db_city']            = $profile[0]['city'];   
            $data['db_postal_code']     = $profile[0]['postal_code'];   
            $data['db_country']         = $profile[0]['FK_idcountry'];
            $data['db_birthday']        = (KO_date_exist($profile[0]['birthday']) === FALSE) ? '' : $profile[0]['birthday'];
            $data['db_gender']          = $profile[0]['FK_idgender'];
               
            $data['db_a1']          = $profile[0]['A1']; 
            $data['db_a2']          = $profile[0]['A2']; 
            $data['db_a']           = $profile[0]['A']; 
            $data['db_b']           = $profile[0]['B']; 
            $data['db_be']          = $profile[0]['BE']; 
            $data['db_ci']          = $profile[0]['CI']; 
            $data['db_cie']         = $profile[0]['CIE']; 
            $data['db_c']           = $profile[0]['C']; 
            $data['db_ce']          = $profile[0]['CE']; 
            $data['db_d1']          = $profile[0]['D1']; 
            $data['db_d1e']         = $profile[0]['D1E']; 
            $data['db_d']           = $profile[0]['D']; 
            $data['db_de']          = $profile[0]['DE']; 
            $data['db_f']           = $profile[0]['F']; 
            $data['db_g']           = $profile[0]['G']; 
            $data['db_h']           = $profile[0]['H']; 
            $data['db_am']          = $profile[0]['AM']; 
            
            $data['db_number']          = $profile[0]['number']; 
            $data['db_type']            = $profile[0]['type']; 
            $data['db_valid_thru']      = (KO_date_exist($profile[0]['valid_thru']) === FALSE) ? '' : $profile[0]['valid_thru'];
        } 
        $data['profile'] = $profile;

        //echo $this -> load -> view('profile_view', $data);

        $this->render_template($data);

        $view = $this -> load -> view('footer_view', '', true);
        $this->output->append_output( $view );
    }

    function render_template($data)
    {

        $data['country_list'] = array();
        foreach($data['country'] AS $key => $value) {
            $each['key']      = $key;
            $each['value']    = $value;
            $each['selected'] = ( $data['db_country'] === $key ) ? 'selected' : '';
            array_push( $data['country_list'], $each );
        }

        $data['user_img']               = base_url() . 'assets/img/placeholders/user.png';

        $data['gender_male_selected']   = ( $data['db_gender'] === 'M' ) ? 'selected' : '';
        $data['gender_female_selected'] = ( $data['db_gender'] === 'F' ) ? 'selected' : '';

        $view = $this->load->view('templates/table-user-personal-data', false, true);
        $template = $this->mustache->render($view, $data);
        $this->output->append_output( $template );

        $this->output->append_output( "<br><br>" );

        $data['a1_checked']  = ($data['db_a1']   ==  1)  ? 'checked' : '';
        $data['a2_checked']  = ($data['db_a2']   ==  1)  ? 'checked' : '';
        $data['a_checked']   = ($data['db_a']    ==  1)  ? 'checked' : '';
        $data['b_checked']   = ($data['db_b']    ==  1)  ? 'checked' : '';
        $data['be_checked']  = ($data['db_be']   ==  1)  ? 'checked' : '';
        $data['ci_checked']  = ($data['db_ci']   ==  1)  ? 'checked' : '';
        $data['cie_checked'] = ($data['db_cie']  ==  1)  ? 'checked' : '';
        $data['c_checked']   = ($data['db_c']    ==  1)  ? 'checked' : '';
        $data['ce_checked']  = ($data['db_ce']   ==  1)  ? 'checked' : '';
        $data['d1_checked']  = ($data['db_d1']   ==  1)  ? 'checked' : '';
        $data['d1e_checked'] = ($data['db_d1e']  ==  1)  ? 'checked' : '';
        $data['d_checked']   = ($data['db_d']    ==  1)  ? 'checked' : '';
        $data['de_checked']  = ($data['db_de']   ==  1)  ? 'checked' : '';
        $data['f_checked']   = ($data['db_f']    ==  1)  ? 'checked' : '';
        $data['g_checked']   = ($data['db_g']    ==  1)  ? 'checked' : '';
        $data['h_checked']   = ($data['db_h']    ==  1)  ? 'checked' : '';
        $data['am_checked']  = ($data['db_am']   ==  1)  ? 'checked' : '';
        $view = $this->load->view('templates/table-user-driver-license', false, true);
        $template = $this->mustache->render($view, $data);
        $this->output->append_output( $template );

        $this->output->append_output( "<br><br>" );

        $view = $this->load->view('templates/table-user-hak', false, true);
        $template = $this->mustache->render($view, $data);
        $this->output->append_output( $template );

    }

}
/* End of file profile.php */


