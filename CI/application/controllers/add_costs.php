<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Add_costs extends CI_Controller
{
    function __construct() 
    {
        parent::__construct();
    }
    
    function index()
    {
        $id_costs           = -1;
        $id_costs_details   = -1;
  
        // If user clicked Cancel button, return
        if ( $this->input->post('cancel_button') )
        {
            if($this -> session -> userdata('costs_id') != -1)
            {
                $this -> session -> unset_userdata('costs_id');
                $this -> session -> unset_userdata('costs_details_id');
            }
            
            redirect(base_url() . 'index.php/' . 'costs'); 
        }    
        
        $data = array(
            'title'                 => 'TROŠKOVI',
            'date_text'             => 'DATUM',
            'year_text'             => 'GOD.',
            'km_text'               => 'KM',
            'type_text'             => 'VRSTA',
            'place_of_cost_text'    => 'MJESTO TROŠKA',
            'note_text'             => 'NAPOMENA',
            'quantity_text'         => 'KOLIČINA',
            'price_text'            => 'CIJENA(KN)',
            'alarm_km_text'         => 'ALARM(KM)',
            'alarm_date_text'       => 'ALARM(DATUM)',
            'cancel_button'         => 'ODUSTANI',
            'submit_button'         => 'POTVRDI',
            'db_date'               => '',
            'db_year'               => '',
            'db_year_alarm'         => '',
            'db_alarm_date'         => '',  
            'db_km'                 => '',
            'db_place_of_cost'      => '',
            'db_note'               => '',
            'db_quantity'           => '',
            'db_price'              => '',
            'db_alarm_km'           => '',
            'edit_mode'             => FALSE
        );
        
        // Get all costs types from database so we can add them to dropdown box
        $this -> load -> model('add_costs_model');
        $type_options = $this -> add_costs_model -> get_costs_types();
        $data['type_options']       = $type_options;
        // Set current selected item in dropdown box
        $curr_type_options          = $this -> input -> post('type_options');
        $data['curr_type_options']  = $curr_type_options;
        
        // Get current cost if editing cost
        if($this -> session -> userdata('costs_id'))
        {
            
            $id_costs           = $this -> session -> userdata('costs_id');
            $id_costs_details   = $this -> session -> userdata('costs_details_id');
            $cost               = $this -> add_costs_model -> get_cost($id_costs_details);
            
            if($cost != FALSE)
            {
                $data['edit_mode']          =   TRUE;
                
                if(KO_date_exist($cost[0]['date']))
                    $data['db_date']        = date("m/d/Y", strtotime($cost[0]['date']));
                if(KO_date_exist($cost[0]['alarm_date']))
                    $data['db_alarm_date']  = date("m/d/Y", strtotime($cost[0]['alarm_date']));
                
                $data['db_km']              =   $cost[0]['km'];
                $data['db_place_of_cost']   =   $cost[0]['place_of_cost'];
                $data['db_note']            =   $cost[0]['note'];
                $data['db_quantity']        =   $cost[0]['quantity'];
                $data['db_price']           =   $cost[0]['price'];
                $data['db_alarm_km']        =   $cost[0]['alarm_km'];

                $data['curr_type_options']  =   $cost[0]['FK_idcosts_types'];
            }
        }
        
        // if not edit mode check if vehicle exist!
        if($data['edit_mode'] === FALSE)
        {
            if($this -> session -> userdata('cur_vehicle_id') === 0)
                redirect(base_url() . 'index.php/' . 'costs'); 
        }
       
        // Form validation and custom error messages
        $this -> form_validation -> set_rules('date',               'naziv vozila',     'strip_tags|trim|required|xss_clean');
        //$this -> form_validation -> set_rules('year',               'god. proizvodnje', 'strip_tags|trim|required|xss_clean|exact_length[4]');
        $this -> form_validation -> set_rules('km',                 'km',               'strip_tags|trim|required|xss_clean|integer');
        $this -> form_validation -> set_rules('place_of_cost',      'mjesto troška',    'strip_tags|trim|required|xss_clean');
        $this -> form_validation -> set_rules('note',               'napomena',         'strip_tags|trim|xss_clean');
        $this -> form_validation -> set_rules('quantity',           'količina',         'strip_tags|trim|required|xss_clean|integer');
        $this -> form_validation -> set_rules('price',              'cijena',           'strip_tags|trim|required|xss_clean|decimal(2)');
        $this -> form_validation -> set_rules('alarm_km',           'alarm km',         'strip_tags|trim|xss_clean|integer');
        $this -> form_validation -> set_rules('alarm_date',         'alarm datum',      'strip_tags|trim|xss_clean');
        //$this -> form_validation -> set_rules('alarm_year',         'alarm godina',     'strip_tags|trim|xss_clean|integer');
        $this -> form_validation -> set_error_delimiters('<div class="error-form-valid">*', '</div>');
        
        // If there is a error on form validation show it
        if ($this-> form_validation -> run() === FALSE)
        {
            $this -> load -> view('header_view', $data);
            $this -> load -> view('add_costs_view');
            $this -> load -> view('footer_view');
        }
        else
        {
           $post_data = array(
                'date'          => $this -> input -> post('date'),
                'km'            => $this -> input -> post('km'),
                'type'          => $this -> input -> post('type_options'),
                'place_of_cost' => $this -> input -> post('place_of_cost'),
                'note'          => $this -> input -> post('note'),
                'price'         => $this -> input -> post('price'),
                'quantity'      => $this -> input -> post('quantity'),
                'alarm_km'      => $this -> input -> post('alarm_km'),
                'alarm_date'    => $this -> input -> post('alarm_date')
            );
            $error = 'Pogreška pri dodavanju troška!';
           
            // Check if year are proper values
            $result = FALSE;
            if($data['edit_mode'] === TRUE)
                $result = $this -> add_costs_model -> edit_costs($post_data, $id_costs);
            else
                $result = $this -> add_costs_model -> add_costs($post_data, $id_costs);
            // If user successfully login redirect back to homepage
            if($result === TRUE)
            {
               if($this -> session -> userdata('costs_id') != -1)
               {
                    $this -> session -> unset_userdata('costs_id');
                    $this -> session -> unset_userdata('costs_details_id');
               }
               redirect(base_url() . 'index.php/' . 'costs'); 
            }
            echo $error;
                
            $this -> load -> view('header_view', $data);
            $this -> load -> view('add_costs_view');
            $this -> load -> view('footer_view');
        }
    }
}

/* End of file add_vehicles.php */