<?php 
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Costs extends CI_Controller
{
    function __construct() 
    {
        parent::__construct();
    }
    
    public function index()
    {
        $ex_vehicle = $this -> session -> userdata('ex_vehicle');    
        $this -> session -> set_userdata('ex_vehicle');    
        
        $data = array (
            'title'             => 'Troškovi',
            'costs_table_js'    => TRUE,
            'enable_changes'    => FALSE,
            'error'             => '',
            'show_year'         => 1,
            'show_sidebar'      => FALSE
        );
        if($ex_vehicle == TRUE)
            $data['show_ex_vehicles'] = $ex_vehicle;
        
        $view = $this -> load -> view('header_view', $data, true);
        $this->output->append_output( $view );
        
        if($this -> input -> post('show_year') == 1)
        {
            $data['show_year'] = 0;
        }
        
        
        $user_id        = $this -> session -> userdata('user_id');
        $vehicle_id     = $this -> session -> userdata('cur_vehicle_id');
        
        if(empty($vehicle_id))
        {
            $vehicle_id = 0;
            $this -> session -> set_userdata('cur_vehicle_id', $vehicle_id);
        }
        
        $this -> load -> model('costs_model');
        $costs          = array();
        if ($vehicle_id === 0)
        {
            $view_type  = 0;
            $costs      = $this -> costs_model -> get_all_costs($user_id);
        }
        else 
        {
            $view_type  = 1;
            $costs      = $this -> costs_model -> get_vehicles_costs($user_id, $vehicle_id);
        }
        
        $data['enable_changes'] = $this -> costs_model -> check_if_vehicle_data_can_be_changed($vehicle_id);
        
        if(($costs === FALSE)&&($data['enable_changes'] == TRUE))
        {
            if ($vehicle_id === 0)
                $data['error'] = 'Ne postoje podaci o troškovima za vozila! Izaberite vozilo i dodajte trošak za vozilo.';
            //else
                //$data['error'] = 'Ne postoje podaci o troškovima za odabrano vozilo! Dodajte trošak za vozilo.';
        }

        $data['costs']      = $costs;
        $data['view_type']  = $view_type;
        //print_r($costs);
        //$this -> load -> view('costs_view', $data);

        $this->output->append_output( $data['error'] );

        $this->render_template($data);

        $view = $this -> load -> view('footer_view', '', true);
        $this->output->append_output( $view );
    }
    
    
    function set_vehicles($sended_idvehicles = FALSE, $ex_vehicles = FALSE)
    {
        if($sended_idvehicles != FALSE)
        {
            $this -> session -> set_userdata('cur_vehicle_id', $sended_idvehicles);
            $this -> session -> set_userdata('ex_vehicle', $ex_vehicles);
        }
        redirect(base_url() . 'index.php/' . 'costs'); 
    }
    

    function edit($id=NULL, $id_costs_details=NULL)
    {
        $this -> session -> set_userdata('costs_id', $id);
        $this -> session -> set_userdata('costs_details_id', $id_costs_details);
        redirect(base_url() . 'index.php/' . 'add_costs'); 
    }
    

    function delete($id=NULL)
    {
        $this -> load -> model('costs_model');
        $this -> costs_model -> delete_cost($id);
        redirect(base_url() . 'index.php/' . 'costs'); 
    }


    function render_template($data)
    {
        if( $data['error'] !== '' ) return;

        if( $data['costs'] !== FALSE )
        {
            $data['have_any_costs'] = TRUE;
            
            $costs = array();
            $curr_month = -1;
            $curr_year  = -1;
            foreach($data['costs'] as $cost)
            {
                $cost['km']       = KO_number_format_km($cost['km']);
                $cost['price']    = KO_number_format_cost($cost['price']);
                $cost['alarm_km'] = KO_number_format_km($cost['alarm_km']);
                
                if($data['show_year'] == 0)
                {
                    if(($cost['month'] != $curr_month)||($curr_year != $cost['year']))
                    {
                        $curr_month = $cost['month'];
                        $curr_year  = $cost['year'];

                        array_push($costs, array(
                                'year_row' => TRUE,
                                'row_month' => $curr_month,
                                'row_year' => $curr_year
                            )
                        );
                    }
                }
                array_push($costs, $cost);
            }
            $data['costs'] = $costs;
        }

        $data['show_year_class'] = ($data['show_year'] === 0) ? ' active' : '';
        $data['img_path'] = base_url('/data/gallery') . '/';

        if( $data['view_type'] === 0 )
            $view = $this->load->view('templates/table-costs-all', '', true);
        else
            $view = $this->load->view('templates/table-costs-vehicle', '', true);

        $template = $this->mustache->render($view, $data);
        $this->output->append_output( $template );
    }

}

/* End of file costs.php */