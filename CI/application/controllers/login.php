<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Login extends CI_Controller
{
    function __construct() 
    {
        parent::__construct();
    }
    
    public function index()
    {
        if ( $this -> input -> post('cancel_button') )
        {
            redirect(base_url() . 'index.php/' . 'homepage'); 
        } 
        
        // Form validation and custom error messages
        $this -> form_validation -> set_rules('username', 'Korisničko ime', 'strip_tags|trim|required|xss_clean');
        $this -> form_validation -> set_rules('password', 'Lozinka', 'strip_tags|trim|required|md5|xss_clean');
        $this -> form_validation -> set_error_delimiters('<div class="error-form-valid">*', '</div>');
        $data = array(
            'title'         => 'Prijavi se',
            'login_text'    => 'Korisničko ime:',
            'login_button'  => 'Potvrdi',
            'cancel_button' => 'Odustani',
            'password_text' => 'Lozinka:'
        );
        
        // If there is a error on form validation show it
        if ($this-> form_validation -> run() === FALSE)
        {
            $this -> load -> view('header_view', $data);
            $this -> load -> view('login_view');
            $this -> load -> view('footer_view');
        }
        else 
        {
            $username = $this -> input -> post ('username');
            $password = $this -> input -> post ('password');
            
            $this -> load -> model('login_model');
            $result = $this -> login_model -> get_user($username, $password);
           
            // If user successfully login redirect back to homepage
            if($result === TRUE)
            {
                redirect(base_url() . 'index.php/' . 'homepage'); 
            }
            
            $this -> load -> view('header_view', $data);
            $this -> load -> view('login_view');
            $this -> load -> view('footer_view');
        }
       
        
        
        
    }
}
/* End of file login.php */