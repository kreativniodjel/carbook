<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Register extends CI_Controller
{
    function __construct() 
    {
        parent::__construct();
    }
    
   function index()
   {
        if ( $this -> input -> post('cancel_button') )
        {
            redirect(base_url() . 'index.php/' . 'homepage'); 
        } 
       
       $data = array (
           'title'              => 'Registriraj se',
           'username_text'      => 'Korisničko ime:',
           'password_text'      => 'Lozinka:',
           'password_conf_text' => 'Ponovi lozinku:',
           'email_text'         => 'e-mail',
           'register_button'    => 'Potvrdi',
           'cancel_button'      => 'Odustani'
       );
      
        $this -> form_validation -> set_rules('username',       'korisničko ime',   'strip_tags|trim|required|min_length[3]|max_length[20]|xss_clean');
        $this -> form_validation -> set_rules('password',       'lozinka',          'trim|required|matches[password_conf]|md5|min_length[5]');
        $this -> form_validation -> set_rules('password_conf',  'lozinka',          'trim|required|md5');
        $this -> form_validation -> set_rules('email',          'e-mail',           'trim|required|valid_email|min_length[5]');
        $this -> form_validation -> set_error_delimiters('<div class="error-form-valid">*', '</div>');
        if ($this -> form_validation -> run() === false) {

            $this -> load -> view('header_view', $data);
            $this -> load -> view('register_view');
            $this -> load -> view('footer_view');

        } 
        else 
        {
            $user_data = array(
                    'username'          => $this -> input -> post('username'),
                    'password'          => $this -> input -> post('password'),
                    'email'             => $this -> input -> post('email')
            );
            
            $this -> load -> model('register_model');
            $result = $this -> register_model -> add_user($user_data);
            if($result === FALSE)
            {
                $data['error'] = "Registracija nije moguća jer postoji korisnik sa unesenim e-mailom ili korisničkim imenom!";
            }
            else if($result == "error_mail")
            {
                $data['error'] = "Nije poslan email, kontaktirajte webmaster@carbook.com!";
            }
            else if($result == "error_db")
            {
                $data['error'] = "Pogreška prilikom dodavanja korisnika!";
            }
            else
            {
                echo "<div id='myDiv'>";
                echo "<form id='my_form' action='".  base_url()."index.php/homepage/register/".$result."/".$user_data['username']."' method='post'>";
                echo "</form>";
                echo "</div>";
            }
            
            $this -> load -> view('header_view', $data);
            $this -> load -> view('register_view');
            $this -> load -> view('footer_view');
            
        }
   }
   
   function confirm_user($id = FALSE, $confirm_code = FALSE)
   {
       if($id === FALSE)
            return FALSE;
        
       if($confirm_code === FALSE)
           return FALSE;
            
       $this -> load -> model('register_model');
       $result = $this -> register_model -> confirm_user($id, $confirm_code);
       
      echo 'Registracija nije uspijela! Kontaktirajte podršku.';
   }
}

/* End of file register.php */