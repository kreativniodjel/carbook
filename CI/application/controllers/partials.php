<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Partials extends CI_Controller
{

    function __construct() 
    {
        parent::__construct();
    }


    function modal_login()
    {
        $view = $this->load->view('templates/modal-login', '', true);
        $template = $this->mustache->render($view);
        $this->output->append_output( $template );
    }


    function modal_register()
    {
        $view = $this->load->view('templates/modal-register', '', true);
        $template = $this->mustache->render($view);
        $this->output->append_output( $template );
    }


    function modal_vehicle_add()
    {
        $view = $this->load->view('templates/modal-vehicle-add', '', true);
        $template = $this->mustache->render($view);
        $this->output->append_output( $template );
    }


    function modal_change_password()
    {
        $view = $this->load->view('templates/modal-change-password', '', true);
        $template = $this->mustache->render($view);
        $this->output->append_output( $template );
    }


    function floater_user()
    {
        $user_id = $this -> session -> userdata('user_id');

        $this->load->model('Profile_model');
        $model = $this->Profile_model->get_profile( $user_id );

        $username = $model[0]['username'];
        $first    = $model[0]['name'];
        $last     = $model[0]['lastname'];

        if( strlen($first) > 0 && strlen($last) > 0 )
        {
            $displayname = "$first $last";
        }
        else
        {
            if( strlen($first) > 0 )
                $displayname = $first;
            else if( strlen($last) > 0 )
                $displayname = $last;
            else
                $displayname = $username;
        }

        $view = $this->load->view('templates/floater-user', '', true);
        $data = array(
            'img'       => base_url() . 'assets/img/placeholders/user.png',
            'full-name' => $displayname
        );
        $template = $this->mustache->render($view, $data);
        $this->output->append_output( $template );
    }


    function floater_vehicle()
    {
        $view = $this->load->view('templates/floater-vehicle', '', true);
        $template = $this->mustache->render($view);
        $this->output->append_output( $template );
    }


    function floater_vehicle_ex()
    {
        $view = $this->load->view('templates/floater-vehicle-ex', '', true);
        $template = $this->mustache->render($view);
        $this->output->append_output( $template );
    }


    function floater_vehicle_photo()
    {
        $view = $this->load->view('templates/floater-vehicle-photo', '', true);
        $template = $this->mustache->render($view);
        $this->output->append_output( $template );
    }


    function floater_costs_note()
    {
        $view = $this->load->view('templates/floater-costs-note', '', true);
        $template = $this->mustache->render($view);
        $this->output->append_output( $template );
    }


    function floater_costs_fuel()
    {
        $view = $this->load->view('templates/floater-costs-fuel', '', true);
        $template = $this->mustache->render($view);
        $this->output->append_output( $template );
    }


    function modal_costs_add()
    {
        $this -> load -> model('add_costs_model');
        $type_options = $this -> add_costs_model -> get_costs_types();
        $types = array();
        foreach($type_options AS $key => $value) {
            $each['key']      = $key;
            $each['value']    = $value;
            array_push( $types, $each );
        }

        $view = $this->load->view('templates/modal-costs-add', '', true);
        $data = array(
            'cost-date' => date('Y-m-d'),
            'cost-day-month' => date('d.m.'),
            'cost-year' => date('Y'),
            'type' => $types
        );
        $template = $this->mustache->render($view, $data);
        $this->output->append_output( $template );
    }


}

/* End of file partials.php */