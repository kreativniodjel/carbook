<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Json extends CI_Controller
{

    function __construct() 
    {
        parent::__construct();
        $this->output->set_header('Content-type: application/json');
    }


    function changepassword()
    {
        $error = "Greška!";

        $this -> form_validation -> set_rules('old_password',       'old password',     'trim|required|min_length[5]|md5|xss_clean');
        $this -> form_validation -> set_rules('new_password',       'new password',     'trim|required|md5|min_length[5]|xss_clean');
        $this -> form_validation -> set_rules('repeat_password',    'repeat password',  'trim|required|matches[new_password]|md5|min_length[5]|xss_clean'); 
        
        if ($this-> form_validation -> run() === FALSE)
        {
            $json['STATUS']  = 'ERROR';
            $json['MESSAGE'] = strip_tags(validation_errors());           
        }
        else
        {
            $reset_password                         = array();
            $reset_password['id_user']              = $this -> session -> userdata('user_id');
            $reset_password['old_password']         = $this -> input -> post('old_password');
            $reset_password['new_password']         = $this -> input -> post('new_password');
            $reset_password['repeat_password']      = $this -> input -> post('repeat_password');
        
            $this -> load -> model('profile_model');
            $ret_val = $this -> profile_model -> go_reset_password($reset_password);

            if( $ret_val === 1 )
            {
                $json['STATUS']  = 'OK';
                $json['MESSAGE'] = "Lozinka uspiješno promijenjena!";
            }
            else
            {
                switch($ret_val)
                {
                    case '-3':
                        $error = 'POGREŠKA! Niste uspiješno ponovili novu lozinku. Lozinka nije promijenjena.';
                        break;
                    case '-2':
                        $error = 'POGREŠKA! Došlo je do pogreške prilikom izmjene lozinke. Lozinka nije promijenjena.';
                    case '-1':
                        $error = 'POGREŠKA! Lozinka nije promijenjena jer ste upisali netočnu trenutnu lozinku!';
                        break;
                    case '0':
                        $error = 'POGREŠKA! Pogreška prilikom promijene lozinke. Ukoliko se problem ponovi kontaktirajte administratora.';
                        break;
                }
                $json['STATUS']  = "ERROR";
                $json['MESSAGE'] = $error;
            }
        }

        print json_encode($json);
    }


    function login()
    {
        $error = 'Invalid username or password!';

        $this -> form_validation -> set_rules('username', 'Korisničko ime', 'strip_tags|trim|required|xss_clean');
        $this -> form_validation -> set_rules('password', 'Lozinka', 'strip_tags|trim|required|md5|xss_clean');
        if ($this-> form_validation -> run() === FALSE)
        {
            $json['STATUS']  = 'ERROR';
            $json['MESSAGE'] = strip_tags(validation_errors());
        }
        else
        {
            $username = $this->input->post('username');
            $password = $this->input->post('password');

            $this->load->model('login_model');
            $result = $this -> login_model -> get_user($username, $password);

            if( $result === TRUE )
            {
                $json['STATUS'] = 'OK';
            }
            else
            {
                $json['STATUS']  = 'ERROR';
                $json['MESSAGE'] = $result;
            }
        }

        print json_encode($json);
    }


    function register()
    {
        $error = "Greška!";

        $this -> form_validation -> set_rules('username',       'korisničko ime',   'strip_tags|trim|required|min_length[3]|max_length[20]|xss_clean');
        $this -> form_validation -> set_rules('password',       'lozinka',          'trim|required|matches[password_conf]|md5|min_length[5]');
        $this -> form_validation -> set_rules('password_conf',  'lozinka',          'trim|required|md5');
        $this -> form_validation -> set_rules('email',          'e-mail',           'trim|required|valid_email|min_length[5]');

        if ($this -> form_validation -> run() === FALSE) {
            $json['STATUS']  = 'ERROR';
            $json['MESSAGE'] = strip_tags(validation_errors());
        } 
        else
        {
            $user_data = array(
                    'username'          => $this -> input -> post('username'),
                    'password'          => $this -> input -> post('password'),
                    'email'             => $this -> input -> post('email')
            );

            $this->load->model('register_model');
            $result = $this -> register_model -> add_user($user_data);
            if($result === FALSE)
            {
                $error = "Registracija nije moguća jer postoji korisnik sa unesenim e-mailom ili korisničkim imenom!";
            }
            else if($result == "error_mail")
            {
                $error = "Nije poslan email, kontaktirajte webmaster@carbook.com!";
            }
            else if($result == "error_db")
            {
                $error = "Pogreška prilikom dodavanja korisnika!";
            }

            if( is_int($result) )
            {
                $json['STATUS']  = 'OK';
                $json['MESSAGE'] = 'Provjeri email.';
            }
            else
            {
                $json['STATUS']  = 'ERROR';
                $json['MESSAGE'] = $error;
            }
        }

        print json_encode($json);
    }


    function addvehicle()
    {
        $error = 'Pogreška pri dodavanju vozila!';

        $this -> form_validation -> set_rules('brand',          'MARKA VOZILA',       'strip_tags|trim|required|xss_clean');
        $this -> form_validation -> set_rules('type',           'TIP VOZILA',         'strip_tags|trim|required|xss_clean');
        $this -> form_validation -> set_rules('model',          'MODEL VOZILA',       'strip_tags|trim|required|xss_clean');
        $this -> form_validation -> set_rules('year',           'GODINA PROIZVODNJE', 'strip_tags|trim|required|xss_clean|exact_length[4]');
        $this -> form_validation -> set_rules('km',             'KILOMETRI',          'strip_tags|trim|required|xss_clean|integer');
        $this -> form_validation -> set_rules('licence_plate',  'REG. OZNAKA',        'strip_tags|trim|required|xss_clean|alpha_numeric');
        if ($this-> form_validation -> run() === FALSE)
        {
            $json['STATUS']  = 'ERROR';
            $json['MESSAGE'] = strip_tags(validation_errors());
        }
        else
        {
            $brand = $this -> input -> post('brand');
            $type  = $this -> input -> post('type');
            $model = $this -> input -> post('model');
            $post_data = array(
                'name'          => "$brand $type $model",
                'brand'         => $this -> input -> post('brand'),
                'type'          => $this -> input -> post('type'),
                'model'         => $this -> input -> post('model'),
                'year'          => $this -> input -> post('year'),
                'km'            => $this -> input -> post('km'),
                'licence_plate' => $this -> input -> post('licence_plate'),
                'user_id'       => $this -> session -> userdata ('user_id')
            );

            $year = (int)$post_data['year'];
            if ($year <= 1950 || $year > date("Y"))
            {
                $error = 'Godina proizvodnje nije ispravna!';
                $json['STATUS']  = 'ERROR';
                $json['MESSAGE'] = $error;
            }
            else
            {
                $this->load->model('add_vehicles_model');
                $result = $this -> add_vehicles_model -> add_vehicles($post_data);
                if (is_int($result))
                {
                    $this -> session -> set_userdata('cur_vehicle_id', $result);
                    $json['STATUS']  = 'OK';
                }
                else
                {
                    $json['STATUS']  = 'ERROR';
                    $json['MESSAGE'] = $error;
                }
            }            
        }   

        print json_encode($json);
    }


    function sellvehicle()
    {
        $error = 'Pogreška!';

        $this -> form_validation -> set_rules('location',       'LOKACIJA',           'strip_tags|trim|required|xss_clean');
        $this -> form_validation -> set_rules('price',          'CIJENA',             'strip_tags|trim|required|xss_clean|integer');
        if ($this-> form_validation -> run() === FALSE)
        {
            $error = strip_tags(validation_errors());

            $json['STATUS']  = 'ERROR';
            $json['MESSAGE'] = $error;
        }
        else
        {
            $user_id    = $this -> session -> userdata('user_id');
            $idvehicles = $this->input->post('idveh_sell');
            $location   = $this->input->post('location');
            $price      = $this->input->post('price');

            $this -> load -> model('vehicles_model');
            $result = $this -> vehicles_model -> sell($idvehicles, $price, $location, $user_id);
            if( $result === TRUE )
            {
                $json['STATUS']  = 'OK';
            }
            else
            {
                $json['STATUS']  = 'ERROR';
                $json['MESSAGE'] = $error;
            }
        }

        print json_encode($json);
    }


    function getcost()
    {
        $error = "Greška!";

        $iddetails = $this->input->get('iddetails');

        $this->load->model('add_costs_model');
        $cost = $this -> add_costs_model -> get_cost($iddetails);
        if($cost != FALSE)
        {
            $data['db_date']           = $cost[0]['date'];
            $data['db_alarm_date']     = $cost[0]['alarm_date'];
            $data['db_km']             = $cost[0]['km'];
            $data['db_place_of_cost']  = $cost[0]['place_of_cost'];
            $data['db_note']           = $cost[0]['note'];
            $data['db_quantity']       = $cost[0]['quantity'];
            $data['db_price']          = $cost[0]['price'];
            $data['db_alarm_km']       = $cost[0]['alarm_km'];
            $data['curr_type_options'] = $cost[0]['FK_idcosts_types'];

            $json['STATUS'] = 'OK';
            $json['DATA']   = $data;
        }
        else
        {
            $json['STATUS']  = 'ERROR';
            $json['MESSAGE'] = $error;
        }

        print json_encode($json);
    }


    function editcost()
    {
        $error = 'Pogreška pri uređivanju troška!';

        $this -> form_validation -> set_rules('date',               'naziv vozila',     'strip_tags|trim|required|xss_clean');
        $this -> form_validation -> set_rules('km',                 'km',               'strip_tags|trim|required|xss_clean|integer');
        $this -> form_validation -> set_rules('place_of_cost',      'mjesto troška',    'strip_tags|trim|xss_clean');
        $this -> form_validation -> set_rules('note',               'napomena',         'strip_tags|trim|xss_clean');
        $this -> form_validation -> set_rules('quantity',           'količina',         'strip_tags|trim|required|xss_clean|integer');
        $this -> form_validation -> set_rules('price',              'cijena',           'strip_tags|trim|required|xss_clean|numeric|decimal(2)');
        $this -> form_validation -> set_rules('alarm_km',           'alarm km',         'strip_tags|trim|xss_clean|integer');
        $this -> form_validation -> set_rules('alarm_date',         'alarm datum',      'strip_tags|trim|xss_clean');
        if ($this-> form_validation -> run() === FALSE)
        {
            $json['STATUS']  = 'ERROR';
            $json['MESSAGE'] = strip_tags(validation_errors());
        }
        else
        {
            $post_data = array(
                'date'          => $this -> input -> post('date'),
                'km'            => $this -> input -> post('km'),
                'type'          => $this -> input -> post('type_options'),
                'place_of_cost' => $this -> input -> post('place_of_cost'),
                'note'          => $this -> input -> post('note'),
                'price'         => $this -> input -> post('price'),
                'quantity'      => $this -> input -> post('quantity'),
                'alarm_km'      => $this -> input -> post('alarm_km'),
                'alarm_date'    => $this -> input -> post('alarm_date')
            );

            $id_costs = $this->input->post('idcost');

            $this->load->model('add_costs_model');
            $result = $this -> add_costs_model -> edit_costs($post_data, $id_costs);
            if( $result === TRUE )
            {
                $json['STATUS']  = 'OK';
            }
            else
            {
                $json['STATUS']  = 'ERROR';
                $json['MESSAGE'] = $error;
            }
        }

        print json_encode($json);
    }


    function addcost()
    {
        $error = 'Pogreška pri dodavanju troška!';

        $this -> form_validation -> set_rules('date',               'naziv vozila',     'strip_tags|trim|required|xss_clean');
        $this -> form_validation -> set_rules('km',                 'km',               'strip_tags|trim|required|xss_clean|integer');
        $this -> form_validation -> set_rules('place_of_cost',      'mjesto troška',    'strip_tags|trim|xss_clean');
        $this -> form_validation -> set_rules('note',               'napomena',         'strip_tags|trim|xss_clean');
        $this -> form_validation -> set_rules('quantity',           'količina',         'strip_tags|trim|required|xss_clean|integer');
        $this -> form_validation -> set_rules('price',              'cijena',           'strip_tags|trim|required|xss_clean|decimal(2)');
        $this -> form_validation -> set_rules('alarm_km',           'alarm km',         'strip_tags|trim|xss_clean|integer');
        $this -> form_validation -> set_rules('alarm_date',         'alarm datum',      'strip_tags|trim|xss_clean');
        if ($this-> form_validation -> run() === FALSE)
        {
            $json['STATUS']  = 'ERROR';
            $json['MESSAGE'] = strip_tags(validation_errors());
        }
        else
        {
            $post_data = array(
                'date'          => $this -> input -> post('date'),
                'km'            => $this -> input -> post('km'),
                'type'          => $this -> input -> post('type_options'),
                'place_of_cost' => $this -> input -> post('place_of_cost'),
                'note'          => $this -> input -> post('note'),
                'price'         => $this -> input -> post('price'),
                'quantity'      => $this -> input -> post('quantity'),
                'alarm_km'      => $this -> input -> post('alarm_km'),
                'alarm_date'    => $this -> input -> post('alarm_date')
            );

            $this->load->model('add_costs_model');
            $result = $this -> add_costs_model -> add_costs($post_data, -1);
            if( $result === TRUE )
            {
                $json['STATUS']  = 'OK';
            }
            else
            {
                $json['STATUS']  = 'ERROR';
                $json['MESSAGE'] = $error;
            }
        }

        print json_encode($json);
    }


}

/* End of file json.php */