<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');
class Vehicles extends CI_Controller
{
    function __construct() 
    {
        parent::__construct();
    }
    
    function index()
    {
        $data = array (
            'title'             => 'Vozni park',
            'exist_ex_vehicles' => FALSE,
            'veh_table_js'      => TRUE
        );
        
        $user_id = $this -> session -> userdata('user_id');
        
        $this -> load -> model('vehicles_model');
        $vehicles = $this -> vehicles_model -> show($user_id);
        if($vehicles === FALSE)
        {
            $vehicles = array();
            $data['error'] = 'Trenutno ne postoji upisano nijedno vozilo za korisnika. Kliknite dugme Dodaj za dodavanje novog vozila!';
        }
        
        // ex vehicles
        $this -> load -> model('vehicles_model');
        $ex_vehicles = $this -> vehicles_model -> show_ex_vehicles($user_id);
        if($ex_vehicles === FALSE)
        {
            $ex_vehicles = array();
            $data['exist_ex_vehicles'] = FALSE;
        }
        else
        {
            $data['exist_ex_vehicles'] = TRUE;
        }
        
        
        $data['vehicles']       = $vehicles;
        $data['ex_vehicles']    = $ex_vehicles;

        $view = $this -> load -> view('header_view', $data, true);
        $this->output->append_output( $view );

        $this->render_template($data);

        $view = $this -> load -> view('footer_view', '', true);
        $this->output->append_output( $view );
    }
    
    
    function sell($idvehicles=NULL, $price=NULL)
    {
        if($idvehicles != NULL)
        {
            $user_id = $this -> session -> userdata('user_id');
            $this -> load -> model('vehicles_model');
            $vehicles = $this -> vehicles_model -> sell($idvehicles, $price, $user_id);
        }
        redirect(base_url() . 'index.php/' . 'ads');  
    }
    

    function delete($idvehicles=NULL, $delete_type=NULL)
    {
        if($idvehicles != NULL)
        {
            $user_id = $this -> session -> userdata('user_id');
            $this -> load -> model('vehicles_model');
            if($delete_type == 0) // if deleting vehicle from current vehicles list
            {
                $vehicles = $this -> vehicles_model -> delete($idvehicles, $user_id);
            }
            else // if deleting vehicle from ex vehicles list
            {
                $vehicles = $this -> vehicles_model -> delete_ex($idvehicles, $user_id);
            }
        }
        $this -> session -> unset_userdata('cur_vehicle_id');
        redirect(base_url() . 'index.php/' . 'vehicles');
    }


    function switch_to_current($idvehicles=NULL)
    {
        if($idvehicles != NULL)
        {
            $user_id = $this -> session -> userdata('user_id');
            $this -> load -> model('vehicles_model');
            $vehicles = $this -> vehicles_model -> switch_to_current($idvehicles, $user_id);
        }
        $this -> session -> unset_userdata('cur_vehicle_id');
        redirect(base_url() . 'index.php/' . 'vehicles');
    }


    function switch_to_ex($idvehicles=NULL)
    {
        if($idvehicles != NULL)
        {
            $user_id = $this -> session -> userdata('user_id');
            $this -> load -> model('vehicles_model');
            $vehicles = $this -> vehicles_model -> switch_to_ex($idvehicles, $user_id);
        }
        $this -> session -> unset_userdata('cur_vehicle_id');
        redirect(base_url() . 'index.php/' . 'vehicles');
    }


    function render_template($data)
    {

        // Current vehicles

        if( sizeof($data['vehicles']) > 0 )
        {
            $data['have_any_vehicle'] = TRUE;

            $vehicles = array();

            $this -> load -> model('navigation_menu_model');
            $alarms = $this -> navigation_menu_model -> alarms;

            foreach($data['vehicles'] as $vehicle)
            {

                $vehicle['km'] = KO_number_format_km($vehicle['km']);
                $vehicle['costs'] = KO_number_format_cost($vehicle['costs']);
                $vehicle['has_any_alarm'] = FALSE;
                $first_alarm = TRUE;
                foreach($alarms as $alarm)
                {
                    if($alarm['idvehicles'] == $vehicle['idvehicles'])
                    {
                        $$vehicle['has_any_alarm'] = TRUE;

                        if($first_alarm === TRUE) {
                            $vehicle['alarms'] = array();
                            $first_alarm = FALSE;
                        } 

                        array_push($vehicle['alarms'], array(
                                'type' => $alarm['idcosts_types']
                            )
                        );                       
                    }

                }

                array_push($vehicles, $vehicle);
            }

            $data['vehicles'] = $vehicles;
        }

        $data['img_path'] = base_url('/gallery') . '/';

        $view = $this->load->view('templates/table-vehicles-active', '', true);
        $template = $this->mustache->render($view, $data);
        $this->output->append_output( $template );


        // Ex vehicles

        if( sizeof($data['ex_vehicles']) == 0 ) return;

        $this->output->append_output( "<br><br>" );

        $vehicles = array();
        foreach($data['ex_vehicles'] as $vehicle)
        {
            $vehicle['km'] = KO_number_format_km($vehicle['km']);
            $vehicle['costs'] = KO_number_format_cost($vehicle['costs']);
            array_push($vehicles, $vehicle);
        }
        $data['ex_vehicles'] = $vehicles;

        $view = $this->load->view('templates/table-vehicles-ex', '', true);
        $template = $this->mustache->render($view, $data);
        $this->output->append_output( $template );

    }

}

/* End of vehicles.php */