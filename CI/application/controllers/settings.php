<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Settings extends CI_Controller
{
    function __construct() 
    {
        parent::__construct();
    }
    
    function index()
    {
        $data = array (
            'title' => 'Postavke'
        );
        $this -> load -> view('header_view', $data);
        
        $user_id = $this -> session -> userdata('user_id');
        $this -> load -> model('settings_model');
        $settings = $this -> settings_model -> get_settings($user_id);
        if($settings === FALSE)
        {
            $data['error'] = 'Pogreška!';
        }
        $data['profile'] = $settings;
        $this -> load -> view('settings_view', $data);
        $this -> load -> view('footer_view');
    }
}
/* End of file settings.php */
