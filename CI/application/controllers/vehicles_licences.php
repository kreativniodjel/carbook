<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Vehicles_licences extends CI_Controller
{
    function __construct() 
    {
        parent::__construct();
    }
    
    public function index()
    {
        $ex_vehicle = $this -> session -> userdata('ex_vehicle');    
        $this -> session -> set_userdata('ex_vehicle');    
        
        $data_header = array (
            'title'                 => 'Podaci o vozilu',
            'gallery_js'            => TRUE
        );

        if($ex_vehicle == TRUE)
            $data_header['show_ex_vehicles'] = $ex_vehicle;
        
        $this -> load -> view('header_view', $data_header);
        
        $id_vehicle = $this -> session -> userdata('cur_vehicle_id');
        $this -> load -> model ('vehicles_licences_model');
        $enable_changes = $this -> vehicles_licences_model -> check_if_vehicle_data_can_be_changed($id_vehicle);
        $data = array (
            'title'                 => 'Podaci o vozilu',
            'name_text'             => 'NAZIV VOZILA', 
            'licence_plate_text'    => 'REGISTARSKA OZNAKA',
            'class_text'            => 'VRSTA VOZILA', 
            'brand_text'            => 'MARKA VOZILA', 
            'type_text'             => 'TIP VOZILA', 
            'model_text'            => 'MODEL VOZILA',
            'color_text'            => 'BOJA', 
            'num_of_chassis_text'   => 'BROJ ŠASIJE',
            'body_shape_text'       => 'OBLIK KAROSERIJE',
            'manuf_country_text'    => 'DRŽAVA I GODINA PROZIVODNJE',
            'date_first_reg_text'   => 'DATUM PRVE REGISTRACIJE',
            'seat_places_text'      => 'MJESTA ZA SJEDNJE',
            'carrying_capacity_text'=> 'DOPUŠTENA NOSIVOST',
            'mass_text'             => 'MASA PRAZNOG VOZILA',
            'max_speed_text'        => 'MAKSIMALNA BRZINA', 
            'engine_type_text'      => 'VRSTA MOTORA', 
            'power_text'            => 'SNAGA', 
            'cubic_capacity_text'   => 'RADNI OBUJAM',
            'length_text'           => 'DUŽINA', 
            'width_text'            => 'ŠIRINA', 
            'height_text'           => 'VISINA', 
            'lowest_point_text'     => 'NAJNIŽA TOČKA',
            'num_of_wheels_text'    => 'BROJ KOTAČA', 
            'tyres_front_text'      => 'PREDNJE GUME', 
            'tyres_back_text'       => 'STRAŽNJE GUME', 
            'brake_type_text'       => 'VRSTA KOČNICA',
            'num_of_axles_text'     => 'BROJ OSOVINA',
            'driving_axles_text'    => 'POGONSKIH OSOVINA',
            'hook_text'             => 'KUKA', 
            'anchor_text'           => 'VITLO',
            'gasoline_text'         => 'BENZIN',
            'diesel_text'           => 'DIZEL', 
            'gas_text'              => 'PLIN', 
            'electricity_text'      => 'STRUJA',
            'short_lights_text'     => 'KRATKA SVJETLA', 
            'high_lights_text'      => 'DUGA SVJETLA', 
            'stop_lights_text'      => 'STOP SVJETLA',
            'db_name'               => '', 
            'db_licence_plate'      => '',
            'db_class'              => '', 
            'db_brand'              => '', 
            'db_type'               => '', 
            'db_model'              => '',
            'db_color'              => '', 
            'db_num_of_chassis'     => '', 
            'db_body_shape'         => '',
            'db_manuf_country'      => '',  
            'db_manuf_year'         => '', 
            'db_date_first_reg'     => '', 
            'db_year_first_reg'     => '',
            'db_seat_places'        => '', 
            'db_carrying_capacity'  => '', 
            'db_mass'               => '',
            'db_max_speed'          => '', 
            'db_engine_type'        => '', 
            'db_power'              => '', 
            'db_cubic_capacity'     => '',
            'db_length'             => '', 
            'db_width'              => '', 
            'db_height'             => '', 
            'db_lowest_point'       => '',
            'db_num_of_wheels'      => '', 
            'db_tyres_front'        => '', 
            'db_tyres_back'         => '', 
            'db_brake_type'         => '',
            'db_num_of_axles'       => '', 
            'db_driving_axles'      => '',
            'db_hook'               => '', 
            'db_anchor'             => '',
            'db_gasoline'           => '', 
            'db_diesel'             => '', 
            'db_gas'                => '', 
            'db_electricity'        => '',
            'db_short_lights'       => '', 
            'db_high_lights'        => '', 
            'db_stop_lights'        => '',
            'pictures'              => array(),
            'submit_button'         => 'Promijeni', 
            'enable_changes'        => FALSE
        );
        $data['enable_changes'] = $enable_changes;
        $this -> form_validation -> set_error_delimiters('<div class="error-form-valid">*', '</div>');       
        
        if(($id_vehicle != 0) && (!empty($id_vehicle)))
        {    
            // First checking if form is submited, if that is true then data will be checked and send to database
            if ($this -> input -> post('submit_form_vehicles')) 
            {
                // Form validation and custom error messages
                $this -> form_validation -> set_rules('name',               'naziv vozila',             'strip_tags|trim|required|xss_clean');
                $this -> form_validation -> set_rules('licence_plate',      'reg. oznaka',              'strip_tags|trim|required|xss_clean|alpha_numeric');

                // If all data is proper add it to database
                if ($this-> form_validation -> run() === TRUE)
                {
                    $data['$id_vehicle']        = $id_vehicle;
                    $data['name']               = $this -> input -> post('name');
                    $data['licence_plate']      = $this -> input -> post('licence_plate');
                    $this -> vehicles_licences_model -> update_vehicles($data);
                }
            }
            else if ($this -> input -> post('submit_form_vehicles_licences'))
            {
                $this -> form_validation -> set_rules('class',              'VRSTA VOZILA',             'strip_tags|trim|xss_clean');
                $this -> form_validation -> set_rules('brand',              'MARKA VOZILA',             'strip_tags|trim|xss_clean');
                $this -> form_validation -> set_rules('type',               'TIP VOZILA',               'strip_tags|trim|xss_clean');
                $this -> form_validation -> set_rules('model',              'MODEL VOZILA',             'strip_tags|trim|xss_clean');
                $this -> form_validation -> set_rules('color',              'BOJA',                     'strip_tags|trim|xss_clean');
                $this -> form_validation -> set_rules('num_of_chassis',     'BROJ ŠASIJE',              'strip_tags|trim|xss_clean');
                $this -> form_validation -> set_rules('body_shape',         'OBLIK KAROSERIJE',         'strip_tags|trim|xss_clean');
                $this -> form_validation -> set_rules('manuf_country',      'DRŽAVA PROZIVODNJE',       'strip_tags|trim|xss_clean');
                $this -> form_validation -> set_rules('manuf_year',         'GODINA PROZIVODNJE',       'strip_tags|trim|xss_clean');
                $this -> form_validation -> set_rules('date_first_reg',     'DATUM PRVE REGISTRACIJE',  'strip_tags|trim|xss_clean');
                //$this -> form_validation -> set_rules('year_first_reg',     'GOD. PRVE REGISTRACIJE',   'strip_tags|trim|xss_clean');
                $this -> form_validation -> set_rules('seat_places',        'MJESTA ZA SJEDNJE',        'strip_tags|trim|xss_clean');
                $this -> form_validation -> set_rules('carrying_capacity',  'DOPUŠTENA NOSIVOST',       'strip_tags|trim|xss_clean');
                $this -> form_validation -> set_rules('mass',               'MASA PRAZNOG VOZILA',      'strip_tags|trim|xss_clean');
                $this -> form_validation -> set_rules('max_speed',          'MAKSIMALNA BRZINA',        'strip_tags|trim|xss_clean');
                $this -> form_validation -> set_rules('engine_type',        'VRSTA MOTORA',             'strip_tags|trim|xss_clean');
                $this -> form_validation -> set_rules('power',              'SNAGA',                    'strip_tags|trim|xss_clean');
                $this -> form_validation -> set_rules('cubic_capacity',     'RADNI OBUJAM',             'strip_tags|trim|xss_clean');
                $this -> form_validation -> set_rules('length',             'DUŽINA',                   'strip_tags|trim|xss_clean');
                $this -> form_validation -> set_rules('width',              'ŠIRINA',                   'strip_tags|trim|xss_clean');
                $this -> form_validation -> set_rules('height',             'VISINA',                   'strip_tags|trim|xss_clean');
                $this -> form_validation -> set_rules('lowest_point',       'NAJNIŽA TOČKA',            'strip_tags|trim|xss_clean');
                $this -> form_validation -> set_rules('num_of_wheels',      'BROJ KOTAČA',              'strip_tags|trim|xss_clean');
                $this -> form_validation -> set_rules('tyres_front',        'PREDNJE GUME',             'strip_tags|trim|xss_clean');
                $this -> form_validation -> set_rules('tyres_back',         'STRAŽNJE GUME',            'strip_tags|trim|xss_clean');
                $this -> form_validation -> set_rules('brake_type',         'VRSTA KOČNICA',            'strip_tags|trim|xss_clean');
                $this -> form_validation -> set_rules('num_of_axles',       'BROJ OSOVINA',             'strip_tags|trim|xss_clean');
                $this -> form_validation -> set_rules('driving_axles',      'POGONSKIH OSOVINA',        'strip_tags|trim|xss_clean');
                
                // If all data is proper add it to database
                if ($this-> form_validation -> run() === TRUE)
                {
                    $data['$id_vehicle']        = $id_vehicle;
                    $data['class']              = $this -> input -> post('class');
                    $data['brand']              = $this -> input -> post('brand');
                    $data['type']               = $this -> input -> post('type');
                    $data['model']              = $this -> input -> post('model');
                    $data['color']              = $this -> input -> post('color');
                    $data['num_of_chassis']     = $this -> input -> post('num_of_chassis');
                    $data['body_shape']         = $this -> input -> post('body_shape');
                    $data['manuf_country']      = $this -> input -> post('manuf_country');
                    $data['manuf_year']         = $this -> input -> post('manuf_year');
                    $data['date_first_reg']     = $this -> input -> post('date_first_reg');
                    $data['seat_places']        = $this -> input -> post('seat_places');
                    $data['carrying_capacity']  = $this -> input -> post('carrying_capacity');
                    $data['mass']               = $this -> input -> post('mass');
                    $data['max_speed']          = $this -> input -> post('max_speed');
                    $data['engine_type']        = $this -> input -> post('engine_type');
                    $data['power']              = $this -> input -> post('power');
                    $data['cubic_capacity']     = $this -> input -> post('cubic_capacity');
                    $data['length']             = $this -> input -> post('length');
                    $data['width']              = $this -> input -> post('width');
                    $data['height']             = $this -> input -> post('height');
                    $data['lowest_point']       = $this -> input -> post('lowest_point');
                    $data['num_of_wheels']      = $this -> input -> post('num_of_wheels');
                    $data['tyres_front']        = $this -> input -> post('tyres_front');
                    $data['tyres_back']         = $this -> input -> post('tyres_back');
                    $data['brake_type']         = $this -> input -> post('brake_type');
                    $data['num_of_axles']       = $this -> input -> post('num_of_axles');
                    $data['driving_axles']      = $this -> input -> post('driving_axles');
                    if($this -> input -> post('hook') === 'accept')
                        $data['hook']               = TRUE;
                    else
                        $data['hook']               = FALSE;
                    if($this -> input -> post('anchor') === 'accept')
                        $data['anchor']               = TRUE;
                    else
                        $data['anchor']               = FALSE;
                    $this -> vehicles_licences_model -> update_vehicles_licences($data);
                }
            }
            else if ($this -> input -> post('submit_form_gas'))
            {
                $data['$id_vehicle']    = $id_vehicle;
                if($this -> input -> post('gasoline') === 'accept')
                    $data['gasoline']       = TRUE;
                else
                    $data['gasoline']       = FALSE;
                
                if($this -> input -> post('diesel') === 'accept')    
                    $data['diesel']         = TRUE;
                else
                    $data['diesel']         = FALSE;
                
                if($this -> input -> post('gas') === 'accept')    
                    $data['gas']            = TRUE;
                else
                    $data['gas']            = FALSE;
                
                if($this -> input -> post('electricity') === 'accept')    
                    $data['electricity']   = TRUE;
                else
                    $data['electricity']   = FALSE;
                
                $this -> vehicles_licences_model -> update_vehicles_gas($data);
            }
            else if ($this -> input -> post('submit_form_lights'))
            {
                $this -> form_validation -> set_rules('short_lights',     'KRATKA SVJETLA',      'strip_tags|trim|xss_clean');
                $this -> form_validation -> set_rules('high_lights',      'DUGA SVJETLA',        'strip_tags|trim|xss_clean');
                $this -> form_validation -> set_rules('stop_lights',      'STOP SVJETLA',        'strip_tags|trim|xss_clean');
                
                // If all data is proper add it to database
                if ($this-> form_validation -> run() === TRUE)
                {
                    $data['$id_vehicle']    = $id_vehicle;
                    $data['short_lights']   = $this -> input -> post('short_lights');
                    $data['high_lights']    = $this -> input -> post('high_lights');
                    $data['stop_lights']    = $this -> input -> post('stop_lights');
                    
                    $this -> vehicles_licences_model -> update_vehicles_lights($data);
                }
            }
            
            $vehicles_data = $this -> vehicles_licences_model -> get_vehicles_data ($id_vehicle);
            if($vehicles_data != FALSE)
            {
                $data['db_thumbnail_name']           = $vehicles_data[0]['thumbnail_name'];
                $data['db_name']                     = $vehicles_data[0]['name'];            
                $data['db_licence_plate']            = $vehicles_data[0]['licence_plate'];      
                $data['db_class']                    = $vehicles_data[0]['class'];             
                $data['db_brand']                    = $vehicles_data[0]['brand'];             
                $data['db_type']                     = $vehicles_data[0]['type'];              
                $data['db_model']                    = $vehicles_data[0]['model'];            
                $data['db_color']                    = $vehicles_data[0]['color'];             
                $data['db_num_of_chassis']           = $vehicles_data[0]['num_of_chassis'];      
                $data['db_body_shape']               = $vehicles_data[0]['body_shape'];        
                $data['db_manuf_country']            = $vehicles_data[0]['manuf_country'];       
                $data['db_manuf_year']               = $vehicles_data[0]['manuf_year'];
                $data['db_date_first_reg']           = (KO_date_exist($vehicles_data[0]['date_first_reg']) === FALSE) ? '' : $vehicles_data[0]['date_first_reg'];
                $data['db_seat_places']              = $vehicles_data[0]['seat_places'];       
                $data['db_carrying_capacity']        = $vehicles_data[0]['carrying_capacity'];  
                $data['db_mass']                     = $vehicles_data[0]['mass'];      
                $data['db_max_speed']                = $vehicles_data[0]['max_speed'];       
                $data['db_engine_type']              = $vehicles_data[0]['engine_type'];          
                $data['db_power']                    = $vehicles_data[0]['power']; 
                $data['db_cubic_capacity']           = $vehicles_data[0]['cubic_capacity'];  
                $data['db_length']                   = $vehicles_data[0]['length']; 
                $data['db_width']                    = $vehicles_data[0]['width'];   
                $data['db_height']                   = $vehicles_data[0]['height'];  
                $data['db_lowest_point']             = $vehicles_data[0]['lowest_point'];  
                $data['db_num_of_wheels']            = $vehicles_data[0]['num_of_wheels'];  
                $data['db_tyres_front']              = $vehicles_data[0]['tyres_front']; 
                $data['db_tyres_back']               = $vehicles_data[0]['tyres_back'];  
                $data['db_brake_type']               = $vehicles_data[0]['brake_type'];  
                $data['db_num_of_axles']             = $vehicles_data[0]['num_of_axles'];   
                $data['db_driving_axles']            = $vehicles_data[0]['driving_axles'];  
                $data['db_hook']                     = $vehicles_data[0]['hook'];  
                $data['db_anchor']                   = $vehicles_data[0]['anchor'];  
                $data['db_gasoline']                 = $vehicles_data[0]['gasoline'];  
                $data['db_diesel']                   = $vehicles_data[0]['diesel'];  
                $data['db_gas']                      = $vehicles_data[0]['gas'];  
                $data['db_electricity']              = $vehicles_data[0]['electricity'];  
                $data['db_short_lights']             = $vehicles_data[0]['short_lights']; 
                $data['db_high_lights']              = $vehicles_data[0]['high_lights'];
                $data['db_stop_lights']              = $vehicles_data[0]['stop_lights'];  
            }
            // gallery
            $data['pictures'] = $this -> vehicles_licences_model -> get_vehicles_gallery($id_vehicle);
            
            //$this -> load -> view('vehicles_licences_view', $data);
            $this->render_template( $data );
        }
        else 
        {
            $data['error'] = 'Izaberite vozilo za prikaz podataka.';

            // $this -> load -> view('vehicles_licences_view', $data, true);
            $this->render_template( $data );
        }

        $this -> load -> view('footer_view');
    }
    
    function set_vehicles($sended_idvehicles = FALSE, $ex_vehicles = FALSE)
    {
        if($sended_idvehicles != FALSE)
        {
            $this -> session -> set_userdata('cur_vehicle_id', $sended_idvehicles);
            $this -> session -> set_userdata('ex_vehicle', $ex_vehicles);
        }
        
        redirect(base_url() . 'index.php/' . 'vehicles_licences'); 
    }
    
    function add_picture_to_gallery($filename=NULL)
    {
        $file_extension = substr(strrchr($_FILES['file']['name'],'.'),1);
        if(($file_extension != 'jpg')&&($file_extension != 'png'))
        {
            return;
        }
        $id_vehicle = $this -> session -> userdata('cur_vehicle_id');
        $user_id    = $this -> session -> userdata('user_id');
        
        //remove file extension
        $new_file_name = substr($_FILES['file']['name'],0,-4);
        $new_file_name = $new_file_name . "_" .create_random_string(32)."_".$user_id.".".$file_extension;
        $_FILES['file']['name'] = $new_file_name;
        
        // First try to upload file on server!
        $target_path = "gallery/";
        $target_path = $target_path . basename( $_FILES['file']['name']);
        if(move_uploaded_file($_FILES['file']['tmp_name'], $target_path)) 
        {
            $data                   = array();
            $data['id_vehicle']     = $id_vehicle; 
            $data['name']           = $this -> input -> post('file_add_name');
            $data['id_file_name']   = $_FILES['file']['name']; 
            $data['description']    = $this -> input -> post('file_add_desc');
                    
            $this -> load -> model ('vehicles_licences_model');
            $this -> vehicles_licences_model -> add_picture_to_gallery ($data);
        }
        
        redirect(base_url() . 'index.php/' . 'vehicles_licences#vehicle-gallery');
    }
    

    function delete_picture($idpictures)
    {
        $id_vehicle = $this -> session -> userdata('cur_vehicle_id');
        $this -> load -> model ('vehicles_licences_model');
        
        $this -> vehicles_licences_model -> delete_pic_from_gallery ($idpictures);
        
        redirect(base_url() . 'index.php/' . 'vehicles_licences#vehicle-gallery');
    }
    

    function set_pic_as_thumbnail($idpictures)
    {
        $id_vehicle = $this -> session -> userdata('cur_vehicle_id');
        $this -> load -> model ('vehicles_licences_model');
        
        $picname = $this -> vehicles_licences_model -> get_pic_thumbnail ($idpictures);
        $this -> vehicles_licences_model -> set_pic_as_thumbnail ($picname, $id_vehicle);
        
        redirect(base_url() . 'index.php/' . 'vehicles_licences');
    }


    function manipulate_picture($idpictures)
    {
        $id_vehicle = $this -> session -> userdata('cur_vehicle_id');
        $this -> load -> model ('vehicles_licences_model');
        
        if($this -> input -> post('btn_pic_delete'))
        {
            $this -> vehicles_licences_model -> delete_pic_from_gallery ($idpictures);
        }
        else
        {
            $picname = $this -> input -> post('pic_path');
            $this -> vehicles_licences_model -> set_pic_as_thumbnail ($picname, $id_vehicle);
        }
        
        redirect(base_url() . 'index.php/' . 'vehicles_licences');
    }
    

    function edit_picture()
    {
        $this -> load -> model ('vehicles_licences_model');
        
        $data                   = array();
        $data['name']           = $this -> input -> post('name_edit'); 
        $data['description']    = $this -> input -> post('desc_edit'); 
        $data['idpictures']     = $this -> input -> post('picid_edit');
       
        $this -> vehicles_licences_model -> edit_pic_in_gallery ($data);
       
        redirect(base_url() . 'index.php/' . 'vehicles_licences');
    }

    function render_template($data)
    {
        $this->output->append_output( @$data['error'] );
        //$this->output->append_output( @form_error('licence_plate') );
        $this->output->append_output( @validation_errors() );

        $data['img_path'] = base_url('/gallery') . '/';
        $view = $this->load->view('templates/table-vehicle', false, true);
        $template = $this->mustache->render($view, $data);
        $this->output->append_output( $template );

        $this->output->append_output( "<br><br>" );

        $data['db_hook_checked']   = ( $data['db_hook'] === "1" )   ? 'checked' : '';
        $data['db_anchor_checked'] = ( $data['db_anchor'] === "1" ) ? 'checked' : '';
        $view = $this->load->view('templates/table-vehicle-license', false, true);
        $template = $this->mustache->render($view, $data);
        $this->output->append_output( $template );

        $this->output->append_output( "<br><br>" );

        $data['db_gasoline_checked']    = ( $data['db_gasoline'] === "1" )    ? 'checked' : '';
        $data['db_diesel_checked']      = ( $data['db_diesel'] === "1" )      ? 'checked' : '';
        $data['db_gas_checked']         = ( $data['db_gas'] === "1" )         ? 'checked' : '';
        $data['db_electricity_checked'] = ( $data['db_electricity'] === "1" ) ? 'checked' : '';
        $view = $this->load->view('templates/table-vehicle-fuel', false, true);
        $template = $this->mustache->render($view, $data);
        $this->output->append_output( $template );

        $this->output->append_output( "<br><br>" );

        $view = $this->load->view('templates/table-vehicle-lights', false, true);
        $template = $this->mustache->render($view, $data);
        $this->output->append_output( $template );

        $this->output->append_output( "<br><br>" );

        $data['img_path'] = base_url('/gallery') . '/';
        $view = $this->load->view('templates/table-vehicle-gallery', false, true);
        $template = $this->mustache->render($view, $data);
        $this->output->append_output( $template );
        
    }

}

/* End of file vehicles_licences.php*/