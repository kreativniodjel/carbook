<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Statistics extends CI_Controller
{
    function __construct() 
    {
        parent::__construct();
    }
    
    function index()
    {
        $data = array (
            'title' => 'Statistika'
        );
        $this -> load -> view('header_view', $data);
        $this -> load -> view('statistics_view', $data);
        $this -> load -> view('footer_view');
    }
}
/* End of file statistics.php */