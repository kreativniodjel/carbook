<?php
require_once '/Users/sceleski/Sites/carbook/web/CI/system/libraries/phamlp/haml/HamlHelpers.php';
?><!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]--><!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]--><!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]--><!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]--><head><meta charset="utf-8" /><meta content="IE=edge,chrome=1" http-equiv="X-UA-Compatible" /><title>CARB00K</title><meta name="description" /><meta content="width=device-width" name="viewport" /><!-- Place favicon.ico and apple-touch-icon.png in the root directory --><link href="../assets/css/screen-theme-default.css" media="screen, projection" rel="stylesheet" type="text/css" /><script src="../assets/js/vendors/modernizr-2.6.2.min.js"></script><script src="../assets/js/vendors/jquery-1.8.2.min.js"></script><script src="../assets/js/vendors/jquery-ui-1.9.1.custom.min.js"></script><script src="../assets/js/scripts-ck.js"></script></head><body><style type="text/css">
/*<![CDATA[*/
.sandbox {
  padding-left: 30px;
  padding-bottom: 30px;
}

.sandbox > * {
  position: relative;
}

.sandbox > h2 {
  margin-top: 60px;
  margin-bottom: 20px;
}

.index {
  margin: 30px;
  padding-bottom: 30px; 
  border-bottom: 1px solid #ccc;
}

div[name="table vehicle"],
div[name="table vehicle lights"],
div[name="table vehicle license"],
div[name="table vehicle fuel"],
div[name="table vehicle gallery"],
div[name="table user personal data"],
div[name="table user driver license"],
div[name="table user hak"] {
  width: 715px;
}

div[name="table costs"] {
  width: 960px;
}

/*]]>*/
</style>
<script>$(document).ready(function(){$(".sandbox > *").each(function(){var name = $(this).attr("name");var $anchor_index = $("<a>").attr("href","#"+name).text(name).add($("<br>"));var $anchor_target = $("<a>").attr("href","#"+name).attr("name",name).text(name);var $title = $("<h2>").append($anchor_target);$anchor_index.appendTo("#index");$(this).before($title);});});</script><div class="index" id="index"></div><div class="sandbox">