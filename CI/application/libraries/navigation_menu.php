<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Navigation_menu 
{   
    var $CI;
    
    public function __construct()
    {
        $this -> CI =& get_instance();
    }
    
    function create_menu($show_ex_vehicles = FALSE)
    {
        $register_user = FALSE;
        $data = array(
            'register_user'             => 'FALSE',
            'menu_selected_id'          => '-1',
            'curr_vehicles'             => '0'
        );
        
        if(isset($this -> CI -> session))
        {
            $user_id = $this -> CI -> session -> userdata('user_id');
            if($user_id > 0)
            {
                $menu_selected_id = -1;
                switch($this -> CI -> uri -> segment(1))
                {
                    case 'homepage':
                        $menu_selected_id = 0;
                        break;
                    case 'vehicles':
                    case 'costs':
                    case 'vehicles_licences':
                        $menu_selected_id = 1;
                        break;
                    case 'ads':
                    case 'yellow_pages':
                        $menu_selected_id = 2;
                        break;
                    case 'profile':
                    case 'settings':
                        $menu_selected_id = 3;
                        break;
                }
                $data['menu_selected_id'] = $menu_selected_id; 
                
                $register_user = TRUE;
                $data['register_user'] = $register_user; 
                
                $this -> CI -> load -> model('navigation_menu_model');
                $vehicles_drop_list         = $this -> CI -> navigation_menu_model -> get_user_vehicles($user_id, $show_ex_vehicles);
                $data['vehicles_drop_list'] = $vehicles_drop_list;

                $vehicles_costs             = $this -> CI -> navigation_menu_model -> get_vehicles_costs($user_id);
                $data['vehicles_costs']     = KO_number_format_total_cost($vehicles_costs);
                
                $data['vehicles_alarms_grouped'] = $this -> CI -> navigation_menu_model -> get_vehicles_alarms($user_id);
                $data['vehicles_alarms']         = $this -> CI -> navigation_menu_model -> alarms;
                
                $curr_vehicles = 0;
                // If dropdown box changed get value
                if($this -> CI -> input -> post('hdn_id'))
                {
                    $curr_vehicles = $this -> CI -> input -> post('vehicles_drop_list');
                    // If we are currently on vehicles redirect to costs!
                    if(($this -> CI -> uri -> segment(1) == 'vehicles')&&($curr_vehicles > 0))
                    {
                        redirect(base_url() . 'index.php/' . 'costs/set_vehicles/'.$curr_vehicles);
                    }
                }
                else
                {
                    if($this -> CI -> session -> userdata('cur_vehicle_id'))
                    {
                        $curr_vehicles = $this -> CI -> session -> userdata('cur_vehicle_id');
                    }
                }
                $this -> CI -> session -> set_userdata('cur_vehicle_id', $curr_vehicles);
                $data['curr_vehicles'] = $curr_vehicles;
            }
        }

        // $this -> CI -> load -> view('navigation_menu_view', $data);
        $this->render_template($data);
    }

    function render_template($data)
    {

        if( $data['curr_vehicles'] !== "0" )
        {
            $data['vehicles_list'] = array();

            foreach($data['vehicles_drop_list'] AS $key => $value)
            {
                $each['key']      = $key;
                $each['value']    = $value;
                $each['selected'] = ( $data['curr_vehicles'] == $key ) ? 'selected' : '';
                array_push( $data['vehicles_list'], $each );
            }
        }

        $cat = $this->CI->uri->segment(1);

        $data['nav_current_vehicles']    = ($cat === 'vehicles')     ? 'active' : '';
        $data['nav_current_ads']         = ($cat === 'ads')          ? 'active' : '';
        $data['nav_current_yellowpages'] = ($cat === 'yellow_pages') ? 'active' : '';
        $data['nav_current_profile']     = ($cat === 'profile')      ? 'active' : '';
        $data['nav_current_settings']    = ($cat === 'settings')     ? 'active' : '';
        $data['nav_current_costs']       = ($cat === 'costs')        ? 'active' : '';
        $data['nav_current_statistics']  = ($cat === 'statistics')   ? 'active' : '';

        if ($cat === 'vehicles' || $cat === 'vehicles_licences')
            $data['nav_current_vehicles_group'] = 'active';
        else
            $data['nav_current_vehicles_group'] = '';

        if ($cat === 'ads' || $cat === 'yellow_pages')
            $data['nav_current_ads_group'] = 'active';
        else
            $data['nav_current_ads_group'] = '';

        $view = $this->CI->load->view('templates/navigation-menu', $data, true);
        echo $this->CI->mustache->render($view, $data);

    }

}

/* End of file navigation_menu.php */
