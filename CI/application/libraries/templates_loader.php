<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Templates_loader 
{   
    var $CI;
    
    public function __construct()
    {
        $this -> CI =& get_instance();
    }
    

    function load()
    {
        $templates = "";

        $templates .= $this->modal_login();
        $templates .= $this->modal_register();
        $templates .= $this->modal_vehicle_add();
        $templates .= $this->modal_vehicle_sell();
        $templates .= $this->modal_change_password();
        $templates .= $this->modal_cost();
        $templates .= $this->floater_user();
        $templates .= $this->floater_vehicle();
        $templates .= $this->floater_vehicle_ex();
        $templates .= $this->floater_vehicle_photo();
        $templates .= $this->floater_cost();
        $templates .= $this->floater_cost_note();
        $templates .= $this->floater_cost_alarm();

        return $templates;
    }


    function modal_login()
    {
        $view = $this->CI->load->view('templates/modal-login', '', true);
        $template = $this->CI->mustache->render($view);
        return $template;
    }


    function modal_register()
    {
        $view = $this->CI->load->view('templates/modal-register', '', true);
        $template = $this->CI->mustache->render($view);
        return $template;
    }


    function modal_vehicle_add()
    {
        $view = $this->CI->load->view('templates/modal-vehicle-add', '', true);
        $template = $this->CI->mustache->render($view);
        return $template;
    }


    function modal_vehicle_sell()
    {
        $view = $this->CI->load->view('templates/modal-vehicle-sell', '', true);
        $template = $this->CI->mustache->render($view);
        return $template;
    }


    function modal_change_password()
    {
        $view = $this->CI->load->view('templates/modal-change-password', '', true);
        $template = $this->CI->mustache->render($view);
        return $template;
    }


    function modal_cost()
    {
        $this -> CI -> load -> model('add_costs_model');
        $type_options = $this -> CI -> add_costs_model -> get_costs_types();
        $types = array();
        foreach($type_options AS $key => $value) {
            $each['key']      = $key;
            $each['value']    = $value;
            array_push( $types, $each );
        }

        $view = $this->CI->load->view('templates/modal-cost', '', true);
        $data = array(
            'cost-date' => date('Y-m-d'),
            'cost-day-month' => date('d.m.'),
            'cost-year' => date('Y'),
            'type' => $types
        );
        $template = $this->CI->mustache->render($view, $data);
        return $template;
    }


    function floater_user()
    {
        $user_id = $this -> CI -> session -> userdata('user_id');

        $this->CI->load->model('Profile_model');
        $model = $this->CI->Profile_model->get_profile( $user_id );

        $username = $model[0]['username'];
        $first    = $model[0]['name'];
        $last     = $model[0]['lastname'];

        if( strlen($first) > 0 && strlen($last) > 0 )
        {
            $displayname = "$first $last";
        }
        else
        {
            if( strlen($first) > 0 )
                $displayname = $first;
            else if( strlen($last) > 0 )
                $displayname = $last;
            else
                $displayname = $username;
        }

        $view = $this->CI->load->view('templates/floater-user', '', true);
        $data = array(
            'img'       => base_url() . 'assets/img/placeholders/user.png',
            'full-name' => $displayname
        );
        $template = $this->CI->mustache->render($view, $data);
        return $template;
    }


    function floater_vehicle()
    {
        $view = $this->CI->load->view('templates/floater-vehicle', '', true);
        $template = $this->CI->mustache->render($view);
        return $template;
    }


    function floater_vehicle_ex()
    {
        $view = $this->CI->load->view('templates/floater-vehicle-ex', '', true);
        $template = $this->CI->mustache->render($view);
        return $template;
    }


    function floater_vehicle_photo()
    {
        $view = $this->CI->load->view('templates/floater-vehicle-photo', '', true);
        $template = $this->CI->mustache->render($view);
        return $template;
    }


    function floater_cost()
    {
        $view = $this->CI->load->view('templates/floater-cost', '', true);
        $template = $this->CI->mustache->render($view);
        return $template;
    }


    function floater_cost_note()
    {
        $view = $this->CI->load->view('templates/floater-cost-note', '', true);
        $template = $this->CI->mustache->render($view);
        return $template;
    }


    function floater_cost_alarm()
    {
        $view = $this->CI->load->view('templates/floater-cost-alarm', '', true);
        $template = $this->CI->mustache->render($view);
        return $template;
    }

}

/* End of file templates_loader.php */