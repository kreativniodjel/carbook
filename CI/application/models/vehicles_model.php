<?php

class Vehicles_model extends CI_Model 
{
    function __construct() 
    {
        parent::__construct();
    }
    
    function show($user_id)
    {
        
        $result = $this -> db -> query ("   SELECT  v.idvehicles, v.name, v.licence_plate, v.idvehicles, v.thumbnail_name, l.manuf_year, j.price, b.km AS km, MAX(k.km) as ent_km,
                                                    l.brand, l.model, l.type, l.gasoline, l.diesel, l.gas, l.electricity
                                            FROM vehicles v
                                            LEFT JOIN log_km k ON v.idvehicles = k.FK_idvehicles
                                            LEFT JOIN vehicles_licences l ON v.FK_idvehicles_licences = l.idvehicles_licences 
                                            LEFT JOIN (SELECT c.FK_idvehicles, SUM(d.price) AS price
                                                    FROM costs c
                                                    LEFT JOIN ( 	SELECT * 
                                                                    FROM  costs_details cd 
                                                                    WHERE cd.entry_time = (	SELECT MAX(cd2.entry_time)
                                                                                            FROM costs_details cd2
                                                                                            WHERE cd2.FK_idcosts = cd.FK_idcosts)) d ON c.idcosts = d.FK_idcosts
                                                    WHERE c.cost_deleted = false
                                                    GROUP BY c.FK_idvehicles
                                                ) j ON j.FK_idvehicles = v.idvehicles
                                            LEFT JOIN (    SELECT o.FK_idvehicles, MAX( z.km ) AS km
                                                            FROM costs o
                                                            LEFT JOIN costs_details z ON o.idcosts = z.FK_idcosts
                                                            GROUP BY o.FK_idvehicles
                                                        ) b ON b.FK_idvehicles = v.idvehicles
                                            WHERE v.FK_idusers = ? AND v.idvehicles IS NOT NULL
                                            AND (   SELECT COUNT(rv.idremoved_vehicles)
                                                    FROM removed_vehicles AS rv
                                                    WHERE rv.FK_idusers=?
                                                    AND rv.FK_idvehicles = v.idvehicles) = 0
                                            GROUP BY v.idvehicles 
                                        ", array($user_id, $user_id));
        if($result -> num_rows() > 0)
        {
             $vehicles = array();
             $i = 0;
             foreach ($result->result_array() as $row)
             {
                 $vehicles[$i]['idvehicles']     = $row['idvehicles'];
                 $vehicles[$i]['name']           = $row['name'];
                 $vehicles[$i]['manuf_year']     = $row['manuf_year'];
                 $vehicles[$i]['licence_plate']  = $row['licence_plate'];
                 if($row['km'] >= $row['ent_km'])
                    $vehicles[$i]['km']             = $row['km'];
                 else
                     $vehicles[$i]['km']            = $row['ent_km'];
                 
                 if($row['price'] === NULL)
                    $vehicles[$i]['costs']          = 0;
                 else
                    $vehicles[$i]['costs']          = $row['price'];
                 
                 $vehicles[$i]['brand']         = $row['brand'];
                 $vehicles[$i]['model']         = $row['model'];
                 $vehicles[$i]['type']          = $row['type'];
                 $vehicles[$i]['gasoline']      = $row['gasoline'];
                 $vehicles[$i]['diesel']        = $row['diesel'];
                 $vehicles[$i]['gas']           = $row['gas'];
                 $vehicles[$i]['electricity']   = $row['electricity'];
                 if(empty($row['thumbnail_name']))
                     $vehicles[$i]['thumbnail_name']= "no_image.gif";
                 else   
                    $vehicles[$i]['thumbnail_name']= $row['thumbnail_name'];
                 
                 $i++;
                 
                 
             }
            $result ->free_result(); 
            return $vehicles;
        }
        return FALSE;
    }
    
    function show_ex_vehicles($user_id)
    {
        $result = $this -> db -> query ("   SELECT  v.idvehicles, v.name, v.licence_plate, v.idvehicles, v.thumbnail_name, l.manuf_year, j.price, b.km AS km, MAX(k.km) as ent_km,
                                                    l.brand, l.model, l.type, l.gasoline, l.diesel, l.gas, l.electricity
                                            FROM vehicles v
                                            LEFT JOIN log_km k ON v.idvehicles = k.FK_idvehicles
                                            LEFT JOIN vehicles_licences l ON v.FK_idvehicles_licences = l.idvehicles_licences 
                                            LEFT JOIN (SELECT c.FK_idvehicles, SUM(d.price) AS price
                                                    FROM costs c
                                                    LEFT JOIN ( 	SELECT * 
                                                                    FROM  costs_details cd 
                                                                    WHERE cd.entry_time = (	SELECT MAX(cd2.entry_time)
                                                                                            FROM costs_details cd2
                                                                                            WHERE cd2.FK_idcosts = cd.FK_idcosts)) d ON c.idcosts = d.FK_idcosts
                                                    WHERE c.cost_deleted = false
                                                    GROUP BY c.FK_idvehicles
                                                ) j ON j.FK_idvehicles = v.idvehicles
                                            LEFT JOIN (    SELECT o.FK_idvehicles, MAX( z.km ) AS km
                                                            FROM costs o
                                                            LEFT JOIN costs_details z ON o.idcosts = z.FK_idcosts
                                                            GROUP BY o.FK_idvehicles
                                                        ) b ON b.FK_idvehicles = v.idvehicles
                                            WHERE v.FK_idusers = ? AND v.idvehicles IS NOT NULL
                                            AND (   SELECT COUNT(rv.idremoved_vehicles)
                                                    FROM removed_vehicles AS rv
                                                    WHERE rv.FK_idusers=?
                                                    AND rv.FK_ididremoved_vehicles_types = 2
                                                    AND rv.FK_idvehicles = v.idvehicles) > 0
                                            GROUP BY v.idvehicles 
                                        ", array($user_id, $user_id));
        if($result -> num_rows() > 0)
        {
             $vehicles = array();
             $i = 0;
             foreach ($result->result_array() as $row)
             {
                 $vehicles[$i]['idvehicles']     = $row['idvehicles'];
                 $vehicles[$i]['name']           = $row['name'];
                 $vehicles[$i]['manuf_year']     = $row['manuf_year'];
                 $vehicles[$i]['licence_plate']  = $row['licence_plate'];
                 if($row['km'] >= $row['ent_km'])
                    $vehicles[$i]['km']             = $row['km'];
                 else
                     $vehicles[$i]['km']            = $row['ent_km'];
                 
                 if($row['price'] === NULL)
                    $vehicles[$i]['costs']          = 0;
                 else
                    $vehicles[$i]['costs']          = $row['price'];
                 
                 $vehicles[$i]['brand']         = $row['brand'];
                 $vehicles[$i]['model']         = $row['model'];
                 $vehicles[$i]['type']          = $row['type'];
                 $vehicles[$i]['gasoline']      = $row['gasoline'];
                 $vehicles[$i]['diesel']        = $row['diesel'];
                 $vehicles[$i]['gas']           = $row['gas'];
                 $vehicles[$i]['electricity']   = $row['electricity'];
                 
                 if(empty($row['thumbnail_name']))
                     $vehicles[$i]['thumbnail_name']= "no_image.gif";
                 else   
                    $vehicles[$i]['thumbnail_name']= $row['thumbnail_name'];
                 
                 $i++;
                 
                 
             }
            $result ->free_result(); 
            return $vehicles;
        }
        return FALSE;
    }
    
    
    function delete($idvehicle, $idusers)
    {
        $date       = date("Y-m-d H:i:s");
         $this->db->trans_start();
        // WARNING must have value 1 DELETED VEHICLE in table removed_vehicles_types!
        $this -> db -> query ("INSERT INTO removed_vehicles (FK_idusers, FK_idvehicles, date, FK_ididremoved_vehicles_types)
                               VALUES(?, ?, ?, 1)",
                               array($idusers, $idvehicle, $date));
       /*$this-> db -> delete('vehicles', array('idvehicles' => $idvehicle));*/ 
       /* $this -> db -> query ("UPDATE vehicles
                               SET deleted = true 
                               WHERE idvehicles = ?", 
                               array($idvehicle));*/
        $this->db->trans_complete();
        if ($this->db->trans_status() === FALSE)
        {   
            return FALSE;
        }
        return TRUE;
    }
    
    function sell($idvehicle, $price, $location, $idusers)
    {
        $date       = date("Y-m-d");
        $this->db->trans_start();
        $this -> db -> query ("INSERT INTO ads (FK_idusers, FK_idvehicles, price, location, date)
                               VALUES(?, ?, ?, ?, ?)",
                               array($idusers, $idvehicle, $price, $location, $date));
        $this->db->trans_complete();
        if ($this->db->trans_status() === FALSE)
        {   
            return FALSE;
        }
        return TRUE;
       
    }

    function switch_to_current($idvehicle, $idusers)
    {
        $this->db->trans_start();
        $this -> db -> query ("DELETE FROM removed_vehicles WHERE FK_idusers=? AND FK_idvehicles=?",
                               array($idusers, $idvehicle));
        $this->db->trans_complete();
        if ($this->db->trans_status() === FALSE)
        {   
            return FALSE;
        }
        return TRUE;
       
    }

    function switch_to_ex($idvehicle, $idusers)
    {
        $date       = date("Y-m-d H:i:s");
        $this->db->trans_start();
        // WARNING must have value 2 Ex vehicle in table removed_vehicles_types!
        $this -> db -> query ("INSERT INTO removed_vehicles (FK_idusers, FK_idvehicles, date, FK_ididremoved_vehicles_types)
                               VALUES(?, ?, ?, 2)",
                               array($idusers, $idvehicle, $date));
        $this->db->trans_complete();
        if ($this->db->trans_status() === FALSE)
        {   
            return FALSE;
        }
        return TRUE;
       
    }
    
    function delete_ex($idvehicle, $idusers)
    {
        $date       = date("Y-m-d H:i:s");
         $this->db->trans_start();
        // WARNING must have value 1 DELETED VEHICLE in table removed_vehicles_types!
        $this -> db -> query ("UPDATE removed_vehicles 
                               SET date = ?,FK_ididremoved_vehicles_types = 1
                               WHERE FK_idusers=? AND FK_idvehicles=?",
                               array($date, $idusers, $idvehicle));
        $this->db->trans_complete();
        if ($this->db->trans_status() === FALSE)
        {   
            return FALSE;
        }
        return TRUE;
    }
}


/* End of file vehicles_model */