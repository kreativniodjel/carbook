<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Homepage_model extends CI_Model
{
    function __construct() 
    {
        parent::__construct();
        
    }
    
    public function get_user($userid, $username)
    {
        $query = $this->db->query("SELECT * FROM users WHERE username = ? AND idusers = ? AND confirmed = TRUE",  array($username, $userid));
        
        // If user exist in database num_rows() will be 1
        if($query -> num_rows() > 0)
        {
            $query          = $query -> row_array();
            $db_user_id     = $query['idusers'];
            $db_username    = $query['username'];
            
            // Setting data to session
            if(! $this -> session -> userdata('user_id'))
            {
                $data = array(
                        'user_id'   => $db_user_id,   
                        'username'  => $db_username
                );
                $this -> session -> set_userdata($data);
            }
            return TRUE;
        }
        return FALSE;
    }

}


/* End of file homepage_model.php */
