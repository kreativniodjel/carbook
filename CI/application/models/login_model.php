<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Login_model extends CI_Model
{
    function __construct() 
    {
        parent::__construct();
        
    }
    
    public function get_user($username, $password)
    {
        $query = $this->db->query("SELECT * FROM users WHERE username = ? AND password = ? AND confirmed = 1",  array($username, $password));
        
        // If user exist in database num_rows() will be 1
        if($query -> num_rows() > 0)
        {
            $query          = $query -> row_array();
            $db_user_id     = $query['idusers'];
            $db_username    = $query['username'];
            
            // Setting data to session
            if(! $this -> session -> userdata('user_id'))
            {
                $data = array(
                        'user_id'   => $db_user_id,   
                        'username'  => $db_username
                );
                $this -> session -> set_userdata($data);
            }
            return TRUE;
        }
        else 
        {
            //echo "Invalid username or password!";
        }
        
        return FALSE;
    }
    
    
    
    function reset_email($email)
    {
        $query = $this->db->query(" SELECT idusers 
                                    FROM users 
                                    WHERE email=?
                                  ", array($email));
        
        if($query -> num_rows() > 0)
        {
            $query          = $query -> row_array();
            $this -> db ->trans_start();
            $this -> db -> query (" UPDATE users
                                    SET     password = ?
                                    WHERE   idusers = ?
                                    ", array(md5($this->create_random_string(10)), $query['idusers']));
            $this -> db -> trans_complete();
            if ($this -> db -> trans_status() === FALSE)
                return "error_change_password";
            
           /* $subject = "<p>
                        Click <a href>here</a> and enter provided code: 
                        </p>", 2;
            $headers = "From: webmaster@carbook.com";
            if(mail($email, $subject, $message, $headers))
                return TRUE;
            else
                return "error_sending_mail";*/
        }
        return "error_no_email_in_database";
    }
    
    
    
    

}


/* End of file login_model.php */