<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Yellow_pages_model extends CI_Model 
{
    function __construct() 
    {
        parent::__construct();
    }
    
    function get_yp()
    {
        $yp = array();
        
        $result = $this -> db -> query ("SELECT * FROM yellow_pages WHERE FK_idyptype = 1;");
        if($result -> num_rows() > 0)
        {
             $i = 0;
             foreach ($result->result_array() as $row)
             {
                 $yp[$i]['name']            = $row['name'];
                 $yp[$i]['text']            = $row['text'];
                 $yp[$i]['address']         = $row['address'];
                 $yp[$i]['city']            = $row['city'];
                 $yp[$i]['website']         = $row['website'];
                 $yp[$i]['phone']           = $row['phone'];
                 $yp[$i]['email']           = $row['email'];
                 $yp[$i]['thumbnail_path']  = $row['thumbnail_path'];
                 $yp[$i]['banner_path']     = $row['banner_path'];
                 $i++;
             }
            $result ->free_result(); 
        }
        return $yp;
    }
    
    function get_yp_service()
    {
        $yp = array();
        
        $result = $this -> db -> query ("SELECT * FROM yellow_pages WHERE FK_idyptype = 2;");
        if($result -> num_rows() > 0)
        {
             $i = 0;
             foreach ($result->result_array() as $row)
             {
                 $yp[$i]['name']            = $row['name'];
                 $yp[$i]['text']            = $row['text'];
                 $yp[$i]['address']         = $row['address'];
                 $yp[$i]['city']            = $row['city'];
                 $yp[$i]['website']         = $row['website'];
                 $yp[$i]['phone']           = $row['phone'];
                 $yp[$i]['email']           = $row['email'];
                 $yp[$i]['thumbnail_path']  = $row['thumbnail_path'];
                 $yp[$i]['banner_path']     = $row['banner_path'];
                 $i++;
             }
            $result ->free_result(); 
        }
        return $yp;
    }
    
    
}
/* End of file yellow_pages_model.php */
