<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Ads_model extends CI_Model 
{
    function __construct() 
    {
        parent::__construct();
    }
    
    function get_ads()
    {
        $result = $this -> db -> query ("   SELECT  a.idads, a.date, a.price, a.location, v.thumbnail_name AS thumb, v.name, l.model, l.brand, l.type, l.manuf_year AS year, MAX(k.km) as km_log, l.gasoline, l.diesel, l.gas, l.electricity, km_cost 
                                            FROM (SELECT * FROM ads b ORDER BY b.idads DESC) as a
                                            LEFT JOIN vehicles v ON v.idvehicles = a.FK_idvehicles
                                            LEFT JOIN vehicles_licences l ON v.FK_idvehicles_licences = l.idvehicles_licences
                                            LEFT JOIN log_km k ON k.FK_idvehicles = v.idvehicles
                                            LEFT JOIN (     SELECT o.FK_idvehicles, MAX( z.km ) AS km_cost
                                                            FROM costs o
                                                            LEFT JOIN costs_details z ON o.idcosts = z.FK_idcosts
                                                            GROUP BY o.FK_idvehicles
                                                        ) b ON b.FK_idvehicles = v.idvehicles
                                            GROUP BY v.idvehicles
                                        ");
        if($result -> num_rows() > 0)
        {
             $ads = array();
             $i = 0;
             foreach ($result->result_array() as $row)
             {
                 $ads[$i]['name']      = $row['name'];
                 $ads[$i]['brand']     = $row['brand'];
                 $ads[$i]['type']      = $row['type'];
                 $ads[$i]['model']     = $row['model'];
                 $ads[$i]['year']      = $row['year'];
                 $ads[$i]['km']        = $row['km_cost'];
                 if($row['km_log']    >= $row['km_cost'])
                    $ads[$i]['km']         = $row['km_log'];
                 $ads[$i]['date']      = $row['date'];
                 $ads[$i]['price']     = $row['price'];
                 $ads[$i]['location']  = $row['location'];
                 $ads[$i]['fuel']      = $this -> get_fuel($row);
                 
                  if(empty($row['thumb']))
                     $ads[$i]['thumb']= "no_image.gif";
                 else
                     $ads[$i]['thumb']            = $row['thumb'];
                 $i++;
                 
             }
             
            $result ->free_result(); 
            return $ads;
        }
        
        return FALSE;
    }
    
    
    function get_fuel($row)
    {
        $fuel = '';
        if($row['gasoline'] == TRUE)
            $fuel = $fuel.'Benzin ';
        if($row['diesel'] == TRUE)
            $fuel = $fuel.'Dizel ';
        if($row['gas'] == TRUE)
            $fuel = $fuel.'Plin ';
        if($row['electricity'] == TRUE)
            $fuel = $fuel.'Električni '; 
        
        return $fuel;
    }
}
/* End of file ads_model.php */