<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Add_costs_model extends CI_Model
{
    function __construct() 
    {
        parent::__construct();
        
    }
    
    /**
    * Get costs types from database
    *
    * @access	public
    * @param	array
    * @return	bool
    */
    public function get_costs_types()
    {
        $result = $this -> db -> query ("   SELECT idcosts_types, type
                                            FROM costs_types
                                        ");
        if($result -> num_rows() > 0)
        {
             $type_options = array();
             foreach ($result->result_array() as $row)
             {
                 $type_options[$row['idcosts_types']] = $row['type'];
             }
            $result ->free_result(); 
            return $type_options;
        }
    }
    
    /**
    * Get costs from database
    *
    * @access	public
    * @param	array
    * @return	bool
    */
    public function get_cost($idcosts_details)
    {
        $result = $this -> db -> query ("   SELECT * 
                                            FROM costs_details
                                            WHERE idcosts_details = ?
                                        ", array($idcosts_details));
        if($result -> num_rows() > 0)
        {
            $cost = $result -> result_array();
            $result ->free_result(); 
            return $cost;
        }
        
        return FALSE;
    }
    
    /**
    * Add costs to database
    *
    * @access	public
    * @param	array
    * @return	bool
    */
    public function add_costs($post_data, $id_costs)
    {
        $user_id    = $this -> session -> userdata('user_id');
        $vehicle_id = $this -> session -> userdata('cur_vehicle_id');
        if(($vehicle_id < 0)||($user_id < 0))
            return FALSE;
        
        KO_check_array_values($post_data);
        
        if(!KO_date_exist($post_data['date']))
            $post_data['date'] = NULL;
        else 
            $post_data['date'] = date("Y-m-d", strtotime($post_data['date']));
        
        if(!KO_date_exist($post_data['alarm_date']))
            $post_data['alarm_date'] = NULL;
        else 
            $post_data['alarm_date'] = date("Y-m-d", strtotime($post_data['alarm_date']));
        
        
        $this -> db ->trans_start();
        if($id_costs === -1)
        {
            $this -> db -> query ("INSERT INTO costs (FK_idvehicles, cost_deleted) 
                               VALUES (?, 'FALSE')", 
                               array($vehicle_id));
            $id_costs   = $this->db->insert_id();    
        }
        
        $date       = date("Y-m-d H:i:s");
        $this -> db -> query ("INSERT INTO costs_details (FK_idcosts, FK_idusers, FK_idcosts_types, entry_time, date, km, place_of_cost, note, quantity, price, alarm_km, alarm_date) 
                               VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)", 
                               array($id_costs, $user_id, $post_data['type'], $date, $post_data['date'], $post_data['km'], $post_data['place_of_cost'], 
                                      $post_data['note'], $post_data['quantity'], $post_data['price'],  $post_data['alarm_km'], $post_data['alarm_date']));
        
        $this -> db -> trans_complete();
                
        if ($this -> db -> trans_status() === FALSE)
        {   
            return FALSE;
        }
       
        return TRUE;
    }
    
     /**
    * Edit costs in database
    *
    * @access	public
    * @param	array
    * @return	bool
    */
    public function edit_costs($post_data, $id_costs)
    {
        $user_id    = $this -> session -> userdata('user_id');
        KO_check_array_values($post_data);
        
        if(!KO_date_exist($post_data['date']))
            $post_data['date'] = NULL;
        else 
            $post_data['date'] = date("Y-m-d", strtotime($post_data['date']));
        
        if(!KO_date_exist($post_data['alarm_date']))
            $post_data['alarm_date'] = NULL;
        else 
            $post_data['alarm_date'] = date("Y-m-d", strtotime($post_data['alarm_date']));
        
        $this -> db ->trans_start();
        
        $date       = date("Y-m-d H:i:s");
        $this -> db -> query ("INSERT INTO costs_details (FK_idcosts, FK_idusers, FK_idcosts_types, entry_time, date, km, place_of_cost, note, quantity, price, alarm_km, alarm_date) 
                               VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)", 
                               array($id_costs, $user_id, $post_data['type'], $date, $post_data['date'], $post_data['km'], $post_data['place_of_cost'], 
                                      $post_data['note'], $post_data['quantity'], $post_data['price'],  $post_data['alarm_km'], $post_data['alarm_date']));
        
        $this -> db -> trans_complete();
                
        if ($this -> db -> trans_status() === FALSE)
        {   
            return FALSE;
        }
       
        return TRUE;
    }
}


/* End of file add_costs_model.php */
