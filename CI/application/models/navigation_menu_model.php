<?php

class Navigation_menu_model extends CI_Model
{
    public $alarms          = array();
    public $alarms_grouped  = array();
    
    function __construct() 
    {
        parent::__construct();
    }
    
    function get_user_vehicles($user_id, $show_ex_vehicles)
    {
        $vehicles = array(
                 '0' => 'Sva vozila'
             );
        
        $result = NULL;
        //- if we need to show ex vehicles we will use different query!
        if($show_ex_vehicles === TRUE)
        {
            $result = $this -> db -> query ("SELECT idvehicles, FK_idusers, name, licence_plate
                                          FROM vehicles
                                          WHERE FK_idusers = ?  AND idvehicles IS NOT NULL
                                           AND (   SELECT COUNT(rv.idremoved_vehicles)
                                                    FROM removed_vehicles AS rv
                                                    WHERE rv.FK_idusers=?  
                                                    AND FK_ididremoved_vehicles_types <> 2
                                                    AND rv.FK_idvehicles = idvehicles) = 0
                                        ", array($user_id, $user_id));
        }
        else
        {
            $result = $this -> db -> query ("SELECT idvehicles, FK_idusers, name, licence_plate
                                          FROM vehicles
                                          WHERE FK_idusers = ?  AND idvehicles IS NOT NULL
                                          AND (   SELECT COUNT(rv.idremoved_vehicles)
                                                    FROM removed_vehicles AS rv
                                                    WHERE rv.FK_idusers=?  
                                                    AND rv.FK_idvehicles = idvehicles) = 0
                                        ", array($user_id, $user_id));
        }
        
        if(!$result)
            return $vehicles;
        
        if($result -> num_rows() > 0)
        {
             foreach ($result->result_array() as $row)
             {
                 $vehicles[$row['idvehicles']] = $row['name'].' - '.$row['licence_plate'];
             }
            $result ->free_result(); 
        }
        return $vehicles;
    }
    
    function get_vehicles_costs($user_id)
    {
        $ret_val = 0;
        $result = $this -> db -> query ("   SELECT SUM(d.price) as price
                                            FROM costs c
                                            LEFT JOIN ( SELECT * FROM  costs_details cd WHERE cd.entry_time = (SELECT MAX(cd2.entry_time)
                                                                FROM costs_details cd2
                                                                where cd2.FK_idcosts = cd.FK_idcosts)) d ON c.idcosts = d.FK_idcosts
                                            LEFT JOIN costs_types t ON d.FK_idcosts_types = t.idcosts_types
                                            LEFT JOIN vehicles v ON c.FK_idvehicles = v.idvehicles
                                            WHERE d.FK_idusers = ? AND c.cost_deleted = false AND c.FK_idvehicles IS NOT NULL
                                            AND (   SELECT COUNT(rv.idremoved_vehicles)
                                                    FROM removed_vehicles AS rv
                                                    WHERE rv.FK_idusers=?
                                                    AND rv.FK_idvehicles = c.FK_idvehicles) = 0
                                        ", array($user_id, $user_id));
        if($result -> num_rows() > 0)
        {
            $cost = $result -> result_array();
            $ret_val = $cost[0]['price'];
            $result ->free_result(); 
            if(empty($ret_val))
                $ret_val = 0;
                
            return $ret_val;
        }
        return $ret_val;
    }
    
    function get_vehicles_alarms($user_id)
    {
        $result = $this -> db -> query ("   SELECT  v.idvehicles, d.alarm_km as alarm_km, d.alarm_date as alarm_date, c.idcosts, d.idcosts_details, d.date, d.km, d.place_of_cost, d.note, d.quantity, d.price,  t.type, t.idcosts_types, v.name, v.thumbnail_name, t.thumb_alarm, t.thumb_sm_alarm
                                            FROM costs c
                                            LEFT JOIN ( SELECT * FROM  costs_details cd WHERE cd.entry_time = (SELECT MAX(cd2.entry_time)
                                                                FROM costs_details cd2
                                                                where cd2.FK_idcosts = cd.FK_idcosts)) d ON c.idcosts = d.FK_idcosts
                                            LEFT JOIN costs_types t ON d.FK_idcosts_types = t.idcosts_types
                                            LEFT JOIN vehicles v ON c.FK_idvehicles = v.idvehicles
                                            WHERE d.FK_idusers = ? AND c.cost_deleted = false AND c.FK_idvehicles IS NOT NULL
                                            AND (   SELECT COUNT(rv.idremoved_vehicles)
                                                    FROM removed_vehicles AS rv
                                                    WHERE rv.FK_idusers=?
                                                    AND rv.FK_idvehicles = c.FK_idvehicles) = 0
                                            ORDER BY d.idcosts_details ASC
                                        ", array($user_id, $user_id));
        
        if($result -> num_rows() > 0)
        {
             $costs = array();
             $i = 0;
             foreach ($result->result_array() as $row)
             {
                 $costs[$i]['id']               = $row['idcosts'];
                 $costs[$i]['idcosts_details']  = $row['idcosts_details'];
                 $costs[$i]['idcosts_types']    = $row['idcosts_types'];
                 $costs[$i]['thumb']            = $row['thumbnail_name'];
                 $costs[$i]['name']             = $row['name'];
                 $costs[$i]['date']             = $row['date'];
                 $costs[$i]['km']               = $row['km'];
                 $costs[$i]['type']             = $row['type'];
                 $costs[$i]['place_of_cost']    = $row['place_of_cost'];
                 $costs[$i]['note']             = $row['note'];
                 $costs[$i]['quantity']         = $row['quantity'];
                 $costs[$i]['price']            = $row['price'];
                 $costs[$i]['alarm_km']         = $row['alarm_km'];
                 $costs[$i]['alarm_date']       = $row['alarm_date'];
                 $costs[$i]['idvehicles']       = $row['idvehicles'];
                 $costs[$i]['thumb_alarm']      = $row['thumb_alarm'];
                 $costs[$i]['thumb_sm_alarm']   = $row['thumb_sm_alarm'];
                 
                 if(KO_date_exist($row['alarm_date']))
                 {
                     $costs[$i]['month']            = date("m", strtotime($row['alarm_date']));
                     $costs[$i]['year']             = date("Y", strtotime($row['alarm_date']));
                 }
                 else
                 {
                     $costs[$i]['month']            = "";
                     $costs[$i]['year']             = "";
                 }
                 if(empty($row['thumbnail_name']))
                     $costs[$i]['thumb']            = "no_image.gif";
                 else
                     $costs[$i]['thumb']            = $row['thumbnail_name'];
                 
                 if(empty($row['thumb_alarm']))
                     $costs[$i]['thumb_alarm']      = base_url()."images/no_image.png";
                 else
                     $costs[$i]['thumb_alarm']      = base_url()."images/".$row['thumb_alarm'];
                 
                 if(empty($row['thumb_sm_alarm']))
                     $costs[$i]['thumb_sm_alarm']      = base_url()."images/no_image_sm.png";
                 else
                     $costs[$i]['thumb_sm_alarm']      = base_url()."images/".$row['thumb_sm_alarm'];
                 
                 $i++;
             }
            $result ->free_result(); 
            
            /******************************************************************/
            //- return wehicles kilometers, its important to have correct value for km based alarms!
            $vehicles   = $this -> return_vehicles_km_from_costs($costs);
            // When we have all vehicles and max km from costs, look at log_km table and find if km entered there is bigger than max of costs!
            // In that way we will have max km for vehicles.
            $vehicles   = $this -> compare_vehivles_km_with_km_log($vehicles, $user_id);
            /******************************************************************/
            //- now go through all costs and get alarms for vehicles!
            $all_alarms                 = $this -> return_alarms_from_costs($costs, $vehicles);
            //- then check if alarms are active
            $this -> alarms             = $this -> check_alarms($all_alarms, $costs);
            //- at the end count costs
            $this -> alarms_grouped     = $this -> group_alarms($this -> alarms);
            return  $this -> alarms_grouped;
        }
        return array();
    }
    
    function return_vehicles_km_from_costs($costs)
    {
        $vehicles = array();
        foreach($costs as $c)
        {
            $curr_veh   = NULL;
            $i          = 0;
            foreach($vehicles as $v)
            {
                if($v['id'] == $c['idvehicles'])
                {
                    $curr_veh = $v;
                    break;
                }
                $i++;
            }
            
            if(!$curr_veh)
            {
                $curr_veh       = array();
                $curr_veh['id'] = $c['idvehicles'];
                $curr_veh['km'] = $c['km'];
                array_push($vehicles, $curr_veh);
            }
            else
            {
                if( intval($c['km']) > intval($curr_veh['km']))
                    $vehicles[$i]['km'] = $c['km'];
            }
           
        }
        return $vehicles;
    }
    
    function compare_vehivles_km_with_km_log($vehicles, $user_id)
    {
        $result = $this -> db -> query ("   SELECT * FROM (SELECT * FROM log_km WHERE FK_idusers=? ORDER BY idlog_km DESC) c
                                            GROUP BY FK_idvehicles  
                                        ", array($user_id));
        if($result -> num_rows() > 0)
        {
             foreach ($result->result_array() as $row)
             {
                 $i = 0;
                 foreach($vehicles as $v)
                 {
                     if($row['FK_idvehicles'] == $v['id'])
                     {
                         if(intval($row['km']) > intval($v['km']))
                         {
                            $vehicles[$i]['km'] =  $row['km'];
                         }
                         break;
                     }
                     $i++;
                 }
             }
             $result ->free_result(); 
        }
        return $vehicles;
    }
    
    function return_alarms_from_costs($costs, $vehicles)
    {
        $alarms = array();
        
        $todays_date    = date("Y-m-d");
        $today          = strtotime($todays_date);
        foreach($costs as $c)
        {
            //- if alarm date is entered and is greater or equal as current date add it to alarms
            if((strtotime($c['alarm_date']) <= $today)&&(!empty($c['alarm_date']))&&($c['alarm_date'] != '0000-00-00')&&($c['year'] > 1950))
            {
                $new_alarm                  = array();
                $new_alarm                  = $c;
                $new_alarm['alarm_type_id']    = 0; //- 0 is date based alarm
                array_push($alarms, $new_alarm);
            }
            else if((!empty($c['alarm_km']))&&($c['alarm_km']>0))
            {
                //- compare alarm_km with vehicles max km to know if alarm is still active!
                foreach($vehicles as $v)
                {
                    if($v['id'] == $c['idvehicles'])
                    {
                        if(intval($v['km']) >= intval($c['alarm_km']))
                        {
                            $new_alarm                  = array();
                            $new_alarm                  = $c;
                            $new_alarm['alarm_type_id']    = 1; //- 1 is km based alarm
                            array_push($alarms, $new_alarm);
                        }
                        break;
                    }
                }
            }
        }
        return $alarms;
    }
    
    function check_alarms($all_alarms, $costs)
    {
        $i = 0;
        // foreach alarm check if there is 
        foreach($all_alarms as $alarm)
        {
            //echo "<br/>ALARM:".$alarm['type']." veh:".$alarm['idvehicles'].", ".$alarm['name']."<br/>";
            // need to go through all costs to find if some alarms arent active anymore
            foreach($costs as $c)
            {
                //echo $alarm['idvehicles']."=".$c['idvehicles']." costs_type:".$alarm['idcosts_types']."=". $c['idcosts_types']." iddetails:".$alarm['idcosts_details'].">".$c['idcosts_details']."</br>";
                //if same vehicle and  if same cost type and different idcosts_details(no need to compare with himself!) 
                if(($alarm['idvehicles'] == $c['idvehicles'])&&($alarm['idcosts_types'] == $c['idcosts_types'])&&($c['idcosts_details'] > $alarm['idcosts_details']))
                {
                    
                    unset($all_alarms[$i]);
                
                    // if km based alarm check km
                    /*  if($alarm['alarm_type_id'] == 1)
                    {
                        if(intval($c['km']) > intval($alarm['km']))
                        {
                            // remove alarm!
                           
                        }
                    }
                    else // if date based alarm check date
                    {
                    }*/
                }
            }
            $i++;
        }
        return $all_alarms;
    }
            
    function group_alarms($alarms)
    {
        $alarms_grouped = array();
        foreach($alarms as $a)
        {
            $curr_group = NULL;
            $i          = 0;
            foreach($alarms_grouped as $g)
            {
                if($g['idcosts_types'] == $a['idcosts_types'])
                {
                    $curr_group = $g;
                    $alarms_grouped[$i]['count']++;
                    break;
                }
                $i++;
            }
            if(!$curr_group)
            {
                $new_group = array();
                $new_group['idcosts_types'] = $a['idcosts_types'];
                $new_group['type']          = $a['type'];
                $new_group['thumb_alarm']   = $a['thumb_alarm'];
                $new_group['count']         = 1;
                array_push($alarms_grouped, $new_group);
            }
        }
        return $alarms_grouped;
    }
}
/* End of file navigation_menu_model.php*/