<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Costs_model extends CI_Model 
{
    function __construct() 
    {
        parent::__construct();
    }
    
    function get_vehicles_costs($user_id, $vehicle_id)
    {
        $result = $this -> db -> query ("   SELECT  c.idcosts, d.idcosts_details, d.date, d.km, d.place_of_cost, d.note, d.quantity, d.price, d.alarm_km, t.type
                                            FROM costs c
                                            LEFT JOIN ( SELECT * FROM  costs_details cd WHERE cd.entry_time = (SELECT MAX(cd2.entry_time)
                                                                FROM costs_details cd2
                                                                where cd2.FK_idcosts = cd.FK_idcosts)) d ON c.idcosts = d.FK_idcosts
                                            LEFT JOIN costs_types t ON d.FK_idcosts_types = t.idcosts_types
                                            WHERE c.FK_idvehicles = ? AND d.FK_idusers = ? AND c.cost_deleted = false
                                            ORDER BY d.date DESC
                                        ", array($vehicle_id, $user_id));
        if($result -> num_rows() > 0)
        {
             $costs = array();
             $i = 0;
             foreach ($result->result_array() as $row)
             {
                 $costs[$i]['id']               = $row['idcosts'];
                 $costs[$i]['idcosts_details']  = $row['idcosts_details'];
                 $costs[$i]['date']             = date('d.m.Y.', strtotime($row['date']));
                 $costs[$i]['km']               = $row['km'];
                 $costs[$i]['type']             = $row['type'];
                 $costs[$i]['place_of_cost']    = $row['place_of_cost'];
                 $costs[$i]['note']             = $row['note'];
                 $costs[$i]['quantity']         = $row['quantity'];
                 $costs[$i]['price']            = $row['price'];
                 $costs[$i]['alarm_km']         = $row['alarm_km'];
                 
                 if(KO_date_exist($row['date']))
                 {
                     $costs[$i]['month']            = date("m", strtotime($row['date']));
                     $costs[$i]['year']             = date("Y", strtotime($row['date']));
                 }
                 else
                 {
                     $costs[$i]['month']            = "";
                     $costs[$i]['year']             = "";
                 }
                 
                 $i++;
             }
            $result ->free_result(); 
            //$costs = $this -> sortByOneKey($costs, 'date');
            return $costs;
        }
        return FALSE;
    }
    
    function get_all_costs($user_id)
    {
        $result = $this -> db -> query ("   SELECT  c.idcosts, d.idcosts_details, d.date, d.km, d.place_of_cost, d.note, d.quantity, d.price, d.alarm_km, t.type, v.name, v.thumbnail_name
                                            FROM costs c
                                            LEFT JOIN ( SELECT * FROM  costs_details cd WHERE cd.entry_time = (SELECT MAX(cd2.entry_time)
                                                                FROM costs_details cd2
                                                                where cd2.FK_idcosts = cd.FK_idcosts)) d ON c.idcosts = d.FK_idcosts
                                            LEFT JOIN costs_types t ON d.FK_idcosts_types = t.idcosts_types
                                            LEFT JOIN vehicles v ON c.FK_idvehicles = v.idvehicles
                                            WHERE d.FK_idusers = ? AND c.cost_deleted = false AND c.FK_idvehicles IS NOT NULL
                                            AND (   SELECT COUNT(rv.idremoved_vehicles)
                                                    FROM removed_vehicles AS rv
                                                    WHERE rv.FK_idusers=?
                                                    AND rv.FK_idvehicles = c.FK_idvehicles) = 0
                                            ORDER BY d.date DESC
                                        ", array($user_id, $user_id));
        if($result -> num_rows() > 0)
        {
             $costs = array();
             $i = 0;
             foreach ($result->result_array() as $row)
             {
                 $costs[$i]['id']               = $row['idcosts'];
                 $costs[$i]['idcosts_details']  = $row['idcosts_details'];
                 $costs[$i]['thumb']            = $row['thumbnail_name'];
                 $costs[$i]['name']             = $row['name'];
                 $costs[$i]['date']             = date('d.m.Y.', strtotime($row['date']));
                 $costs[$i]['km']               = $row['km'];
                 $costs[$i]['type']             = $row['type'];
                 $costs[$i]['place_of_cost']    = $row['place_of_cost'];
                 $costs[$i]['note']             = $row['note'];
                 $costs[$i]['quantity']         = $row['quantity'];
                 $costs[$i]['price']            = $row['price'];
                 $costs[$i]['alarm_km']         = $row['alarm_km'];
                 
                 if(KO_date_exist($row['date']))
                 {
                     $costs[$i]['month']            = date("m", strtotime($row['date']));
                     $costs[$i]['year']             = date("Y", strtotime($row['date']));
                 }
                 else
                 {
                     $costs[$i]['month']            = "";
                     $costs[$i]['year']             = "";
                 }           
                 
                 if(empty($row['thumbnail_name']))
                     $costs[$i]['thumb']= "no_image.gif";
                 else
                     $costs[$i]['thumb']            = $row['thumbnail_name'];
                 
                 $i++;
             }
            $result ->free_result(); 
            
            //$costs = $this -> sortByOneKey($costs, 'date');
            return $costs;
        }
        return FALSE;
    }
    
    function delete_cost($id)
    {
        $this->db->trans_start();
        $this -> db -> query ("UPDATE costs
                               SET cost_deleted = true 
                               WHERE idcosts = ?", 
                               array($id));
         
        $this->db->trans_complete();
        
        if ($this->db->trans_status() === FALSE)
        {   
            return FALSE;
        }
       
        return TRUE;
    }
    
    function check_if_vehicle_data_can_be_changed($idvehicles)
    {
        $user_id    = $this -> session -> userdata('user_id');
        $result = $this -> db -> query ("SELECT *
                                          FROM removed_vehicles
                                          WHERE FK_idusers = ?  AND FK_idvehicles = ? 
                                        ", array($user_id, $idvehicles));
        if($result -> num_rows() > 0)
        {
             return FALSE;
        }
        return TRUE;
    }
    
    function sortByOneKey(array $array, $key, $asc = true) 
    {
        $result = array();
        $values = array();
        foreach ($array as $id => $value) 
        {
            $values[$id] = isset($value[$key]) ? $value[$key] : '';
        }

        if ($asc) 
        {
            asort($values);
        }
        else 
        {
            arsort($values);
        }

        foreach ($values as $key => $value) 
        {
            $result[$key] = $array[$key];
        }

        return $result;
    }
    
}
/* End of file costs_model.php*/
