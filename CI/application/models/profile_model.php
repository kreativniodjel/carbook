<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Profile_model extends CI_Model 
{
    function __construct() 
    {
        parent::__construct();
    }
    
    
    
    function get_profile($user_id)
    {
        $result = $this -> db -> query ("   SELECT  *
                                            FROM users u
                                            LEFT JOIN driver_licences d ON d.iddriver_licences = u.FK_iddriver_licenses
                                            LEFT JOIN hak h ON h.idhak = u.FK_idhak
                                            LEFT JOIN genders g ON g.idgender = u.FK_idgender
                                            LEFT JOIN countries c ON c.idcountries = u.FK_idcountry
                                            WHERE u.idusers = ?
                                        ", array($user_id));
                
        if($result -> num_rows() > 0)
        {
            $data = $result -> result_array();

            if(!KO_date_exist($data[0]['birthday']))
                $data[0]['birthday'] = '';

            if(!KO_date_exist($data[0]['valid_thru']))
                $data[0]['valid_thru'] = '';
            
            $result ->free_result(); 
            return $data;
        }
        return FALSE;
    }
    
    function get_gender()
    {
        $data = array();
        $result = $this -> db -> query ("   SELECT  *
                                            FROM genders
                                            ORDER BY gender ASC"
                                            );
                
        if($result -> num_rows() > 0)
        {
            $i = 0;
            foreach ($result->result_array() as $row)
            {
                $data[$row['idgender']]    = $row['gender'];
            }
            $result ->free_result(); 
            return $data;
        }
        return $data;
    }
    
    function get_country()
    {
        $data = array();
        $result = $this -> db -> query ("   SELECT  *
                                            FROM countries
                                            ORDER BY country ASC"
                                            );
                
        if($result -> num_rows() > 0)
        {
            $i = 0;
            $data['1']    = "";
            foreach ($result->result_array() as $row)
            {
                $data[$row['idcountries']]    = $row['country'];
            }
            
            $result ->free_result(); 
            return $data;
        }
        return $data;
    }
    
    
    function update_user_profile($data)
    {
        KO_check_array_values($data);
        if($data['db_country'] == 1)
            $data['db_country'] = NULL;
        
        if(!KO_date_exist($data['db_birthday']))
            $data['db_birthday'] = NULL;
        
        $this->db->trans_start();
        $this -> db -> query (" UPDATE users SET
                                FK_idcountry = ?, FK_idgender= ?, name= ?, lastname= ?, email= ?,
                                phone= ?, address= ?, city= ?, postal_code= ?, birthday= ? 
                                WHERE idusers=?",
                                    array($data['db_country'], $data['db_gender'] , $data['db_name'], $data['db_lastname'], $data['db_email'], $data['db_phone'],
                                    $data['db_address'], $data['db_city'], $data['db_postal_code'], $data['db_birthday'], $data['id_user']));
        $this->db->trans_complete();
        
        if ($this->db->trans_status() === FALSE)
        {   
            return FALSE;
        }
       
        return TRUE;
    }
    function update_licence($driver_lic)
    {
        $user_id = $driver_lic['id_user'];
        KO_check_array_values($driver_lic);
        $driver_lic = $this -> setup_check_array_values($driver_lic);
        $this->db->trans_start();
        // first find if users licence exist
        $result = $this -> db -> query (" SELECT  *
                                FROM users 
                                WHERE idusers = ?
                              ", array($user_id)); 

        $data_array = array();
        if($result -> num_rows() > 0)
        {
            $data_array = $result -> result_array();
        }
        $driver_licence_id = '';
        if(isset($data_array))
            $driver_licence_id  = $data_array[0]['FK_iddriver_licenses'];
     
        //- create new users drivers licence and add it to user!
        if((empty($driver_licence_id))||($driver_licence_id == '')|| $driver_licence_id == 'NULL')
        {
            $this -> db -> query (" INSERT INTO driver_licences 
                                        (A1, A2, A, B, BE, CI, CIE, C, 
                                        CE, D1, D1E, D, DE, F, G, H, AM)
                                    VALUES
                                        (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)"
                                    ,array($driver_lic['db_a1'], $driver_lic['db_a2'], $driver_lic['db_a'], $driver_lic['db_b'], $driver_lic['db_be'], $driver_lic['db_ci'],
                                          $driver_lic['db_cie'], $driver_lic['db_c'], $driver_lic['db_ce'], $driver_lic['db_d1'], $driver_lic['db_d1e'], $driver_lic['db_d'],
                                          $driver_lic['db_de'], $driver_lic['db_f'], $driver_lic['db_g'], $driver_lic['db_h'], $driver_lic['db_am']));
            $driver_licence_id   = $this->db->insert_id(); 
            $this -> db -> query (" UPDATE users SET FK_iddriver_licenses=? WHERE idusers=?"
                                    , array($driver_licence_id, $user_id));
        }
        //- else users driver licence exist, update data
        else
        {
            $this -> db -> query (" UPDATE driver_licences SET
                                        A1=?, A2=?, A=?, B=?, BE=?, CI=?, CIE=?, C=?, 
                                        CE=?, D1=?, D1E=?, D=?, DE=?, F=?, G=?, H=?, AM=? 
                                        WHERE iddriver_licences = ?"
                                     ,array($driver_lic['db_a1'], $driver_lic['db_a2'], $driver_lic['db_a'], $driver_lic['db_b'], $driver_lic['db_be'], $driver_lic['db_ci'],
                                          $driver_lic['db_cie'], $driver_lic['db_c'], $driver_lic['db_ce'], $driver_lic['db_d1'], $driver_lic['db_d1e'], $driver_lic['db_d'],
                                          $driver_lic['db_de'], $driver_lic['db_f'], $driver_lic['db_g'], $driver_lic['db_h'], $driver_lic['db_am'], $driver_licence_id));
        }
        $this->db->trans_complete();
        
        if ($this->db->trans_status() === FALSE)
        {   
            return FALSE;
        }
       
        return TRUE;
    }

    function update_hak($hak)
    {
        $user_id = $hak['id_user'];
        KO_check_array_values($hak);

        if(!KO_date_exist($hak['valid_thru']))
            $hak['valid_thru'] = NULL;
        
        $this->db->trans_start();
        // first find if users licence exist
        $result = $this -> db -> query (" SELECT  *
                                FROM users 
                                WHERE idusers = ?
                              ", array($user_id)); 

        $data_array = array();
        if($result -> num_rows() > 0)
        {
            $data_array = $result -> result_array();
        }
        $hak_id = '';
        if(isset($data_array))
            $hak_id             = $data_array[0]['FK_idhak'];
     
        //- create new users drivers licence and add it to user!
        if((empty($hak_id))||($hak_id == '')|| $hak_id == 'NULL')
        {
            $this -> db -> query (" INSERT INTO hak 
                                        (number, type, valid_thru)
                                    VALUES
                                        (?, ?, ?)"
                                    ,array($hak['number'], $hak['type'], $hak['valid_thru']));
            $hak_id   = $this->db->insert_id(); 
            $this -> db -> query (" UPDATE users SET FK_idhak=? WHERE idusers=?"
                                    , array($hak_id, $user_id));
        }
        //- else users driver licence exist, update data
        else
        {
            $this -> db -> query (" UPDATE hak SET
                                        number=?, type=?, valid_thru=? 
                                        WHERE idhak = ?"
                                     ,array($hak['number'], $hak['type'], $hak['valid_thru'], $hak_id));
        }
        $this->db->trans_complete();
        
        if ($this->db->trans_status() === FALSE)
        {   
            return FALSE;
        }
       
        return TRUE;
    }
    
    function go_reset_password($reset_password)
    {
        $user_id = $reset_password['id_user'];
        KO_check_array_values($reset_password);
        
        $query = $this->db->query("SELECT * FROM users WHERE idusers = ? AND password = ? AND confirmed = TRUE",  array($user_id, $reset_password['old_password']));
        $ret_val = 0;
        
        if($query -> num_rows() > 0)
        {
            $ret_val  = 1; 
            $this->db->trans_start();
            $query = $this->db->query("UPDATE users SET password = ? WHERE idusers = ?",  array($reset_password['new_password'], $user_id));
            $this->db->trans_complete();
            if ($this->db->trans_status() === FALSE)
                $ret_val  = -2; 
            
        }
        else
            $ret_val  = -1;
        
        return $ret_val;
    }
    
    function setup_check_array_values($driver_lic)
    {
        foreach($driver_lic as $key => $d)
        {
            if($d == 'accept')
            {
                $driver_lic[$key] = 1;
            }
        }
        return $driver_lic;
    }
}
/* End of file profile_model.php */
