<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Add_vehicles_model extends CI_Model
{
    function __construct() 
    {
        parent::__construct();
        
    }
    
     /**
    * Add vehicles to database
    *
    * @access	public
    * @param	array
    * @return	bool
    */
    public function add_vehicles($post_data)
    {
        KO_check_array_values($post_data);
        
        $this->db->trans_start();
        $this -> db -> query ("INSERT INTO vehicles (FK_idusers, name, licence_plate) 
                               VALUES (?, ?, ?)", 
                               array($post_data['user_id'], $post_data['name'], $post_data['licence_plate']) );
        
        $id_vehicles    = $this->db->insert_id(); 
        
        $date = date("Y-m-d");
        $this -> db -> query ("INSERT INTO log_km (FK_idusers, FK_idvehicles, km, date) 
                               VALUES (?, ?, ?, ?)", 
                               array($post_data['user_id'], $id_vehicles, $post_data['km'], $date) );
        
        $this -> db -> query ("INSERT INTO vehicles_licences (brand, type, model, manuf_year) 
                               VALUES (?, ?, ?, ?)", 
                               array($post_data['brand'], $post_data['type'], $post_data['model'], $post_data['year']) );
        
        $id_vehicles_licences    = $this->db->insert_id();
        
         $this -> db -> query ("UPDATE vehicles
                               SET FK_idvehicles_licences = ? 
                               WHERE idvehicles = ?", 
                               array($id_vehicles_licences, $id_vehicles) );
         
        $this->db->trans_complete();
        
        if ($this->db->trans_status() === FALSE)
        {   
            return FALSE;
        }

       
       
        return $id_vehicles;
    }
}


/* End of file add_vehicles_model.php */

        


