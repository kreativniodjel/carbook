<?php
class Vehicles_licences_model extends CI_Model 
{
    function __construct() 
    {
        parent::__construct();
    }
    
    function get_vehicles_data($vehicle_id)
    {
       $result = $this -> db -> query ("   SELECT  *
                                            FROM vehicles_licences l
                                            LEFT JOIN vehicles v ON l.idvehicles_licences = v.FK_idvehicles_licences
                                            WHERE v.idvehicles = ?
                                        ", array($vehicle_id));
        if($result -> num_rows() > 0)
        {
            $data = $result -> result_array();
            $result ->free_result();
            return $data;
        }
        return FALSE;
    }
    
    function get_vehicles_gallery($vehicle_id)
    {
       $result = $this -> db -> query ("   SELECT  *
                                            FROM pictures p
                                            WHERE p.FK_idvehicles = ?
                                        ", array($vehicle_id));
        if($result -> num_rows() > 0)
        {
            $data = $result -> result_array();
            $result ->free_result();
            return $data;
        }
        return array();
    }
    
    function check_if_vehicle_data_can_be_changed($idvehicles)
    {
        $user_id    = $this -> session -> userdata('user_id');
        $result = $this -> db -> query ("SELECT *
                                          FROM removed_vehicles
                                          WHERE FK_idusers = ?  AND FK_idvehicles = ? 
                                        ", array($user_id, $idvehicles));
        if($result -> num_rows() > 0)
        {
             return FALSE;
        }
        return TRUE;
    }
    
    function update_vehicles($data)
    {
        KO_check_array_values($data);
        
        $this -> db ->trans_start();
        $this -> db -> query (" UPDATE  vehicles 
                                SET     name = ?, licence_plate = ?
                                WHERE   idvehicles = ?
                                ", array($data['name'] , $data['licence_plate'], $data['$id_vehicle'] ));
        $this -> db -> trans_complete();
                
        if ($this -> db -> trans_status() === FALSE)
        {   
            return FALSE;
        }
        return TRUE;
    }
    
    function update_vehicles_licences($data)
    {
        KO_check_array_values($data);
        $result = $this -> db -> query (" SELECT  FK_idvehicles_licences
                                FROM    vehicles 
                                WHERE   idvehicles = ?
                                ", array($data['$id_vehicle']));
         
        if($result -> num_rows() > 0)
        {
            $value = $result -> result_array();
            $this -> db ->trans_start();
            $this -> db -> query (" UPDATE vehicles_licences 
                                    SET class = ?, brand = ?, type = ?, model = ?, color = ?, num_of_chassis = ?, body_shape = ?, manuf_country = ?,
                                        manuf_year = ?, date_first_reg = ?, seat_places = ?, carrying_capacity = ?, mass = ?, max_speed = ?, engine_type = ?,
                                        power = ?, cubic_capacity = ?, length = ?, width = ?, height = ?, lowest_point = ?, num_of_wheels = ?,
                                        tyres_front = ?, tyres_back = ?, brake_type = ?, num_of_axles = ?, driving_axles = ?, hook = ?, anchor = ?
                                    WHERE idvehicles_licences = ?
                                    ", array($data['class'] , $data['brand'], $data['type'] , $data['model'], $data['color'] , $data['num_of_chassis'], $data['body_shape'] , $data['manuf_country'], 
                                       $data['manuf_year'] , $data['date_first_reg'], $data['seat_places'] , $data['carrying_capacity'], $data['mass'] , $data['max_speed'], $data['engine_type'], 
                                       $data['power'] , $data['cubic_capacity'], $data['length'] , $data['width'], $data['height'] , $data['lowest_point'], $data['num_of_wheels'] ,    
                                       $data['tyres_front'] , $data['tyres_back'], $data['brake_type'] , $data['num_of_axles'], $data['driving_axles'] , $data['hook'], $data['anchor'], 
                                       $value[0]['FK_idvehicles_licences'] ));
            $this -> db -> trans_complete();

            if ($this -> db -> trans_status() === FALSE)
            {   
                return FALSE;
            }
        }
        return TRUE; 
    }
    
    function update_vehicles_gas($data)
    {
        KO_check_array_values($data);
        $result = $this -> db -> query (" SELECT  FK_idvehicles_licences
                                FROM    vehicles 
                                WHERE   idvehicles = ?
                                ", array($data['$id_vehicle']));
         
        if($result -> num_rows() > 0)
        {
            $value = $result -> result_array();
            $this -> db ->trans_start();
            $this -> db -> query (" UPDATE vehicles_licences 
                                    SET gasoline = ?, diesel = ?, gas = ?, electricity = ?
                                    WHERE idvehicles_licences = ?
                                    ", array($data['gasoline'] , $data['diesel'], $data['gas'] , $data['electricity'], 
                                       $value[0]['FK_idvehicles_licences'] ));
            $this -> db -> trans_complete();

            if ($this -> db -> trans_status() === FALSE)
            {   
                return FALSE;
            }
        }
        return TRUE; 
    }
    
    function update_vehicles_lights($data)
    {
        KO_check_array_values($data);
        $result = $this -> db -> query (" SELECT  FK_idvehicles_licences
                                FROM    vehicles 
                                WHERE   idvehicles = ?
                                ", array($data['$id_vehicle']));
         
        if($result -> num_rows() > 0)
        {
            $value = $result -> result_array();
            $this -> db ->trans_start();
            $this -> db -> query (" UPDATE vehicles_licences 
                                    SET short_lights = ?, high_lights = ?, stop_lights = ?
                                    WHERE idvehicles_licences = ?
                                    ", array($data['short_lights'] , $data['high_lights'], $data['stop_lights'], 
                                       $value[0]['FK_idvehicles_licences'] ));
            $this -> db -> trans_complete();

            if ($this -> db -> trans_status() === FALSE)
            {   
                return FALSE;
            }
        }
        return TRUE; 
    }
    
    
    function add_picture_to_gallery($data)
    {
        $this->db->trans_start();
        $this -> db -> query ("INSERT INTO pictures (FK_idvehicles, name, path, description) 
                               VALUES (?, ?, ?, ?)", 
                               array($data['id_vehicle'], $data['name'], $data['id_file_name'], $data['description'])); 
        $this -> db -> trans_complete();
        if ($this -> db -> trans_status() === FALSE)
        {   
            return FALSE;
        }
        return TRUE; 
    }
    
    function delete_pic_from_gallery ($idpictures)
    {
        $this->db->trans_start();
        $this -> db -> query ("DELETE FROM pictures WHERE idpictures = ?",
                               array($idpictures)); 
        $this -> db -> trans_complete();
        if ($this -> db -> trans_status() === FALSE)
        {   
            return FALSE;
        }
       
        return TRUE; 
    }

    function edit_pic_in_gallery ($data)
    {
        $this->db->trans_start();
        $this -> db -> query ("UPDATE pictures SET name = ?, description = ?
                               WHERE idpictures = ?", 
                               array($data['name'], $data['description'], $data['idpictures'])); 
        $this -> db -> trans_complete();
        if ($this -> db -> trans_status() === FALSE)
        {   
            return FALSE;
        }
        return TRUE;
    }

    function get_pic_thumbnail ($idpictures)
    {
       $result = $this -> db -> query ("   SELECT  *
                                            FROM pictures
                                            WHERE idpictures = ?
                                        ", array($idpictures));

        if($result -> num_rows() > 0)
        {
            $data = $result -> result_array();
            $result ->free_result(); 
            return $data[0]['path'];
        }

        return FALSE;
    }

    function set_pic_as_thumbnail ($picname, $idvehicles)
    {
        $this->db->trans_start();
        $this -> db -> query ("UPDATE vehicles SET thumbnail_name = ?
                               WHERE idvehicles = ?", 
                               array($picname, $idvehicles)); 
        $this -> db -> trans_complete();
        if ($this -> db -> trans_status() === FALSE)
        {   
            return FALSE;
        }
        
        return TRUE;
    }
    
}

/* End of file vehicles_licences.php */
