<?php

class Register_model extends CI_model
{
    function __construct() 
    {
        parent::__construct();
    }
    
    /**
    * Add user to database
    *
    * @access	public
    * @param	array
    * @return	bool
    */
    public function add_user($user_data)
    {
        // Checking username and email are over 3 chars lenght(just to be sure if form validation dont work properly)
        if(strlen($user_data['username']) < 3)
            return FALSE;
        else if(strlen($user_data['email']) < 5)
            return FALSE;
        
        KO_check_array_values($user_data);
        $code = create_random_string(6);
        
        $this -> db -> trans_start();
        $result = $this -> db -> query ("SELECT * FROM users WHERE username = ? OR email = ?", array($user_data['username'], $user_data['email']));
        if($result -> num_rows() > 0)
        {
            $this -> db -> trans_complete();
            return FALSE;
        }
            
        
        $result = $this -> db -> query ("INSERT INTO users (username, password, email, confirm_code) VALUES (?, ?, ?, ?)", 
                array($user_data['username'], $user_data['password'], $user_data['email'], md5($code)));
        
        $idusers    = $this -> db -> insert_id();
        
        $this -> db -> trans_complete();
        if ($this -> db -> trans_status() === FALSE)
        {
            return "error_db";
        }

        $this   -> load    ->   library('email');

        $config['protocol'] = 'mail';
        $this   ->  email   -> initialize($config);
        $this   ->  email   ->  from('admin@carbook.com', 'Carbook admin');
        $this   ->  email   ->  to($user_data['email']); 
        $this   ->  email   ->  subject('Carbook - confirmation mail');
        $message = "Klikni na link: ".base_url()."index.php/register/confirm_user/".$idusers."/".md5($code)." za potvrdu registracije!";
        $this   ->  email   ->  message($message);	
        $this   ->  email   ->  send();
        
        
        return $idusers;
    }
    
    function confirm_user($id, $confirm_code)
    {   
        $this -> db -> trans_start();
        $result = $this -> db -> query ("   UPDATE users 
                                            SET confirmed = TRUE
                                            WHERE idusers = ? AND confirm_code = ?
                                ", array($id, $confirm_code));
        $this -> db -> trans_complete();
        if ($this -> db -> trans_status() === FALSE)
        {
            return FALSE;
        }
        
        $query = $this -> db -> query ("   SELECT username 
                                            FROM users 
                                            WHERE idusers = ? AND confirm_code = ?
                                ", array($id, $confirm_code));
         if($query -> num_rows() > 0)
         {
            $query          = $query -> row_array();
            $db_user_id     = $id;
            $db_username    = $query['username'];
            
            if(! $this -> session -> userdata('user_id'))
            {
                $data = array(
                        'user_id'   => $db_user_id,   
                        'username'  => $db_username
                );
                $this -> session -> set_userdata($data);
            }
            redirect(base_url() . 'index.php/homepage'); 
            return TRUE;
         }
        return FALSE;
    }
}
