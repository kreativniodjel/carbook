<div class="common-modal modal-vehicle-sell" id="modal-vehicle-sell">

    <form>

    <input type="hidden" name="idveh_sell" />

    <header>
        <div class="required-fields-notice">* OBAVEZAN UNOS</div>
		<h2>PRODAJA VOZILA</h2>
    </header>

    <div class="common-table">

    <!-- the table -->
    <table class="table-wrapper">

        <tr>

            <td>

                <!-- first row -->
                <table class="table-content">

                    <colgroup>
                        <col class="col-brand">
                        <col class="col-type">
                        <col class="col-model">
                        <col class="col-year">
                        <col class="col-km">
                        <col class="col-fuel">
                    </colgroup>

                    <thead>
                        <tr>
			                <th>MARKA VOZILA</th>
			                <th>TIP VOZILA</th>
			                <th>MODEL VOZILA</th>
			                <th class="text-align-right">GODINA</th>
			                <th class="text-align-right">KILOMETRI</th>
			                <th>GORIVO</th>
                        </tr>
                    </thead>

                    <tbody>
                        <tr>
			                <td class="abs-pos-content">
			                    <input type="text" name="brand" value="{{brand}}" readonly />
			                </td>
			                <td class="abs-pos-content">
			                    <input type="text" name="type" value="{{type}}" readonly />
			                </td>
			                <td class="abs-pos-content">
			                    <input type="text" name="model" value="{{model}}" readonly />
			                </td>
			                <td class="abs-pos-content">
			                    <input type="text" name="year" value="{{year}}" readonly />
			                </td>
			                <td class="abs-pos-content">
			                    <input type="text" name="km" value="{{km}}" readonly />
			                </td>
			                <td class="abs-pos-content">
			                    <input type="text" name="fuel" value="{{fuel}}" readonly />
			                </td>
                        </tr>
                    </tbody>

                </table>
                <!-- /first row -->


               <!-- second row -->
               <table class="table-wrapper">

                    <tr>

                        <td>

                            <!-- the table-->
                            <table class="table-content">

                                <colgroup>
                                    <col class="col-location">
                                    <col class="col-price">
                                </colgroup>

                                <thead>
                                    <tr>
                                        <th>LOKACIJA *</th>
                                        <th>CIJENA (KN) *</th>
                                    </tr>
                                </thead>

                                <tbody>
                                    <tr>
						                <td class="abs-pos-content">
						                    <input type="text" name="location" value="{{location}}" required />
						                </td>
						                <td class="abs-pos-content">
						                    <input type="text" name="price" value="{{price}}" class="text-align-right" required />
						                </td>
                                    </tr>
                                </tbody>

                            </table>

                        </td>

                        <td class="table-spacer abs-pos-content col-buttons">
                            <div class="wrapper">
                                <button type="button" class="common-button" data-trigger="close">ODUSTANI</button>
                                <button type="submit" class="common-button">POTVRDI</button>
                            </div>
                        </td>

                    </tr>

                </table>
                <!-- /third row -->

            </td>

        </tr>

    </table>

    </div>

    </form>

</div>