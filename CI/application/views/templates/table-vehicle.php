<form method="post" accept-charset="utf-8">

    <div class="common-table table-vehicle" cb-toggle-container>

        <header>
            <div class="toggle" cb-toggle-trigger></div>
            <h2>VOZILO</h2>
        </header>
        
        <!-- the table -->
        <table class="table-wrapper" cb-toggle-content>

            <colgroup>
                <col class="col-1">
                <col class="col-2">
                <col class="col-3">
            </colgroup>

            <tr>

                <td class="abs-pos-content vehicle-image">
                    <img src="{{img_path}}{{db_thumbnail_name}}" />
                </td>

                <td>

                    <!-- the table-->
                    <table class="table-content">

                        <colgroup>
                            <col class="col-name">
                            <col class="col-number">
                        </colgroup>

                        <thead>
                            <tr>
                                <th>NAZIV</th>
                                <th>REG. OZNAKA</th>
                            </tr>
                        </thead>

                        <tbody>
                            <tr>
                                <td class="abs-pos-content">
                                    <input type="text" name="name" value="{{db_name}}" />
                                </td>
                                <td class="abs-pos-content">
                                    <input type="text" name="licence_plate" value="{{db_licence_plate}}" />
                                </td>
                            </tr>
                        </tbody>

                    </table>

                </td>

                <td class="table-spacer"></td>

            </tr>

        </table>

        {{#enable_changes}}
        <footer class="table-spacer">
            <input type="submit" name="submit_form_vehicles" value="PROMJENI" id="submit_form_vehicles" class="common-button">
        </footer>
        {{/enable_changes}}

    </div>

</form>