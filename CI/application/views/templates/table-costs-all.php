<div class="common-table table-costs-all" id="table-costs-all" cb-toggle-container>

    <header>
        <div class="toggle" cb-toggle-trigger></div>
        <h2>TROŠKOVI</h2>
        <form method='post' id='form_view_data'>
            <input type='hidden' name='show_year' value='{{show_year}}'>
            <button type='submit' class='toggle-year {{show_year_class}}'></button>
        </form>
    </header>

    <table class='table-content hover-rows table_class' border='1' cb-toggle-content>
        <colgroup>
            <col class="col-img">
            <col class="col-name">
            <col class="col-date">
            <col class="col-km">
            <col class="col-type">
            <col class="col-place">
            <col class="col-note">
            <col class="col-quantity">
            <col class="col-price">
            <col class="col-alarm">
        </colgroup>
        <thead>
            <tr>
                <th class="icon-photo"></th>
                <th>NAZIV</th>
                <th class='text-align-right'>DATUM</th>
                <th class='text-align-right'>KM</th>
                <th>VRSTA</th>
                <th>MJESTO TROŠKA</th>
                <th>NAPOMENA</th>
                <th class='text-align-right'>KOLIČINA</th>
                <th class='text-align-right'>CIJENA (KN)</th>
                <th>ALARM (KM)</th>
            </tr>
        </thead>
        <tbody>
            {{#costs}}

                {{#year_row}}
                <tr class='bg-stripes row-year'>
                    <td colspan='10'>{{row_month}} / {{row_year}}</td>
                </tr>
                {{/year_row}}

                {{^year_row}}
                <tr data-clickable-costs='{{id}}'
                    data-costs-details='{{idcosts_details}}'
                    data-name='{{place_of_cost}}'
                    data-type='{{type}}'
                    data-note='{{note}}'>
                    <td class='abs-pos-content vehicle-image'>
                        <img src='{{img_path}}{{thumb}}' />
                    </td>
                    <td>
                        {{name}}
                    </td>
                    <td class='text-align-right'>
                        {{date}}
                    </td>
                    <td class='text-align-right'>
                        {{km}}
                    </td>
                    <td class="text-padding-right">
                        {{#type}}
                        <div class='icon-dropdown' data-trigger='show-cost-floater'></div>
                        {{/type}}
                        {{type}}
                    </td>
                    <td>
                        {{place_of_cost}}
                    </td>
                    <td>
                        {{#note}}
                        <div class='icon-note' data-trigger='show-cost-note-floater'></div>
                        {{/note}}
                        {{note}}
                    </td>
                    <td class='text-align-right'>
                        {{quantity}}
                    </td>
                    <td class='text-align-right'>
                        {{price}}
                    </td>
                    <td>
                        <div class='icon-dropdown' data-trigger='show-alarm-floater'></div>
                        {{alarm_km}}
                    </td>
                </tr>
                {{/year_row}}

            {{/costs}}
        </tbody>
    </table>

</div>