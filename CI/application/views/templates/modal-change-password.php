<div class="common-modal modal-change-password" id="modal-change-password">

    <form>

	<header>
		<h2>PROMJENA ZAPORKE</h2>
	</header>

    <div class="common-table">

	    <table class="table-content">

	        <colgroup>
	            <col>
	            <col>
	            <col>
	        </colgroup>

	        <thead>
	            <tr>
	                <th>STARA ZAPORKA</th>
	                <th>NOVA ZAPORKA</th>
	                <th>PONOVI NOVU ZAPORKU</th>
	            </tr>
	        </thead>

	        <tbody>
	            <tr>
	                <td class="abs-pos-content">
	                    <input type="password" name="old_password" required />
	                </td>
	                <td class="abs-pos-content">
	                    <input type="password" name="new_password" required />
	                </td>
	                <td class="abs-pos-content">
	                    <input type="password" name="repeat_password" required />
	                </td>
	            </tr>

	        </tbody>

	    </table>

	</div>

    <footer class="bg-stripes">
        <button type="button" class="common-button" data-trigger="close">ODUSTANI</button>
        <button type="submit" class="common-button">POTVRDI</button>
    </footer>

    </form>

</div>	