<form method="post" accept-charset="utf-8">

  <div class="common-table table-user-personal-data" id="table-personal-data" cb-toggle-container>

    <!-- header -->
    <header>
      <div class="toggle" cb-toggle-trigger></div>
      <h2>OSOBNI PODACI</h2>
    </header>

    <div cb-toggle-content>

      <!-- table wrapper -->
      <table class="table-wrapper">

        <colgroup>
            <col class="col-1">
            <col class="col-2">
            <col class="col-3">
        </colgroup>

        <tr>
          <td rowspan="2" class="abs-pos-content user-image">
            <img src="{{user_img}}" />
          </td>
          <td>

            <!-- the table-->
            <table class="table-content">

              <colgroup>
                  <col class="col-username">
                  <col class="col-password">
              </colgroup>

              <thead>
                <tr>
                    <th>KORISNIČKO IME</th>
                    <th>ZAPORKA</th>
                  </tr>
                </thead>

                <tbody>
                <tr>
                    <td class="abs-pos-content">
                      <input type="text" name="username" value="{{db_username}}" readonly disabled />
                    </td>
                    <td class="abs-pos-content">
                      <input type="password" name="password" value="*******" readonly disabled />
                    </td>
                </tr>
              </tbody>

            </table>

          </td>
          <td class="table-spacer abs-pos-content user-password-button col-password-button">
            <div class="user-password-wrapper">
              <button type="button" name="submit_psw_profile" id="submit_psw_profile" class="common-button" cb-click="open change password modal">PROMJENI ZAPORKU</button>
            </div>
          </td>
        </tr>
        <tr>
          <td colspan="2">

            <!-- the table-->
            <table class="table-content">

              <colgroup>
                  <col class="col-name">
                  <col class="col-surname">
                  <col class="col-email">
              </colgroup>

              <thead>
                <tr>
                    <th>IME</th>
                    <th>PREZIME</th>
                    <th>E-MAIL ADRESA</th>
                  </tr>
                </thead>

                <tbody>
                <tr>
                    <td class="abs-pos-content">
                      <input type="text" name="name" value="{{db_name}}" />
                    </td>
                    <td class="abs-pos-content">
                      <input type="text" name="lastname" value="{{db_lastname}}" />
                    </td>
                    <td class="abs-pos-content">
                      <input type="text" name="email" value="{{db_email}}" />
                    </td>
                </tr>
              </tbody>

            </table>

          </td>
        </tr>
      </table>

      <!-- the table-->
      <table class="table-content">

        <colgroup>
            <col class="col-address-street">
            <col class="col-address-city">
            <col class="col-address-zip">
            <col class="col-address-country">
        </colgroup>

        <thead>
          <tr>
              <th>ULICA U KUĆNI BROJ</th>
              <th>GRAD</th>
              <th>P. BROJ</th>
              <th>DRŽAVA</th>
            </tr>
          </thead>

          <tbody>
          <tr>
              <td class="abs-pos-content">
                <input type="text" name="address" value="{{db_address}}" />
              </td>
              <td class="abs-pos-content">
                <input type="text" name="city" value="{{db_city}}" />
              </td>
              <td class="abs-pos-content">
                <input type="text" name="postal_code" value="{{db_postal_code}}" />
              </td>
              <td class="abs-pos-content dropdown">
                <div class="icon-dropdown"></div>
                <select name="country" class="customized">
                  {{#country_list}}
                  <option value="{{key}}" {{selected}}>{{value}}</option>
                  {{/country_list}}
                </select>
              </td>
          </tr>
        </tbody>

      </table>

      <!-- separator -->
      <div class="table-spacer separator"></div>

      <!-- the table-->
      <table class="table-content">

        <colgroup>
            <col class="col-phone">
            <col class="col-birthday-date">
            <col class="col-birthday-year">
            <col class="col-sex">
            <col>
        </colgroup>

        <thead>
          <tr>
              <th>BROJ TELEFONA</th>
              <th colspan="2">DATUM ROĐENJA</th>
              <th>SPOL</th>
            </tr>
          </thead>

          <tbody>
          <tr>
            <input type="hidden" name="birthday" value="{{db_birthday}}" />
            <td class="abs-pos-content">
                <input type="text" name="phone" value="{{db_phone}}" />
              </td>
              <td class="abs-pos-content dropdown">
                <div class="icon-dropdown"></div>
                <input type="text" class="datepicker-trigger datepicker-birthday-date" readonly />
              </td>
              <td class="abs-pos-content dropdown">
                <div class="icon-dropdown"></div>
                <input type="text" class="datepicker-trigger datepicker-birthday-year" readonly />
              </td>
              <td class="abs-pos-content dropdown">
                <div class="icon-dropdown"></div>
                <select name="gender" class="customized">
                  <option value="M" {{gender_male_selected}}>muški spol</option>
                  <option value="F" {{gender_female_selected}}>ženski spol</option>
                </select>
              </td>
          </tr>
        </tbody>

      </table>

      <footer class="table-spacer">
        <input type="submit" name="submit_form_profile" value="PROMJENI" id="submit_form_profile" class="common-button">
      </footer>

    </div>

  </div> <!-- cb-toggle-container -->

</form>