<nav class='navigation-wrapper'>

<?php if($register_user === TRUE) { ?>

    <div class="row-wrapper">
        <div class="row-content header-nav-cats">
            <a href="<?=URL_VEHICLES?>" class="header-button {{nav_current_vehicles_group}}"><?php echo lang('nav-vehicles')?></a>
            <a href="<?=URL_ADS?>" class="header-button {{nav_current_ads_group}}"><?php echo lang('nav-ads')?></a>
        </div>
    </div>

        <?php if($menu_selected_id === 1) { ?>

        <div class="row-wrapper">
            <div class="row-content header-nav-inventory">
                <form action="<?=site_url('costs')?>" method="post" accept-charset="utf-8" name="vehicles_drop_list_form" id="vehicles_drop_list_form">
                    <input type="hidden" name="hdn_id" value="234">
                    <select class="float-left dropdown-vozila customized" name="vehicles_drop_list" id="vehicles_drop_list">
                        {{#vehicles_list}}
                        <option value="{{key}}" {{selected}}>{{value}}</option>
                        {{/vehicles_list}}
                    </select>
                </form>

                <?php if(!empty($vehicles_alarms_grouped)) : ?>
                <div class="alarms">
                <?php endif; ?>

                    <?php foreach ($vehicles_alarms_grouped as $a) : ?>
                    <div class="alarm icon-alarm-type-<?php echo $a['idcosts_types']; ?>">
                        <span class='yellow-circle'>
                            <?php echo $a['count']; ?>
                        </span>
                    </div>
                    <?php endforeach; ?>

                <?php if(!empty($vehicles_alarms_grouped)) : ?>
                </div>
                <?php endif; ?>

                <a href="<?=URL_COSTS?>" class="header-button with-icon {{nav_current_costs}}"><div class="icon-money"></div><?php echo $vehicles_costs; ?> <?php echo lang('nav-currency')?></a>
                <!-- <a href="<?=URL_STATISTICS?>" class="header-button with-icon {{nav_current_statistics}}"><div class="icon-statistics"></div><?php echo lang('nav-statistics')?></a> -->
            </div>
        </div>

        <?php } else if($menu_selected_id === 2) { ?>

        <div class="row-wrapper">
            <div class="row-content header-nav-account">
                <a href="<?=URL_ADS?>" class="header-button {{nav_current_ads}}">MALI OGLASI</a>
                <a href="<?=URL_YELLOWPAGES?>" class="header-button {{nav_current_yellowpages}}">ŽUTE STRANICE</a>
            </div>
        </div>

        <?php } else if($menu_selected_id === 3) { ?>

        <div class="row-wrapper">
            <div class="row-content header-nav-account">
                <a href="<?=URL_PROFILE?>" class="header-button {{nav_current_profile}}"><?php echo lang('nav-profile')?></a>
                <!-- <a href="<?=URL_SETTINGS?>" class="header-button {{nav_current_settings}}"><?php echo lang('nav-settings')?></a> -->
                <a href="<?=URL_LOGOUT?>" class="header-button"><?php echo lang('nav-logout')?></a>
            </div>
        </div>

        <?php } ?>

    <?php } else { ?>

    <div class="row-wrapper">
        <div class="row-content header-nav-logreg">
            <a href="<?=URL_LOGIN?>" class="header-button" data-trigger="modal-login"><?php echo lang('nav-login')?></a>
            <a href="<?=URL_REGISTER?>" class="header-button" data-trigger="modal-register"><?php echo lang('nav-register')?></a>
        </div>
    </div>

<?php } ?>

</nav>