<div class="common-table table-vehicle-gallery" id="vehicle-gallery" cb-toggle-container>

    <header>
        <div class="toggle" cb-toggle-trigger></div>
        <h2>GALERIJA</h2>
    </header>
    
    <div class="content bg-stripes" cb-toggle-content>
        {{#pictures}}
        <dl>
            <dt>
                <img src="{{img_path}}{{path}}" data-id="{{idpictures}}" />
                <div class="options">
                    <div class="option option-delete" data-trigger="delete"></div>
                    <!--<div class="option option-edit" data-trigger="edit"></div>-->
                    <div class="option option-featured" data-trigger="featured"></div>
                </div>
            </dt>
            <dd>
                {{caption}}
            </dd>
        </dl>
        {{/pictures}}
        <dl class="upload">
            <dt><a href="#upload" data-trigger="upload"></a></dt>
            <dd></dd>
        </dl>
        <form enctype="multipart/form-data" action="vehicles_licences/add_picture_to_gallery" method="post">
            <input type="hidden" name="file_add_name"> 
            <input type="hidden" name="file_add_desc"> 
            <input type="file" class="hidden" name="file">
        </form>
    </div>

</div>
