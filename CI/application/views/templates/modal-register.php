<div class="common-modal modal-register" id="modal-register">
	<header>
		<h2><?php echo lang('modal-register-title'); ?></h2>
		<div class="close" data-trigger="close"></div>
	</header>
	<div class="content">
		<form action="" method="post">
			<input name="username" type="text" placeholder="<?php echo lang('modal-register-username'); ?>" />
			<input name="password" type="password" placeholder="<?php echo lang('modal-register-password'); ?>" />
			<input name="email" type="email" placeholder="<?php echo lang('modal-register-email'); ?>" />
			<button type="submit" class="common-button"><?php echo lang('modal-register-ok'); ?></button>
		</form>
	</div>
</div>