<form method="post" accept-charset="utf-8">

    <div class="common-table table-user-driver-license" cb-toggle-container>

        <header>
            <div class="toggle" cb-toggle-trigger></div>
            <h2>VOZAČKA DOZVOLA</h2>
        </header>

        <div cb-toggle-content>

            <table class="table-content">

                <thead>
                    <tr>
                        <th>A1</th>
                        <th>A2</th>
                        <th>A</th>
                        <th>B</th>
                        <th>B+E</th>
                        <th>C1</th>
                        <th>C1+E</th>
                        <th>C</th>
                        <th>C+E</th>
                        <th>D1</th>
                        <th>D1+E</th>
                        <th>D</th>
                        <th>D+E</th>
                        <th>F</th>
                        <th>G</th>
                        <th>H</th>
                        <th>AM</th>
                    </tr>
                </thead>

                <tbody>
                    <tr>
                        <td class="text-align-center"> <input type="checkbox" class="customized" value="accept" name="a1"  {{a1_checked}}   /> </td>
                        <td class="text-align-center"> <input type="checkbox" class="customized" value="accept" name="a2"  {{a2_checked}}   /> </td>
                        <td class="text-align-center"> <input type="checkbox" class="customized" value="accept" name="a"   {{a_checked}}    /> </td>
                        <td class="text-align-center"> <input type="checkbox" class="customized" value="accept" name="b"   {{b_checked}}    /> </td>
                        <td class="text-align-center"> <input type="checkbox" class="customized" value="accept" name="be"  {{be_checked}}   /> </td>
                        <td class="text-align-center"> <input type="checkbox" class="customized" value="accept" name="ci"  {{ci_checked}}   /> </td>
                        <td class="text-align-center"> <input type="checkbox" class="customized" value="accept" name="cie" {{cie_checked}}  /> </td>
                        <td class="text-align-center"> <input type="checkbox" class="customized" value="accept" name="c"   {{c_checked}}    /> </td>
                        <td class="text-align-center"> <input type="checkbox" class="customized" value="accept" name="ce"  {{ce_checked}}   /> </td>
                        <td class="text-align-center"> <input type="checkbox" class="customized" value="accept" name="d1"  {{d1_checked}}   /> </td>
                        <td class="text-align-center"> <input type="checkbox" class="customized" value="accept" name="d1e" {{d1e_checked}}  /> </td>
                        <td class="text-align-center"> <input type="checkbox" class="customized" value="accept" name="d"   {{d_checked}}    /> </td>
                        <td class="text-align-center"> <input type="checkbox" class="customized" value="accept" name="de"  {{de_checked}}   /> </td>
                        <td class="text-align-center"> <input type="checkbox" class="customized" value="accept" name="f"   {{f_checked}}    /> </td>
                        <td class="text-align-center"> <input type="checkbox" class="customized" value="accept" name="g"   {{g_checked}}    /> </td>
                        <td class="text-align-center"> <input type="checkbox" class="customized" value="accept" name="h"   {{h_checked}}    /> </td>
                        <td class="text-align-center"> <input type="checkbox" class="customized" value="accept" name="am"  {{am_checked}}   /> </td>
                    </tr>
                </tbody>

            </table>

            <footer class="table-spacer">
                <input type="submit" class="common-button" name="submit_form_driver_licence" value="PROMJENI" id="submit_form_driver_licence">
            </footer>

        </div> <!-- cb-toggle-content -->

    </div>

</form>