<div class="common-modal modal-login" id="modal-login">
	<header>
		<h2><?php echo lang('modal-login-title'); ?></h2>
		<div class="close" data-trigger="close"></div>
	</header>
	<div class="content">
		<form action="" method="post">
    		<input name="username" type="text" placeholder="<?php echo lang('modal-login-username'); ?>" />
    		<input name="password" type="password" placeholder="<?php echo lang('modal-login-password'); ?>" />
    		<!--
    		<a class="lost-password" href="#izgubljena-lozinka"><?php echo lang('modal-login-lost-password'); ?></a>
    		-->
    		<button type="submit" class="common-button"><?php echo lang('modal-login-ok'); ?></button>
		</form>
	</div>
</div>	