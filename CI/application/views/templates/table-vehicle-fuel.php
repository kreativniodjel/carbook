<form method="post" accept-charset="utf-8">

    <div class="common-table table-vehicle-fuel" cb-toggle-container>

        <header>
            <div class="toggle" cb-toggle-trigger></div>
            <h2>GORIVO</h2>
        </header>
        
        <!-- the table -->
        <table class="table-content" cb-toggle-content>

            <colgroup>
                <col>
                <col>
                <col>
                <col>
            </colgroup>

            <thead>
                <tr>
                    <th>BENZIN</th>
                    <th>DIZEL</th>
                    <th>PLIN</th>
                    <th>STRUJA</th>
                </tr>
            </thead>

            <tbody>
                <tr>
                    <td class='text-align-center'><input type="checkbox" value="accept" name="gasoline" {{db_gasoline_checked}} /></td>
                    <td class="text-align-center"><input type="checkbox" value="accept" name="diesel" {{db_diesel_checked}} /></td>
                    <td class="text-align-center"><input type="checkbox" value="accept" name="gas" {{db_gas_checked}} /></td>
                    <td class="text-align-center"><input type="checkbox" value="accept" name="electricity" {{db_electricity_checked}} /></td>
                </tr>
            </tbody>

        </table>

        {{#enable_changes}}
        <footer class="table-spacer">
            <input type="submit" name="submit_form_gas" value="PROMJENI" id="submit_form_gas" class="common-button">
        </footer>
        {{/enable_changes}}

    </div>

</form>