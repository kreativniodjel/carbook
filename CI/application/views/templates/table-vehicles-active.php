<div class="common-table table-vehicles" id="table-vehicles" cb-toggle-container>

    <header>
      <div class="toggle" cb-toggle-trigger></div>
      <h2>SADAŠNJA VOZILA</h2>
    </header>

    <div cb-toggle-content>

        <table id='vehicles_table' class='table-content hover-rows table_class' border='1'>
            <colgroup>
                <col class="col-img">
                <col class="col-title">
                <col class="col-year">
                <col class="col-plate">
                <col class="col-km">
                <col class="col-cost">
            </colgroup>
            {{#have_any_vehicle}}
            <thead>
                <tr>
                    <th class="icon-photo"></th>
                    <th>NAZIV</th>
                    <th class="text-align-right">GOD.</th>
                    <th>REG. OZNAKA</th>
                    <th class="text-align-right">KILOMETRI</th>
                    <th class="text-align-right">TROŠAK</th>
                </tr>
            </thead>
            {{/have_any_vehicle}}
            <tbody>
                {{#vehicles}}
                <tr data-clickable-vehicle="{{idvehicles}}"
                    data-name="{{name}}"
                    data-brand="{{brand}}"
                    data-model="{{model}}"
                    data-type="{{type}}"
                    data-gasoline="{{gasoline}}"  
                    data-diesel="{{diesel}}"    
                    data-gas="{{gas}}"      
                    data-electricity="{{electricity}}"
                    data-km="{{km}}"
                    data-year="{{manuf_year}}"
                    data-veh=""
                    class="clickable">
                    <td id='popup-on-1' class='abs-pos-content vehicle-image thumb_img'>
                        <img src="{{img_path}}{{thumbnail_name}}" data-trigger="show-vehicle-photo" />
                    </td>
                    <td id='popup-on-2' class='text-padding-right'>
                        <div class='icon-dropdown' data-trigger='show-vehicle-options'></div>
                        <a href="<?=site_url('vehicles_licences/set_vehicles')?>/{{idvehicles}}">{{name}}</a>
                        {{#has_any_alarm}}
                        <div class='alarms'>
                            {{#alarms}}
                            <div class='alarm icon-alarm-type-{{type}}'></div>
                            {{/alarms}}
                        </div>
                        {{/has_any_alarm}}
                    </td>
                    <td id='popup-on-3' class="text-align-right">
                        {{manuf_year}}
                    </td>
                    <td id='popup-off'>
                        <a href="<?=site_url('vehicles_licences/set_vehicles')?>/{{idvehicles}}">{{licence_plate}}</a>
                    </td>
                    <td id='popup-on-4' class="text-align-right">
                        {{km}}
                    </td>
                    <td id='popup-off' class="text-align-right">
                        <a href="<?=site_url('costs/set_vehicles')?>/{{idvehicles}}">{{costs}}</a>
                    </td>
                </tr>
                {{#has_any_alarm}}
                <tr class="bg-stripes">
                    <td colspan='6'></td>
                </tr>
                {{/has_any_alarm}}
                {{/vehicles}}
            </tbody>
        </table>

        <footer class="table-spacer">
            <button class="common-button">DODAJ VOZILO</button>
        </footer>

    </div>

</div>