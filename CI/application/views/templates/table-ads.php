<div class="common-table table-ads" id="table-ads" cb-toggle-container>

    <header>
      <div class="toggle" cb-toggle-trigger></div>
      <h2>OSOBNA VOZILA</h2>
    </header>

    <div cb-toggle-content>

        <table class='table-content hover-rows table_class' border='1'>

            <colgroup>
                <col>
                <col>
                <col>
                <col>
                <col>
                <col>
                <col>
                <col>
                <col>
            </colgroup>

            <thead>
                <tr>
                    <th class="icon-photo"></th>
                    <th>MARKA</th>
                    <th>TIP</th>
                    <th>MODEL</th>
                    <th class="text-align-right">GOD.</th>
                    <th class="text-align-right">KILOMETRI</th>
                    <th>GORIVO</th>
                    <th>LOKACIJA</th>
                    <th class="text-align-right">CIJENA</th>
                </tr>
            </thead>

            <tbody>
                {{#ads}}
                <tr>
                    <td class='abs-pos-content vehicle-image'>
                        <img src="{{img_path}}{{thumb}}" data-trigger="show-vehicle-photo" />
                    </td>
                    <td>
                        {{brand}}
                    </td>
                    <td>
                        {{type}}
                    </td>
                    <td>
                        {{model}}
                    </td>
                    <td class="text-align-right">
                        {{year}}
                    </td>
                    <td class="text-align-right">
                        {{km}}
                    </td>
                    <td>
                        {{fuel}}
                    </td>
                    <td>
                        {{location}}
                    </td>
                    <td class="text-align-right">
                        {{price}}
                    </td>
                </tr>
                {{/ads}}
            </tbody>
        </table>

    </div>

</div>