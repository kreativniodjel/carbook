<form method="post" accept-charset="utf-8">

    <div class="common-table table-vehicle-license" id="table-vehicle-license" cb-toggle-container>

        <header>
            <div class="toggle" cb-toggle-trigger></div>
            <h2>KNJIŽICA VOZILA</h2>
        </header>

        <div cb-toggle-content>

            <!-- the table-->
            <table class="table-content">

                <colgroup class="row-1">
                    <col>
                    <col>
                    <col>
                    <col>
                </colgroup>

                <thead>
                    <tr>
                        <th>VRSTA VOZILA</th>
                        <th>MARKA VOZILA</th>
                        <th>TIP VOZILA</th>
                        <th>MODEL VOZILA</th>
                    </tr>
                </thead>

                <tbody>
                    <tr>
                        <td class="abs-pos-content">
                            <input type="text" name="class" value="{{db_class}}">
                        </td>
                        <td class="abs-pos-content">
                            <input type="text" name="brand" value="{{db_brand}}">
                        </td>
                        <td class="abs-pos-content">
                            <input type="text" name="type" value="{{db_type}}">
                        </td>
                        <td class="abs-pos-content">
                            <input type="text" name="model" value="{{db_model}}">
                        </td>
                    </tr>
                </tbody>

            </table>

            <!-- the table-->
            <table class="table-content">

                <colgroup class="row-2">
                    <col>
                    <col>
                    <col>
                </colgroup>

                <thead>
                    <tr>
                        <th>BOJA</th>
                        <th>BROJ ŠASIJE</th>
                        <th>OBLIK KAROSERIJE</th>
                    </tr>
                </thead>

                <tbody>
                    <tr>
                        <td class="abs-pos-content">
                            <input type="text" name="color" value="{{db_color}}" />
                        </td>
                        <td class="abs-pos-content">
                            <input type="text" name="num_of_chassis" value="{{db_num_of_chassis}}" />
                        </td>
                        <td class="abs-pos-content">
                            <input type="text" name="body_shape" value="{{db_body_shape}}">
                        </td>
                    </tr>
                </tbody>

            </table>

            <!-- the table-->
            <table class="table-content">

                <colgroup class="row-3">
                    <col>
                    <col>
                    <col>
                    <col>
                    <col>
                </colgroup>

                <tr>
                    <th colspan="2">DRŽAVA I GODINA PROIZVODNJE</th>
                    <th colspan="2">DATUM PRVE REGISTRACIJE</th>
                    <th rowspan="2" class="table-spacer"></th>
                </tr>

                <tr>
                    <input type="text" class="hidden" name="date_first_reg" value="{{db_date_first_reg}}">
                    <td class="abs-pos-content">
                        <input type="text" name="manuf_country" value="{{db_manuf_country}}">
                    </td>
                    <td class="abs-pos-content">
                        <input type="text" name="manuf_year" value="{{db_manuf_year}}">
                    </td>
                    <td class="abs-pos-content dropdown">
                        <div class="icon-dropdown"></div>
                        <input type="text" class="datepicker-trigger datepicker-first-reg-date" readonly>
                    </td>
                    <td class="abs-pos-content dropdown">
                        <div class="icon-dropdown"></div>
                        <input type="text" class="datepicker-trigger datepicker-first-reg-year" readonly >
                    </td>
                </tr>

            </table>

            <!-- the table-->
            <table class="table-content">

                <colgroup class="row-4">
                    <col>
                    <col>
                    <col>
                    <col>
                </colgroup>

                <tr>
                    <th>MJESTA ZA SJEDENJE</th>
                    <th>DOPUŠTENA NOSIVOST</th>
                    <th>MASA PRAZNOG VOZILA</th>
                    <th rowspan="2" class="table-spacer"></th>
                </tr>

                <tr>
                    <td class="abs-pos-content">
                        <input type="text" name="seat_places" value="{{db_seat_places}}">
                    </td>
                    <td class="abs-pos-content">
                        <input type="text" name="carrying_capacity" value="{{db_carrying_capacity}}">
                    </td>
                    <td class="abs-pos-content">
                        <input type="text" name="mass" value="{{db_mass}}">
                    </td>
                </tr>

            </table>

            <!-- the table-->
            <table class="table-content">

                <colgroup class="row-5">
                    <col>
                    <col>
                    <col>
                    <col>
                </colgroup>

                <tr>
                    <th>MAKSIMALNA BRZINA (km/h)</th>
                    <th>VRSTA MOTORA</th>
                    <th>SNAGA (kw)</th>
                    <th>RADNI OBUJAM (cm<sup>3</sup>)</th>
                </tr>

                <tr>
                    <td class="abs-pos-content">
                        <input type="text" name="max_speed" value="{{db_max_speed}}">
                    </td>
                    <td class="abs-pos-content">
                        <input type="text" name="engine_type" value="{{db_engine_type}}">
                    </td>
                    <td class="abs-pos-content">
                        <input type="text" name="power" value="{{db_power}}">
                    </td>
                    <td class="abs-pos-content">
                        <input type="text" name="cubic_capacity" value="{{db_cubic_capacity}}">
                    </td>
                </tr>

            </table>

            <!-- the table-->
            <table class="table-content">

                <colgroup class="row-6">
                    <col>
                    <col>
                    <col>
                    <col>
                </colgroup>

                <tr>
                    <th>DUŽINA (cm)</th>
                    <th>ŠIRINA (cm)</th>
                    <th>VISINA (cm)</th>
                    <th>NAJNIŽA TOČKA</th>
                </tr>

                <tr>
                    <td class="abs-pos-content">
                        <input type="text" name="length" value="{{db_length}}">
                    </td>
                    <td class="abs-pos-content">
                        <input type="text" name="width" value="{{db_width}}">
                    </td>
                    <td class="abs-pos-content">
                        <input type="text" name="height" value="{{db_height}}">
                    </td>
                    <td class="abs-pos-content">
                        <input type="text" name="lowest_point" value="{{db_lowest_point}}">
                    </td>
                </tr>

            </table>

            <!-- the table-->
            <table class="table-content">

                <colgroup class="row-7">
                    <col>
                    <col>
                    <col>
                    <col>
                </colgroup>

                <tr>
                    <th>BROJ KOTAČA</th>
                    <th>PREDNJE GUME</th>
                    <th>STRAŽNJE GUME</th>
                    <th>VRSTA KOČNICA</th>
                </tr>

                <tr>
                    <td class="abs-pos-content">
                        <input type="text" name="num_of_wheels" value="{{db_num_of_wheels}}" />
                    </td>
                    <td class="abs-pos-content">
                        <input type="text" name="tyres_front" value="{{db_tyres_front}}" />
                    </td>
                    <td class="abs-pos-content">
                        <input type="text" name="tyres_back" value="{{db_tyres_back}}" />
                    </td>
                    <td class="abs-pos-content">
                        <input type="text" name="brake_type" value="{{db_brake_type}}" />
                    </td>
                </tr>

            </table>

            <!-- the table-->
            <table class="table-content">

                <colgroup class="row-8">
                    <col>
                    <col>
                    <col>
                </colgroup>

                <tr>
                    <th>BROJ OSOVINA</th>
                    <th>POGONSKIH OSOVINA</th>
                    <th rowspan="2" class="table-spacer"></th>
                </tr>

                <tr>
                    <td class="abs-pos-content">
                        <input type="text" name="num_of_axles" value="{{db_num_of_axles}}" />
                    </td>
                    <td class="abs-pos-content">
                        <input type="text" name="driving_axles" value="{{db_driving_axles}}" />
                    </td>
                </tr>

            </table>

            <!-- the table-->
            <table class="table-content">

                <colgroup class="row-9">
                    <col>
                    <col>
                    <col>
                </colgroup>

                <tr>
                    <th>KUKA</th>
                    <th>VITLO</th>
                    <th rowspan="2" class="table-spacer"></th>
                </tr>

                <tr>
                    <td class="text-align-center">
                        <input type="checkbox" value="accept" name="hook" {{db_hook_checked}} />
                    </td>
                    <td class="text-align-center">
                        <input type="checkbox" value="accept" name="anchor" {{db_anchor_checked}} />
                    </td>
                </tr>

            </table>

            {{#enable_changes}}
            <footer class="table-spacer">
                <input type="submit" name="submit_form_vehicles_licences" value="PROMJENI" id="submit_form_vehicles_licences" class="common-button">
            </footer>
            {{/enable_changes}}

        </div> <!-- cb-toggle-content -->

    </div> <!-- cb-toggle-container -->

</form>