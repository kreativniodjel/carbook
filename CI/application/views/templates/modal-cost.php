<div class="common-modal modal-cost" id="modal-cost">

    <form>

    <input type="text" class="hidden" name="idcost" value="" />
    <input type="text" class="hidden" name="iddetails" value="" />

    <header>
        <div class="required-fields-notice">* OBAVEZAN UNOS</div>
        <h2>UNOS NOVOG TROŠKA</h2>
    </header>

    <div class="common-table">

    <!-- the table -->
    <table class="table-wrapper">

        <tr>

            <td>

                <!-- first row -->
                <table class="table-content">

                    <colgroup>
                        <col class="col-cal-date">
                        <col class="col-cal-year">
                        <col class="col-km">
                        <col class="col-type">
                        <col class="col-location">
                    </colgroup>

                    <thead>
                        <tr>
                            <th colspan="2" class="text-align-right">DATUM *</th>
                            <th>KM *</th>
                            <th>VRSTA *</th>
                            <th>MJESTO TROŠKA</th>
                        </tr>
                    </thead>

                    <tbody>
                        <tr>
                            <input type="hidden" value="{{cost-date}}" name="date" />
                            <td class="abs-pos-content dropdown">
                                <div class="icon-dropdown"></div>
                                <input type="text" class="datepicker-trigger datepicker-cost-date" value="{{cost-day-month}}" readonly />
                            </td>
                            <td class="abs-pos-content dropdown">
                                <div class="icon-dropdown"></div>
                                <input type="text" class="datepicker-trigger datepicker-cost-year" value="{{cost-year}}" readonly />
                            </td>
                            <td class="abs-pos-content">
                                <input type="text" value="{{km}}" name="km" required />
                            </td>
                            <td class="abs-pos-content">
                                <select name="type_options" class="customized">
                                    {{#type}}
                                    <option value="{{key}}">{{value}}</option>
                                    {{/type}}
                                </select>
                            </td>
                            <td class="abs-pos-content">
                                <input type="text" value="{{place}}" name="place_of_cost" />
                            </td>
                        </tr>
                    </tbody>

                </table>
                <!-- /first row -->


                <!-- second row -->
                <table class="table-content">

                    <colgroup>
                        <col class="col-note">
                    </colgroup>

                    <thead>
                        <tr>
                            <th>NAPOMENA (PREOSTALO <span id="modal-cost-note-counter">150</span> ZANKOVA)</th>
                        </tr>
                    </thead>

                    <tbody>
                        <tr>
                            <td class="abs-pos-content">
                                <textarea id="modal-cost-note" name="note">{{note}}</textarea>
                            </td>
                        </tr>
                    </tbody>

                </table>
                <!-- /second row -->


               <!-- third row -->
               <table class="table-wrapper">

                    <tr>

                        <td>

                            <!-- the table-->
                            <table class="table-content">

                                <colgroup>
                                    <col class="col-quantity">
                                    <col class="col-price">
                                    <col class="col-alarm">
                                    <col class="col-alarm-cal-date">
                                    <col class="col-alarm-cal-year">
                                </colgroup>

                                <thead>
                                    <tr>
                                        <th>KOLIČINA *</th>
                                        <th>CIJENA (KN) *</th>
                                        <th>ALARM (KM)</th>
                                        <th colspan="2" class="text-align-right">
                                            <div class="icon-x" data-trigger="clear-alarm-date"></div>
                                            ALARM (DATUM)
                                        </th>
                                    </tr>
                                </thead>

                                <tbody>
                                    <tr>
                                        <input type="hidden" value="{{alarm-date}}" name="alarm_date" />
                                        <td class="abs-pos-content">
                                            <input type="text" value="{{quantity}}" name="quantity" />
                                        </td>
                                        <td class="abs-pos-content">
                                            <input type="text" value="{{price}}" name="price" required />
                                        </td>
                                        <td class="abs-pos-content">
                                            <input type="text" value="{{alarm-km}}" name="alarm_km" />
                                        </td>
                                        <td class="abs-pos-content dropdown">
                                            <div class="icon-dropdown"></div>
                                            <input type="text" class="datepicker-trigger datepicker-alarm-date" value="{{alarm-day-month}}" readonly />
                                        </td>
                                        <td class="abs-pos-content dropdown">
                                            <div class="icon-dropdown"></div>
                                            <input type="text" class="datepicker-trigger datepicker-alarm-year" value="{{alarm-year}}" readonly />
                                        </td>
                                    </tr>
                                </tbody>

                            </table>

                        </td>

                        <td class="table-spacer abs-pos-content button">
                            <div class="wrapper">
                                <button type="button" class="common-button" data-trigger="close">ODUSTANI</button>
                                <button type="submit" class="common-button">POTVRDI</button>
                            </div>
                        </td>

                    </tr>

                </table>
                <!-- /third row -->

            </td>

        </tr>

    </table>

    </div>

    </form>

</div>