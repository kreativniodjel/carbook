<!DOCTYPE HTML>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <title>CARB00K</title>
    <meta name="description">
    <meta name="viewport" content="width=device-width">
    <link href="../assets/stylesheets/screen-theme-default.css" rel='stylesheet' type="text/css">
    <link href="../assets/js/libs/jquery.ui/smoothness/jquery-ui-1.9.2.custom.min.css" rel='stylesheet' type="text/css">
    <link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type="text/css">
    <script src="../assets/js/libs/modernizr-2.6.2.min.js"></script>
    <script src="../assets/js/libs/jquery-1.8.2.min.js"></script>
    <script src="../assets/js/libs/jquery.ui/jquery-ui-1.9.2.custom.min.js"></script>
    <script src="../assets/js/libs/jquery.ezmark.min.js"></script>
    <script src="../assets/js/libs/jquery.customSelect.min.js"></script>
    <script src="../assets/js/scripts/sanjin/abs-pos-in-table-cell.js"></script>
    <script src="../assets/js/scripts/sanjin/get-vendor-prefix.js"></script>
    <script src="../assets/js/scripts/sanjin/init.js"></script>
    <body>

        <style>

        .sandbox {
            padding-left: 30px;
            padding-bottom: 30px;
        }

            .sandbox > div {
                margin-top: 60px;
                margin-bottom: 20px;
            }

                .sandbox > div > h2 {
                    font-size: 30px;
                    margin-bottom: 10px;
                    opacity: 0.12;
                }

                    .sandbox > div > h2 a {
                        text-decoration: none;
                    }

                .sandbox > div > * {
                    position: relative;
                }

        .index {
            padding: 30px;
            border-bottom: 1px solid #ccc;
            background-color: white;
        }

        div[name="table-vehicle"],
        div[name="table-vehicle-lights"],
        div[name="table-vehicle-license"],
        div[name="table-vehicle-fuel"],
        div[name="table-vehicle-gallery"],
        div[name="table-user-personal-data"],
        div[name="table-user-driver-license"],
        div[name="table-user-hak"],
        div[name="table-ads"],
        div[name="table-yp"],
        div[name="table-vehicles-active"],
        div[name="table-vehicles-ex"] {
            width: 715px;
        }

        div[name="modal-table-costs"],
        div[name="table-costs-all"],
        div[name="table-costs-vehicle"] {
            width: 960px;
        }

        </style>

        <script>
        $(document).ready(function(){
            
            $(".sandbox > *").each(function(){
                var name       = $(this).attr("name"),
                $anchor_index  = $("<a>").attr("href","#"+name).text(name).add($("<br>")),
                $anchor_target = $("<a>").attr("href","#"+name).attr("name",name).text(name),
                $title         = $("<h2>").append($anchor_target);

                $anchor_index.appendTo("#index");
                $(this).children().removeClass('hidden').before($title);
            });

            $('button, input[type=submit], input[type=button]').on('click', function(e){
                e.preventDefault();
            });

            $("#modal-costs-add .datepicker-cost").datepicker({
                firstDay: 1,
                dateFormat: "dd.mm.",
                altField: "#modal-costs-add .datepicker-cost-alt-year",
                altFormat: "yy"
            });

            $("#modal-costs-add .datepicker-alarm").datepicker({
                firstDay: 1,
                dateFormat: "dd.mm.",
                altField: "#modal-costs-add .datepicker-alarm-alt-year",
                altFormat: "yy"
            });

        });
        </script>

        <div class="index" id="index"></div>

        <div class="sandbox">