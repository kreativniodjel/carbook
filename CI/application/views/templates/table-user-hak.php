<form method="post" accept-charset="utf-8">

    <div class="common-table table-user-hak" id="table-user-hak" cb-toggle-container>

        <header>
            <div class="toggle" cb-toggle-trigger></div>
            <h2>HAK</h2>
        </header>
        
        <div cb-toggle-content>

            <!-- the table -->
            <table class="table-content">

                <colgroup>
                    <col class="col-number">
                    <col class="col-expire-date">
                    <col class="col-expire-year">
                    <col class="col-type">
                </colgroup>

                <thead>
                    <tr>
                        <th>ČLANSKI BROJ</th>
                        <th colspan="2">
                            <div class="icon-x" data-trigger="clear-date"></div>
                            VRIJEDI DO
                        </th>
                        <th>TIP ČLANSTVA</th>
                    </tr>
                </thead>

                <tbody>
                    <tr>
                        <input type="hidden" name="valid_thru" value="{{db_valid_thru}}" />
                        <td class="abs-pos-content">
                            <input type="text" name="number" value="{{db_number}}" />
                        </td>
                        <td class="abs-pos-content dropdown">
                            <div class="icon-dropdown"></div>
                            <input type="text" class="datepicker-trigger datepicker-hak-date" readonly />
                        </td>
                        <td class="abs-pos-content dropdown">
                            <div class="icon-dropdown"></div>
                            <input type="text" class="datepicker-trigger datepicker-hak-year" readonly />
                        </td>
                        <td class="abs-pos-content">
                            <input type="text" name="type" value="{{db_type}}" class= />
                        </td>
                    </tr>
                </tbody>

            </table>

            <footer class="table-spacer">
                <input type="submit" class="common-button" name="submit_form_hak" value="PROMJENI" id="submit_form_hak">
            </footer>

        </div> <!-- cb-toggle-content -->

    </div>

</form>