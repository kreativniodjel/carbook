<div class="common-floater floater-user hidden" id="user-modul">
	<img class="user-img" src="{{img}}" />
	<div class="title">{{full-name}}</div>
	<div class="user-options">
		<a href="<?=URL_PROFILE?>" class="user-option-profil">PROFIL</a>
		<!-- <a href="<?=URL_SETTINGS?>" class="user-option-postavke">POSTAVKE</a> -->
		<a href="<?=URL_LOGOUT?>" class="user-option-odjava">ODJAVI SE</a>
	</div>
</div>