<div class="common-table table-yp" cb-toggle-container>

    <header>
        <div class="toggle" cb-toggle-trigger></div>
        <h2>Autokuća</h2>
    </header>

    {{#yp}}

    <div cb-toggle-content>

        {{#banner_path}}
        <!-- the table -->
        <table class="table-wrapper yp-banner">
            <tr>
                <td>
                    <img src="{{img_path}}{{banner_path}}" />
                </td>
            </tr>
        </table>
        {{/banner_path}}

        {{^banner_path}}
        <!-- the table -->
        <table class="table-wrapper yp-ad">

            <colgroup>
                <col>
                <col>
            </colgroup>

            <tr>

                <td class="abs-pos-content yp-img">
                    <img src="{{img_path}}{{thumbnail_path}}" />
                </td>

                <td class="yp-ad">

                    <!-- the table-->
                    <table class="table-content">

                        <thead>
                            <tr>
                                <th>{{name}}</th>
                            </tr>
                        </thead>

                        <tbody>
                            <tr>
                                <td>{{text}}</td>
                            </tr>
                        </tbody>

                    </table>

                </td>

            </tr>

        </table>
        {{/banner_path}}

    </div> <!-- [cb-toggle-content] -->

    {{/yp}}

</div>
