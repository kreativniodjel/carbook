<form method="post" accept-charset="utf-8">

    <div class="common-table table-vehicle-lights" cb-toggle-container>

        <header>
            <div class="toggle" cb-toggle-trigger></div>
            <h2>ŽARULJE</h2>
        </header>
        
        <!-- the table -->
        <table class="table-content" cb-toggle-content>

            <colgroup>
                <col>
                <col>
                <col>
            </colgroup>

            <thead>
                <tr>
                    <th>KRATKA SVJETLA</th>
                    <th>DUGA SVJETLA</th>
                    <th>STOP SVJETLA</th>
                </tr>
            </thead>

            <tbody>
                <tr>
                    <td class="abs-pos-content">
                        <input type="text" name="short_lights" value="{{db_short_lights}}" />
                    </td>
                    <td class="abs-pos-content">
                        <input type="text" name="high_lights" value="{{db_high_lights}}" />
                    </td>
                    <td class="abs-pos-content">
                        <input type="text" name="stop_lights" value="{{db_stop_lights}}" />
                    </td>
                </tr>
            </tbody>

        </table>

        {{#enable_changes}}
        <footer class="table-spacer">
            <input type="submit" name="submit_form_lights" value="PROMJENI" id="submit_form_lights" class="common-button">
        </footer>
        {{/enable_changes}}

    </div>

</form>