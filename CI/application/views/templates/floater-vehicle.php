<div class="common-floater floater-vehicle hidden" id="vehicle-modul">
	<img class="vehicle-image" src="{{img}}" />
	<div class="title">{{title}}</div>
	<div class="vehicle-options">
		<a href="#prodaj" class="vehicle-option-prodaj" data-trigger="open-sell-vehicle-modal">PRODAJ</a>
		<a href="#obrisi" class="vehicle-option-obrisi">OBRIŠI</a>
		<a href="#premjesti-u-bivsa" class="vehicle-option-premjesti">PREMJESTI U BIVŠA</a>
	</div>
</div>