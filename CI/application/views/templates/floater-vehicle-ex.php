<div class="common-floater floater-vehicle hidden" id="ex-vehicle-modul">
	<img class="vehicle-image" src="{{img}}" />
	<div class="title">{{title}}</div>
	<div class="vehicle-options">
		<a href="#obrisi" class="vehicle-option-obrisi">OBRIŠI</a>
		<a href="#premjesti-u-sadasnja" class="vehicle-option-premjesti">PREMJESTI U SADAŠNJA</a>
	</div>
</div>