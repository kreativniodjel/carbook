<div class="common-table table-vehicles table-vehicles-ex" id="table-vehicles-ex" cb-toggle-container>

    <header>
      <div class="toggle" cb-toggle-trigger></div>
      <h2>BIVŠA VOZILA</h2>
    </header>

    <table id='vehicles_table' class='table-content hover-rows table_class' border='1' cb-toggle-content>
        <colgroup>
            <col class="col-img">
            <col class="col-title">
            <col class="col-year">
            <col class="col-plate">
            <col class="col-km">
            <col class="col-cost">
        </colgroup>
        <thead>
            <tr>
                <th class="icon-photo"></th>
                <th>NAZIV</th>
                <th class="text-align-right">GOD.</th>
                <th>REG. OZNAKA</th>
                <th class="text-align-right">KILOMETRI</th>
                <th class="text-align-right">TROŠAK</th>
            </tr>
        </thead>
        <tbody>
            {{#ex_vehicles}}
            <tr data-clickable-vehicle="{{idvehicles}}"
                data-name="{{name}}"
                data-brand="{{brand}}"
                data-model="{{model}}"
                data-model="{{type}}"
                data-gasoline="{{gasoline}}"  
                data-diesel="{{diesel}}"    
                data-gas="{{gas}}"      
                data-electricity="{{electricity}}"
                data-km="{{km}}"
                data-year="{{manuf_year}}"
                data-veh=""
                class="clickable">
                <td id='popup-on-1' class='abs-pos-content vehicle-image thumb_img'>
                    <img src="{{img_path}}{{thumbnail_name}}" data-trigger="show-vehicle-photo" />
                </td>
                <td id='popup-on-2' class='text-padding-right'>
                    <div class='icon-dropdown' data-trigger='show-vehicle-options'></div>
                    <a href="<?=site_url('vehicles_licences/set_vehicles')?>/{{idvehicles}}/TRUE">{{name}}</a>
                </td>
                <td id='popup-on-3' class="text-align-right">
                    {{manuf_year}}
                </td>
                <td id='popup-off'>
                    <a href="<?=site_url('vehicles_licences/set_vehicles')?>/{{idvehicles}}/TRUE">{{licence_plate}}</a>
                </td>
                <td id='popup-on-4' class="text-align-right">
                    {{km}}
                </td>
                <td id='popup-off' class="text-align-right">
                    <a href="<?=site_url('costs/set_vehicles')?>/{{idvehicles}}/TRUE">{{costs}}</a>
                </td>
            </tr>
            {{/ex_vehicles}}
        </tbody>
    </table>

</div>