<div class="common-modal modal-vehicle-add" id="modal-vehicle-add">

    <form>

	<header>
        <div class="required-fields-notice">* OBAVEZAN UNOS</div>
		<h2>UNOS NOVOG VOZILA</h2>
	</header>

    <div class="common-table">

	    <table class="table-content">

	    	<colgroup>
	    		<col>
	    		<col>
	    		<col>
	    		<col>
	    		<col>
	    		<col>
	    	</colgroup>

	        <thead>
	            <tr>
	                <th>MARKA VOZILA *</th>
	                <th>TIP VOZILA *</th>
	                <th>MODEL VOZILA *</th>
	                <th>GODINA *</th>
	                <th class="text-align-right">KILOMETRI *</th>
	                <th>REG. OZNAKA *</th>
	            </tr>
	        </thead>

	        <tbody>
	            <tr>
	                <td class="abs-pos-content">
	                    <input type="text" name="brand" value="{{brand}}" placeholder="Nissan" required />
	                </td>
	                <td class="abs-pos-content">
	                    <input type="text" name="type" value="{{type}}" placeholder="GTR" required />
	                </td>
	                <td class="abs-pos-content">
	                    <input type="text" name="model" value="{{model}}" placeholder="3.8 V6" required />
	                </td>
	                <td class="abs-pos-content">
	                    <input type="text" name="year" value="{{year}}" placeholder="2000" required />
	                </td>
	                <td class="abs-pos-content">
	                    <input type="text" name="km" value="{{km}}" class="text-align-right" placeholder="0" required />
	                </td>
	                <td class="abs-pos-content">
	                    <input type="text" name="licence_plate" value="{{plate}}" placeholder="AAXXXBB" required />
	                </td>
	            </tr>

	        </tbody>

	    </table>

	</div>

    <footer class="bg-stripes">
        <button type="button" class="common-button" data-trigger="close">ODUSTANI</button>
        <button type="submit" class="common-button">POTVRDI</button>
    </footer>

    </form>

</div>	