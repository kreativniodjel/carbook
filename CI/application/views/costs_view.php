<?php 
    if($costs != FALSE)
    {
        
        echo "<table id='costs_table' class='table_class'>";
        echo "<thead>";
        //- button for view switch
        if($show_year == 0)
            $cls = 'btn_show_month_on';
        else
            $cls = 'btn_show_month_off';
        
        if($view_type === 0)
            echo    "<tr class='no_border'><td  colspan='9'></td><td><form action='costs' method='post' id='form_view_data'><input type='text' name='show_year' value='".$show_year."' style='display:none;'><button class='".$cls."'></button></form></td></tr>";
        else
            echo    "<tr class='no_border'><td  colspan='7'></td><td><form action='costs' method='post' id='form_view_data'><input type='text' name='show_year' value='".$show_year."' style='display:none;'><button class='".$cls."'></button></form></td></tr>";
        echo     "<tr>";
        if($view_type === 0)
            echo "<th></th><th>NAZIV</th><th>DATUM</th><th>KM</th><th>VRSTA</th><th>MJESTO TROŠAK</th><th>NAPOMENA</th><th>KOLIČINA</th><th>CIJENA(KN)</th><th>ALARM</th>";
        else 
            echo "<th>DATUM</th><th>KM</th><th>VRSTA</th><th>MJESTO TROŠAK</th><th>NAPOMENA</th><th>KOLIČINA</th><th>CIJENA(KN)</th><th>ALARM</th>";
        echo     "</tr>";
        echo "</thead>";
        echo "<tbody>";
        $curr_month = -1;
        $curr_year  = -1;
        foreach($costs as $cost)
        {
            if($show_year == 0)
            {
                if(($cost['month'] != $curr_month)||($curr_year != $cost['year']))
                {
                    $curr_month = $cost['month'];
                    $curr_year  = $cost['year'];
                    if($view_type === 0)
                        echo "<tr><td colspan='10'>".$cost['month']." / ".$cost['year']."</td></tr>";
                    else
                        echo "<tr><td colspan='8'>".$cost['month']." / ".$cost['year']."</td></tr>";
                }
            }
            echo "<tr data-clickable-costs='".$cost['id']."' data-costs-details='".$cost['idcosts_details']."' data-name='".$cost['place_of_cost']."' class='clickable'>";
            if($view_type === 0)
            {
                echo "<td id='popup-on-1' class='thumb_img'><img src='".base_url()."/gallery/".$cost['thumb']."'></img></td>";
                echo "<td id='popup-on-2'>".$cost['name']."</td>";
                echo "<td id='popup-on-3'>".$cost['date']."</td>";
                echo "<td id='popup-on-4'>".$cost['km']."</td>";
                echo "<td id='popup-on-5'>".$cost['type']."</td>";
                echo "<td id='popup-on-6'>".$cost['place_of_cost']."</td>";
                echo "<td id='popup-on-7'>".$cost['note']."</td>";
                echo "<td id='popup-on-8'>".$cost['quantity']."</td>";
                echo "<td id='popup-on-9'>".$cost['price']."</td>";
                echo "<td id='popup-on-10'>".$cost['alarm_km']."</td>";
            }
            else
            {
                echo "<td id='popup-on-1'>".$cost['date']."</td>";
                echo "<td id='popup-on-2'>".$cost['km']."</td>";
                echo "<td id='popup-on-3'>".$cost['type']."</td>";
                echo "<td id='popup-on-4'>".$cost['place_of_cost']."</td>";
                echo "<td id='popup-on-5'>".$cost['note']."</td>";
                echo "<td id='popup-on-6'>".$cost['quantity']."</td>";
                echo "<td id='popup-on-7'>".$cost['price']."</td>";
                echo "<td id='popup-on-8'>".$cost['alarm_km']."</td>";
            }
            echo "</tr>";
        }
        echo "</tbody></table>";
    }
    else
    {
        echo "<tr>";
        echo "<td>".$error."</td>";
        echo "</tr>";
    }
   
    if($view_type == 1)
    {
        echo "<br/>";
        if($enable_changes === TRUE)//ONLY if changes are enabled show button add costs
        {
?>
            <button id="add_cost" name="add_cost" type="button" onclick="location.href='<?php echo base_url()?>index.php/add_costs'">Dodaj trošak</button>
<?php
            echo "<br/><br/>";
        }
    }
?>

<!-- Popup dialog  -->
<?php
if($enable_changes === TRUE) //ONLY if changes are enabled show dialog edit/delete costs
{
?>
    <div id="dialog">
        <label id="nameLabel" name="nameLabel"></label></br>
        <label id="idcost" name="idcost" style="display:none"></label>
        <label id="iddetails" name="iddetails" style="display:none"></label>
        <hr/>
        <input id="1" type="button" OnClick="" value="Uredi" class="dialog-button"/><br/><br/>
        <input id="2" type="button" OnClick="" value="Obriši" class="dialog-button"/><br/><br/>
    </div>
<?php
}
?>
<!-- End of vehicles_view.php -->
