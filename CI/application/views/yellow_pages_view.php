<div class='yp_content'>
<?php 
    echo "<div class='yp_header'><h1>Autokuća<h1></div>";
    foreach($yp as $y)
    {
        if(!empty($y['banner_path']))
        {
            echo "<div class='yp_all'>";
                 echo "<a href='http://".$y['website']."'><img src='".base_url()."/ypages/".$y['banner_path']."'></img></a>";
            echo "</div>";
        }
        else
        {
            echo "<div class='yp_part'>";
                echo "<div class='yp_left thumb_img'><img src='".base_url()."/ypages/".$y['thumbnail_path']."'></img></div>";
                echo "<div class='yp_right'>";
                    echo "<div class='yp_name'> <a href='http://".$y['website']."'>".$y['name'].", ".$y['address'].", ".$y['city']." * ".$y['phone']." * ".$y['email']."</a></div>";
                    echo "<div class='yp_text'>".$y['text']."</div>";
                echo "</div>";
            echo "</div>";
        }
    }

    echo "<div class='yp_header'><h1>Servisi<h1></div>";
    foreach($service as $s)
    {
        if(!empty($s['banner_path']))
        {
            echo "<div class='yp_all'>";
                 echo "<a href='http://".$s['website']."'><img src='".base_url()."/ypages/".$s['banner_path']."'></img></a>";
            echo "</div>";
        }
        else
        {
            echo "<div class='yp_part'>";
                echo "<div class='yp_left thumb_img'><img src='".base_url()."/ypages/".$s['thumbnail_path']."'></img></div>";
                echo "<div class='yp_right'>";
                    echo "<div class='yp_name'> <a href='http://".$s['website']."'>".$s['name'].", ".$s['address'].", ".$s['city']." * ".$s['phone']." * ".$s['email']."</a></div>";
                    echo "<div class='yp_text'>".$s['text']."</div>";
                echo "</div>";
            echo "</div>";
        }
    }
?>
</div>        