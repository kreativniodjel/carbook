<!-- Add vehicles form -->
<div id="add_vehicles_form" style="width:400px; margin:100px auto; border-radius: 5px;border:1px solid #909090; padding:20px">
    <h1>Podaci o vozilu:</h1><br/>
<?php
    echo form_open();
    ////////////////////////////////////////////////////////////////
    //- Name label and input field
     echo form_error('name');
        $data = array(
            'style' => 'display:inline-block; width:120px;'
        ); 
        echo form_label($name_text, 'name_text', $data);
        
        $data = array(
            'name'  => 'name',
            'id'    => 'name',
            'value' => set_value('name'),
            'style' => 'width:60%;'
        );
        echo form_input($data).br(2);
    ////////////////////////////////////////////////////////////////
    //- Year label and input field
     echo form_error('year');
        $data = array(
            'style' => 'display:inline-block; width:120px;'
        ); 
        echo form_label($year_text, 'year_text', $data);
        
        $data = array(
            'name'  => 'year',
            'id'    => 'year',
            'value' => set_value('year'),
            'style' => 'width:60%;'
        );
        echo form_input($data).br(2);
    ////////////////////////////////////////////////////////////////
    //- Licence plate label and input field
     echo form_error('licence_plate');
        $data = array(
            'style' => 'display:inline-block; width:120px;'
        ); 
        echo form_label($licence_plate_text, 'licence_plate_text', $data);
        
        $data = array(
            'name'  => 'licence_plate',
            'id'    => 'licence_plate',
            'value' => set_value('licence_plate'),
            'style' => 'width:60%;'
        );
        echo form_input($data).br(2);
    ////////////////////////////////////////////////////////////////
    //- Km label and input field
     echo form_error('km');
        $data = array(
            'style' => 'display:inline-block; width:120px;'
        ); 
        echo form_label($km_text, 'km_text', $data);
        
        $data = array(
            'name'  => 'km',
            'id'    => 'km',
            'value' => set_value('km'),
            'style' => 'width:60%;'
        );
        echo form_input($data).br(2);
    ////////////////////////////////////////////////////////////////
    //- Add vehicles form submit button
    $data = array(
        'name'  =>'add_button',
        'id'    =>'add_button',
        'value' => $add_button,
        'style' =>'width:40%'
        );
    echo form_submit($data)."&nbsp;&nbsp;&nbsp;";
    // CANCEL BUTTON
    $data = array(
        'name'  =>'cancel_button',
        'id'    =>'cancel_button',
        'value' => $cancel_button,
        'style' =>'width:40%'
        );
    echo form_submit($data).br(2);
    ////////////////////////////////////////////////////////////////    
    echo form_close();
    
?>
</div>