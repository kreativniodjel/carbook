<?php if (!defined('BASEPATH'))exit('No direct script access allowed');?>
<?php echo doctype("html5");?>
<html>
    <head>
        <meta charset="UTF-8" />
        <link href="<?=base_url('/assets/stylesheets/screen-theme-default.css')?>" media="screen, projection" rel="stylesheet" type="text/css" />
        <title><?php echo $title; ?></title>
        <script>CARBOOK = { site_url : '<?=site_url()?>' }</script>
        <link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css'>
    </head>
    <body>


        <!-- Page header -->

        <div class="page-header">

            <div class="top-wrapper">
                <div class="top-content">
                    <?php 
                        $username = $this -> session -> userdata('username');
                        if(strlen($username) > 1) :
                    ?>
                    <a href="#account" id="user-toggler"><?php echo $username; ?></a>
                    <?php endif; ?>
                </div>
            </div>
        
            <div class="header-wrapper">
                <div class="header-content">
                    <a class="logo" href="<?=URL_BASE?>">CARB00K</a>
                    <div class="banner">
                        <img src="<?=base_url('/assets/img/banners/leaderboard'.rand(1,3).'.jpg')?>">
                    </div>
                </div>
            </div>

            <?php 
                $menu = new Navigation_menu;
                if(isset($show_ex_vehicles))
                    $menu->create_menu(TRUE);
                else 
                    $menu->create_menu(FALSE);
            ?>   
            
        </div> <!-- /.page-header -->


        <!-- Page content -->

        <table class="page-content-wrapper">
        <tr>

            <td class="page-content">

     
<!-- End of header_view.php -->