<?php 
    if(!isset($error))
    {
        echo "<h3>SADAŠNJA VOZILA</h3><hr/>".br(1);
        echo "<table id='vehicles_table' class='table_class' border='1'>";
        echo "<thead>";
        echo     "<tr>";
        echo         "<th></th><th>NAZIV</th><th>GOD.</th><th>REG. OZNAKA</th><th>KILOMETRI</th><th>TROŠAK</th>";
        echo     "</tr>";
        echo "</thead>";
        echo "<tbody>";
        foreach($vehicles as $vehicle)
        {
            echo "<tr data-clickable-vehicle='".$vehicle['idvehicles']."' data-name='".$vehicle['name']."' ".
                    "data-brand='".$vehicle['brand']."' ".         
                    "data-model='".$vehicle['model']."' ".         
                    "data-gasoline='".$vehicle['gasoline']."' ".     
                    "data-diesel='".$vehicle['diesel']."' ".       
                    "data-gas='".$vehicle['gas']."' ".           
                    "data-electricity='".$vehicle['electricity']."' ".
                    "data-km='".$vehicle['km']."' ".
                    "data-year='".$vehicle['manuf_year']."' ".
                    "data-veh=''". 
                    "class='clickable'>";
            echo "<td id='popup-on-1' class='thumb_img'><img src='".base_url()."/gallery/".$vehicle['thumbnail_name']."'></img></td>";
            echo "<td id='popup-on-2'>".$vehicle['name']."</td>";
            echo "<td id='popup-on-3'>".$vehicle['manuf_year']."</td>";
            echo "<td id='popup-off'>"."<a href='".base_url()."index.php/vehicles_licences/set_vehicles/".$vehicle['idvehicles']."'>".$vehicle['licence_plate']."</a>"."</td>";
            echo "<td id='popup-on-4'>".$vehicle['km']."</td>";
            echo "<td id='popup-off'>"."<a href='".base_url()."index.php/costs/set_vehicles/".$vehicle['idvehicles']."'>".$vehicle['costs']."</a>"."</td>";
            echo "</tr>";
             //- if there is alarm for vehicle show it in row!
            $first_alarm = TRUE;
            foreach($vehicles_alarms as $a)
            {
                if($a['idvehicles'] == $vehicle['idvehicles'])
                {
                    if($first_alarm === TRUE)
                    {
                        echo "<tr><td></td><td id='popup-off' colspan='5'>";
                        $first_alarm = FALSE;
                    } 
                     
                    echo "<img src='".$a['thumb_sm_alarm']."'></img>";
                   
                }       
            }
            //it means that there is at least one alarm for vehicle so we need to close table row!
            if($first_alarm === FALSE) 
                echo "</td></tr>";
        }
        echo "</tbody></table>";
    }
?>    
<br/>
<button type="button" onclick="location.href='<?php echo base_url()?>index.php/add_vehicles'">Dodaj vozilo</button>
<br/><br/>

<?php
    if($exist_ex_vehicles === TRUE)
    {
        echo "<h3>BIVŠA VOZILA</h3><hr/>".br(1);
        echo "<table id='vehicles_table' class='table_class'>";
        echo "<thead>";
        echo     "<tr>";
        echo         "<th></th><th>NAZIV</th><th>GOD.</th><th>REG. OZNAKA</th><th>KILOMETRI</th><th>TROŠAK</th>";
        echo     "</tr>";
        echo "</thead>";
        echo "<tbody>";
        foreach($ex_vehicles as $vehicle)
        {
            echo "<tr data-clickable-vehicle='".$vehicle['idvehicles']."' data-name='".$vehicle['name']."' ".
                    "data-brand='".$vehicle['brand']."' ".         
                    "data-model='".$vehicle['model']."' ".         
                    "data-gasoline='".$vehicle['gasoline']."' ".     
                    "data-diesel='".$vehicle['diesel']."' ".       
                    "data-gas='".$vehicle['gas']."' ".           
                    "data-electricity='".$vehicle['electricity']."' ".
                    "data-km='".$vehicle['km']."' ".
                    "data-year='".$vehicle['manuf_year']."' ".
                    "data-veh='ex'". 
                    "class='clickable'>";
            echo "<td id='popup-on-1' class='thumb_img'><img src='".base_url()."/gallery/".$vehicle['thumbnail_name']."'></img></td>";
            echo "<td id='popup-on-2'>".$vehicle['name']."</td>";
            echo "<td id='popup-on-3'>".$vehicle['manuf_year']."</td>";
            echo "<td id='popup-off'>"."<a href='".base_url()."index.php/vehicles_licences/set_vehicles/".$vehicle['idvehicles']."/TRUE'>".$vehicle['licence_plate']."</a>"."</td>";
            echo "<td id='popup-on-4'>".$vehicle['km']."</td>";
            echo "<td id='popup-off'>"."<a href='".base_url()."index.php/costs/set_vehicles/".$vehicle['idvehicles']."/TRUE'>".$vehicle['costs']."</a>"."</td>";
            echo "</tr>";
        }
        echo "</tbody></table>";
    }
?>
                    
<br/><br/>

       

<!-- Popup dialog  -->
<div id="dialog">
    <label id="nameLabel" name="nameLabel"></label></br>
    <label id="idveh" name="idveh" style="display:none"></label>
    <label id="deltype" name="deltype" style="display:none"></label>
    <hr/>
    <input id="1" type="button" OnClick="" value="Prodaj" class="dialog-button"/><br/><br/>
    <input id="2" type="button" OnClick="" value="Obriši" class="dialog-button"/><br/><br/>
    <input id="3" type="button" OnClick="" value="Premjesti u bivša" class="dialog-button"/><br/><br/> 
</div>

<!-- Popup dialog sell -->
<div id="dialog-sell" title="Prodaja vozila">
    <label id="nameLabel_sell" name="nameLabel_sell"></label></br>
    <hr/>
    <label id="brandLabel"  name="brandLabel" class="label-dialog">MARKA</label>    <input id="brand" disabled="disabled"  type="text" class="input-dialog"></input></br>
    <label id="modelLabel"  name="modelLabel" class="label-dialog">MODEL</label>    <input id="model" disabled="disabled"  type="text" class="input-dialog"></input></br>
    <label id="yearLabel"   name="yearLabel"  class="label-dialog">GOD.</label>     <input id="year"  disabled="disabled"  type="text" class="input-dialog"></input></br>
    <label id="kmLabel"     name="kmLabel"    class="label-dialog">KM</label>       <input id="km"    disabled="disabled"  type="text" class="input-dialog"></input></br>
    <label id="fuelLabel"   name="fuelLabel"  class="label-dialog">GORIVO</label>   <input id="fuel"  disabled="disabled"  type="text" class="input-dialog"></input></br>
    <label id="priceLabel"  name="priceLabel" class="label-dialog">CIJENA(KN)</label>   <input id="price" type="text" class="input-dialog"></input></br>
    <label id="idveh_sell"  name="idveh_sell" style="display:none"></label>
    <hr/>
    <input id="2" type="button" OnClick="" value="POTVRDI" class="dialog-sell-button"/>
    <input id="1" type="button" OnClick="" value="ODUSTANI" class="dialog-sell-button"/><br/><br/>
</div>

<!-- End of vehicles_view.php -->