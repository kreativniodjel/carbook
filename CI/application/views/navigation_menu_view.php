
<nav class='navigation-wrapper'>

<?php if($register_user === TRUE) { ?>

    <div class="row-wrapper">
        <div class="row-content header-nav-cats">
            <a href="<?=URL_VEHICLES?>" class="header-button"><?php echo lang('nav-vehicles')?></a>
            <a href="<?=URL_ADS?>" class="header-button"><?php echo lang('nav-ads')?></a>
        </div>
    </div>

        <?php if($menu_selected_id === 1) { ?>

        <div class="row-wrapper">
            <div class="row-content header-nav-inventory">
                <select class="float-left dropdown-vozila">
                    <option disabled>Sva vozila</option>
                    <option>Vozilo #1</option>
                    <option>Vozilo #2</option>
                    <option>Vozilo #3</option>
                    <option>Vozilo #4</option>
                    <option>Vozilo #5</option>
                    <option>Vozilo #6</option>
                    <option>Vozilo #7</option>
                    <option>Vozilo #8</option>
                    <option>Vozilo #9</option>
                    <option>Vozilo #10</option>
                </select>
                <div class="alarms">
                    <a href="#akumulator" class="alarm-akumulator"><span class='yellow-circle'>13</span></a>
                    <a href="#kocnice" class="alarm-kocnice"><span class='blue-circle'>5</span></a>
                    <a href="#popis" class="alarm-notes"><span class='yellow-circle'>7</span></a>
                </div>
                <a href="<?=URL_COSTS?>" class="header-button with-icon"><div class="icon-money"></div>5.347,42 <?php echo lang('nav-currency')?></a>
                <a href="<?=URL_STATISTICS?>" class="header-button with-icon"><div class="icon-statistics"></div><?php echo lang('nav-statistics')?></a>
            </div>
        </div>

        <?php } else if($menu_selected_id === 2) { ?>

        <?php
            /*
                echo br(2);
                echo '&nbsp;&nbsp;<li>'.anchor('ads',               'Mali oglasi',          'class="link-class"').'</li>'; 
                echo '&nbsp;&nbsp;<li>'.anchor('yellow_pages',      'Yellow pages',         'class="link-class"').'</li>'; 
            */
        ?>

        <?php } else if($menu_selected_id === 3) { ?>

        <div class="row-wrapper">
            <div class="row-content header-nav-account">
                </select>
                <a href="<?=URL_PROFILE?>" class="header-button"><?php echo lang('nav-profile')?></a>
                <a href="<?=URL_SETTINGS?>" class="header-button"><?php echo lang('nav-settings')?></a>
                <a href="<?=URL_LOGOUT?>" class="header-button"><?php echo lang('nav-logout')?></a>
            </div>
        </div>

        <?php } ?>

    <?php } else { ?>

    <div class="row-wrapper">
        <div class="row-content header-nav-logreg">
            <a href="<?=URL_LOGIN?>" class="header-button" data-trigger="modal-login"><?php echo lang('nav-login')?></a>
            <a href="<?=URL_REGISTER?>" class="header-button" data-trigger="modal-register"><?php echo lang('nav-register')?></a>
        </div>
    </div>

<?php } ?>

</nav>
