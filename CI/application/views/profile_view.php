<!-- Profile -->
<div id="profile_form" style="width:700px; margin:100px auto; border-radius: 5px;border:1px solid #909090; padding:20px">
<?php 
    if(isset($error))
    {
        echo $error;
    }
    else
    {
?>  
    <!-- Profile form -->
    <h1>OSOBNI PODACI</h1>
<?php
    ////////////////////////////////////////////////////////////////////////////////
    // Username & password are excluded from form!
    ////////////////////////////////////////////////////////////////////////////////
    $data = array(
            'style' => 'display:inline-block; width:30%;'
        ); 
    echo form_label($username_text, 'username_text', $data)."&nbsp;&nbsp;";
    $data = array(
            'style' => 'display:inline-block; width:30%;'
        ); 
    echo form_label($password_text, 'password_text', $data).br(2);
    echo form_error('username');
    echo form_error('password');
    $data = array(
        'name'  => 'username',
        'id'    => 'username',
        'value' => set_value('username', $db_username),
        'style' => 'display:inline-block; width:30%;',
        'disabled' => 'disabled'
    );
    echo form_input($data)."&nbsp;&nbsp;";
    $data = array(
        'name'  => 'password',
        'id'    => 'password',
        'value' => set_value('password', $db_password),
        'style' => 'width:30%;',
        'disabled' => 'disabled'
    );
    echo form_password($data)."&nbsp;&nbsp;&nbsp;";
    
    $data = array(
        'name'  =>'submit_psw_profile',
        'id'    =>'submit_psw_profile',
        'value' => $submit_psw_button,
        'style' =>'width:25%;'
        );
    echo form_submit($data).  br(2);
    ////////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////////
   
    ////////////////////////////////////////////////////////////////////////////////
    echo form_open(); 
    echo form_error('name');
    echo form_error('lastname');
    echo form_error('email');
    
    $data = array(
            'style' => 'display:inline-block;   width:30%;'
        ); 
    echo form_label($name_text, 'name_text', $data)."&nbsp;&nbsp;&nbsp;";
    $data = array(
            'style' => 'display:inline-block;   width:30%;'
        ); 
    echo form_label($lastname_text, 'lastname_text', $data)."&nbsp;&nbsp;&nbsp;";
     $data = array(
            'style' => 'display:inline-block;   width:30%;'
        ); 
    echo form_label($email_text, 'email_text', $data) .   br(2);
   
    $data = array(
        'name'  => 'name',
        'id'    => 'name',
        'value' => set_value('name', $db_name),
        'style' => 'width:30%;'
    );
    echo form_input($data)."&nbsp;&nbsp;";
    $data = array(
        'name'  => 'lastname',
        'id'    => 'lastname',
        'value' => set_value('lastname', $db_lastname),
        'style' => 'width:30%;'
    );
    echo form_input($data)."&nbsp;&nbsp;";
    $data = array(
        'name'  => 'email',
        'id'    => 'email',
        'value' => set_value('email', $db_email),
        'style' => 'width:34%;'
    );
    echo form_input($data). br(2);
    ////////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////////
    $data = array(
            'style' => 'display:inline-block;   width:18%;'
        ); 
    echo form_label($phone_text, 'phone_text', $data)."&nbsp;&nbsp;&nbsp;";
    $data = array(
            'style' => 'display:inline-block;   width:18%;'
        ); 
    echo form_label($address_text, 'address_text', $data)."&nbsp;&nbsp;&nbsp;";
    $data = array(
            'style' => 'display:inline-block;   width:18%;'
        ); 
    echo form_label($city_text, 'city_text', $data)."&nbsp;&nbsp;&nbsp;";
    $data = array(
            'style' => 'display:inline-block;   width:18%;'
        ); 
    echo form_label($postal_code_text, 'postal_code_text', $data).  br(2);
    echo form_error('phone');
    echo form_error('address');
    echo form_error('city');
    echo form_error('postal_code');
    $data = array(
        'name'  => 'phone',
        'id'    => 'phone',
        'value' => set_value('phone', $db_phone),
        'style' => 'width:18%;'
    );
    echo form_input($data)."&nbsp;&nbsp;";
    $data = array(
        'name'  => 'address',
        'id'    => 'address',
        'value' => set_value('address', $db_address),
        'style' => 'width:18%;'
    );
    echo form_input($data)."&nbsp;&nbsp;";
    $data = array(
        'name'  => 'city',
        'id'    => 'city',
        'value' => set_value('city', $db_city),
        'style' => 'width:18%;'
    );
    echo form_input($data)."&nbsp;&nbsp;";
    $data = array(
        'name'  => 'postal_code',
        'id'    => 'postal_code',
        'value' => set_value('postal_code', $db_postal_code),
        'style' => 'width:18%;'
    );
    echo form_input($data).br(2);
    ////////////////////////////////////////////////////////////////////////////////
    echo form_error('country');
    echo form_error('gender');
    $data = array(
            'style' => 'display:inline-block;   width:55%;'
        ); 
    echo form_label($country_text, 'country_text', $data);
    $data = array(
            'style' => 'display:inline-block;   width:40%;'
        ); 
    echo form_label($gender_text, 'gender_text', $data).   br(2);
    echo form_dropdown('country', $country, $db_country)."&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
    echo form_dropdown('gender', $gender, $db_gender). br(2);
    ////////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////////
    $data = array(
            'style' => 'display:inline-block;   width:30%;'
        ); 
    echo form_label($birthday_text, 'birthday_text', $data).br(2);
    echo form_error('birthday');
    $data = array(
        'name'  => 'birthday',
        'id'    => 'birthday',
        'value' => set_value('birthday', $db_birthday),
        'style' => 'width:18%;'
    );
    echo form_input($data). br(2);
    ////////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////////
    $data = array(
        'name'  =>'submit_form_profile',
        'id'    =>'submit_form_profile',
        'value' => $submit_button,
        'style' =>'width:25%;'
        );
    echo form_submit($data).  br(2);
    ////////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////////
    echo form_close();   
?>   
    <br /> <hr /> <br />
    <!-- Driver licence form -->
    <h1>VOZAČKA DOZVOLA</h1>
<?php
    echo form_open(); 
    ////////////////////////////////////////////////////////////////
    $data = array(
            'style' => 'display:inline-block; width:4%; text-align:center;'
        ); 
    echo form_label($a1_text, 'A1_text', $data). "&nbsp;&nbsp;&nbsp;";
    $data = array(
            'style' => 'display:inline-block; width:4%; text-align:center;'
        ); 
    echo form_label($a2_text, 'a2_text', $data). "&nbsp;";
    $data = array(
            'style' => 'display:inline-block; width:4%; text-align:center;'
        ); 
    echo form_label($a_text, 'a_text', $data). "&nbsp;&nbsp;";
    $data = array(
            'style' => 'display:inline-block; width:4%; text-align:center;'
        ); 
    echo form_label($b_text, 'b_text', $data). "&nbsp;&nbsp;";
    $data = array(
            'style' => 'display:inline-block; width:4%; text-align:center;'
        ); 
    echo form_label($be_text, 'be_text', $data). "&nbsp;&nbsp;";
    $data = array(
            'style' => 'display:inline-block; width:4%; text-align:center;'
        ); 
    echo form_label($ci_text, 'ci_text', $data). "&nbsp;&nbsp;";
    $data = array(
            'style' => 'display:inline-block; width:4%; text-align:center;'
        ); 
    echo form_label($cie_text, 'cie_text', $data). "&nbsp;";
    $data = array(
            'style' => 'display:inline-block; width:4%; text-align:center;'
        ); 
    echo form_label($c_text, 'c_text', $data). "&nbsp;&nbsp;";
    $data = array(
            'style' => 'display:inline-block; width:4%; text-align:center;'
        ); 
    echo form_label($ce_text, 'ce_text', $data). "&nbsp;&nbsp;";
    $data = array(
            'style' => 'display:inline-block; width:4%; text-align:center;'
        ); 
    echo form_label($d1_text, 'd1_text', $data). "&nbsp;";
    $data = array(
            'style' => 'display:inline-block; width:4%; text-align:center;'
        ); 
    echo form_label($d1e_text, 'd1e_text', $data). "&nbsp;&nbsp;";
    $data = array(
            'style' => 'display:inline-block; width:4%; text-align:center;'
        ); 
    echo form_label($d_text, 'd_text', $data). "&nbsp;&nbsp;";
    $data = array(
            'style' => 'display:inline-block; width:4%; text-align:center;'
        ); 
    echo form_label($de_text, 'de_text', $data). "&nbsp;&nbsp;";
    $data = array(
            'style' => 'display:inline-block; width:4%; text-align:center;'
        ); 
    echo form_label($f_text, 'f_text', $data). "&nbsp;";
    $data = array(
            'style' => 'display:inline-block; width:4%; text-align:center;'
        ); 
    echo form_label($g_text, 'g_text', $data). "&nbsp;&nbsp;";
    $data = array(
            'style' => 'display:inline-block; width:4%; text-align:center;'
        ); 
    echo form_label($h_text, 'h_text', $data). "&nbsp;&nbsp;";
    $data = array(
            'style' => 'display:inline-block; width:4%; text-align:center;'
        );
    echo form_label($am_text, 'am_text', $data).  br(2);
    
    //
    $data = array(
        'name'        => 'a1',
        'id'          => 'a1',
        'value'       => 'accept',
        'checked'     => $db_a1,
        'style'       => 'width:4%',
    );
    echo form_checkbox($data);
    $data = array(
        'name'        => 'a2',
        'id'          => 'a2',
        'value'       => 'accept',
        'checked'     => $db_a2,
        'style'       => 'width:4%',
    );
    echo form_checkbox($data);
    $data = array(
        'name'        => 'a',
        'id'          => 'a',
        'value'       => 'accept',
        'checked'     => $db_a,
        'style'       => 'width:4%',
    );
    echo form_checkbox($data);
    $data = array(
        'name'        => 'b',
        'id'          => 'b',
        'value'       => 'accept',
        'checked'     => $db_b,
        'style'       => 'width:4%',
    );
    echo form_checkbox($data);
    $data = array(
        'name'        => 'be',
        'id'          => 'be',
        'value'       => 'accept',
        'checked'     => $db_be,
        'style'       => 'width:4%',
    );
    echo form_checkbox($data);
    $data = array(
        'name'        => 'ci',
        'id'          => 'ci',
        'value'       => 'accept',
        'checked'     => $db_ci,
        'style'       => 'width:4%',
    );
    echo form_checkbox($data);
    $data = array(
        'name'        => 'cie',
        'id'          => 'cie',
        'value'       => 'accept',
        'checked'     => $db_cie,
        'style'       => 'width:4%',
    );
    echo form_checkbox($data);
    $data = array(
        'name'        => 'c',
        'id'          => 'c',
        'value'       => 'accept',
        'checked'     => $db_c,
        'style'       => 'width:4%',
    );
    echo form_checkbox($data);
    $data = array(
        'name'        => 'ce',
        'id'          => 'ce',
        'value'       => 'accept',
        'checked'     => $db_ce,
        'style'       => 'width:4%',
    );
    echo form_checkbox($data);
    $data = array(
        'name'        => 'd1',
        'id'          => 'd1',
        'value'       => 'accept',
        'checked'     => $db_d1,
        'style'       => 'width:4%',
    );
    echo form_checkbox($data);
    $data = array(
        'name'        => 'd1e',
        'id'          => 'd1e',
        'value'       => 'accept',
        'checked'     => $db_d1e,
        'style'       => 'width:4%',
    );
    echo form_checkbox($data);
    $data = array(
        'name'        => 'd',
        'id'          => 'd',
        'value'       => 'accept',
        'checked'     => $db_d,
        'style'       => 'width:4%',
    );
    echo form_checkbox($data);
    $data = array(
        'name'        => 'de',
        'id'          => 'de',
        'value'       => 'accept',
        'checked'     => $db_de,
        'style'       => 'width:4%',
    );
    echo form_checkbox($data);
    $data = array(
        'name'        => 'f',
        'id'          => 'f',
        'value'       => 'accept',
        'checked'     => $db_f,
        'style'       => 'width:4%',
    );
    echo form_checkbox($data);
    $data = array(
        'name'        => 'g',
        'id'          => 'g',
        'value'       => 'accept',
        'checked'     => $db_g,
        'style'       => 'width:4%',
    );
    echo form_checkbox($data);
    $data = array(
        'name'        => 'h',
        'id'          => 'h',
        'value'       => 'accept',
        'checked'     => $db_h,
        'style'       => 'width:4%',
    );
    echo form_checkbox($data);
    $data = array(
        'name'        => 'am',
        'id'          => 'am',
        'value'       => 'accept',
        'checked'     => $db_am,
        'style'       => 'width:4%',
    );
    echo form_checkbox($data). br(2);
    ////////////////////////////////////////////////////////////////
    $data = array(
        'name'  =>'submit_form_driver_licence',
        'id'    =>'submit_form_driver_licence',
        'value' => $submit_button,
        'style' =>'width:25%;'
        );
    echo form_submit($data).  br(2);
    ////////////////////////////////////////////////////////////////
    echo form_close();   
?>
    
    <br /> <hr /> <br />
    <!-- HAK form -->
    <h1>HAK</h1>
<?php
    echo form_open();
    ////////////////////////////////////////////////////////////////
    $data = array(
            'style' => 'display:inline-block; width:30%;'
        ); 
    echo form_label($number_text, 'number_text', $data)."&nbsp;&nbsp;&nbsp;";
    $data = array(
            'style' => 'display:inline-block; width:30%;'
        ); 
    echo form_label($type_text, 'type_text', $data)."&nbsp;&nbsp;&nbsp;";
    $data = array(
            'style' => 'display:inline-block; width:35%;'
        ); 
    echo form_label($valid_thru_text, 'valid_thru_text', $data).br(2);
    echo form_error('number');
    echo form_error('type');
    echo form_error('valid_thru');
    $data = array(
        'name'  => 'number',
        'id'    => 'number',
        'value' => set_value('number', $db_number),
        'style' => 'width:30%;'
    );
    echo form_input($data)."&nbsp;&nbsp;";
    $data = array(
        'name'  => 'type',
        'id'    => 'type',
        'value' => set_value('type', $db_type),
        'style' => 'width:30%;'
    );
    echo form_input($data)."&nbsp;&nbsp;";
    $data = array(
        'name'  => 'valid_thru',
        'id'    => 'valid_thru',
        'value' => set_value('valid_thru', $db_valid_thru),
        'style' => 'width:25%;'
    );
    echo form_input($data). br(2);
    ////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////
    $data = array(
        'name'  =>'submit_form_hak',
        'id'    =>'submit_form_hak',
        'value' => $submit_button,
        'style' =>'width:25%;'
        );
    echo form_submit($data).  br(2);
    ////////////////////////////////////////////////////////////////
    echo form_close();   
?>
    
<?php
    } //- End of else
    
    if($show_message === TRUE)
    {
        echo "<input type='text' id='show_message' name='show_message' value='".$show_message_val."' style='display: none;'</input>";
    }
?>     
</div>

<!-- Popup dialog change psw -->
<div id="dialog-change-psw" title="Promijena lozinke">
    <form id="form_dlg_change_psw" name="form_dlg_change_psw" method="post">
        <label id="old_passwordLabel"  name="old_passwordLabel" class="label-dialog">TRENUTNA LOZINKA</label>      <input id="old_password"  name="old_password" type="password" class="input-dialog"></input></br>
        <label id="new_passwordLabel"  name="new_passwordLabel" class="label-dialog">NOVA LOZINKA</label>      <input id="new_password"  name="new_password" type="password" class="input-dialog"></input></br>
        <label id="repeat_passwordLabel"   name="repeat_passwordLabel"  class="label-dialog">PONOVI LOZINKU</label>   <input id="repeat_password"  name="repeat_password" type="password" class="input-dialog"></input></br>
        <input style="display: none;" id="btn_dlg_change_psw" type="button" name="btn_dlg_change_psw"/>
    </form>
</div>

