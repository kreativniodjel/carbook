<!-- Vehicles licences -->
<div id="vehicles_licences_form" style="width:700px; margin:100px auto; border-radius: 5px;border:1px solid #909090; padding:20px">
<?php 
    if(isset($error))
    {
        echo $error;
    }
    else
    {
?>
    <!-- Vehicles form -->
    <h1>VOZILO</h1>
<?php
    $disabled = FALSE;
    if($enable_changes === FALSE)
        $disabled = TRUE;
    
    echo form_open(); 
    ////////////////////////////////////////////////////////////////
    echo form_error('name');
    echo form_error('licence_plate');
    $data = array(
            'style' => 'display:inline-block; width:45%;'
        ); 
    echo form_label($name_text, 'name_text', $data)."&nbsp;&nbsp;";
    $data = array(
            'style' => 'display:inline-block; width:45%;'
        ); 
    echo form_label($licence_plate_text, 'licence_plate_text', $data).br(2);
    $data = array(
        'name'  => 'name',
        'id'    => 'name',
        'value' => set_value('name', $db_name),
        'style' => 'display:inline-block; width:45%;'
    ); if($disabled === TRUE)$data['disabled'] = 'disabled';
    
    echo form_input($data)."&nbsp;&nbsp;";
    $data = array(
        'name'  => 'licence_plate',
        'id'    => 'licence_plate',
        'value' => set_value('licence_plate', $db_licence_plate),
        'style' => 'width:45%;  disabled="disabled"'
    );if($disabled === TRUE)$data['disabled'] = 'disabled';
    echo form_input($data). br(2);
    ////////////////////////////////////////////////////////////////
    $data = array(
        'name'  =>'submit_form_vehicles',
        'id'    =>'submit_form_vehicles',
        'value' => $submit_button,
        'style' =>'width:25%;'
        );if($disabled === TRUE)$data['disabled'] = 'disabled';
    echo form_submit($data).  br(2);
    ////////////////////////////////////////////////////////////////
    echo form_close();   
?>
    <br /> <hr /> <br />
    <!-- Vehicle licence form -->
    <h1>KNJIŽICA VOZILA</h1>
<?php
    echo form_open();
    ////////////////////////////////////////////////////////////////
    $data = array(
            'style' => 'display:inline-block; width:22%;'
        ); 
    echo form_label($class_text, 'class_text', $data)."&nbsp;&nbsp;&nbsp;";
    $data = array(
            'style' => 'display:inline-block; width:22%;'
        ); 
    echo form_label($brand_text, 'brand_text', $data)."&nbsp;&nbsp;&nbsp;";
    $data = array(
            'style' => 'display:inline-block; width:22%;'
        ); 
    echo form_label($type_text, 'type_text', $data)."&nbsp;&nbsp;&nbsp;";
    $data = array(
            'style' => 'display:inline-block; width:22%;'
        ); 
    echo form_label($model_text, 'model_text', $data).  br(2);
    echo form_error('class');
    echo form_error('brand');
    echo form_error('type');
    echo form_error('model');
    $data = array(
        'name'  => 'class',
        'id'    => 'class',
        'value' => set_value('class', $db_class),
        'style' => 'width:22%;'
    );if($disabled === TRUE)$data['disabled'] = 'disabled';
    echo form_input($data)."&nbsp;&nbsp;";
    $data = array(
        'name'  => 'brand',
        'id'    => 'brand',
        'value' => set_value('brand', $db_brand),
        'style' => 'width:22%;'
    );if($disabled === TRUE)$data['disabled'] = 'disabled';
    echo form_input($data)."&nbsp;&nbsp;";
    $data = array(
        'name'  => 'type',
        'id'    => 'type',
        'value' => set_value('type', $db_type),
        'style' => 'width:22%;'
    );if($disabled === TRUE)$data['disabled'] = 'disabled';
    echo form_input($data)."&nbsp;&nbsp;";
    $data = array(
        'name'  => 'model',
        'id'    => 'model',
        'value' => set_value('model', $db_model),
        'style' => 'width:22%;'
    );if($disabled === TRUE)$data['disabled'] = 'disabled';
    echo form_input($data). br(2);
    ////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////
    $data = array(
            'style' => 'display:inline-block;   width:30%;'
        ); 
    echo form_label($color_text, 'color_text', $data)."&nbsp;&nbsp;&nbsp;";
    $data = array(
            'style' => 'display:inline-block;   width:30%;'
        ); 
    echo form_label($num_of_chassis_text, 'num_of_chassis_text', $data)."&nbsp;&nbsp;&nbsp;";
     $data = array(
            'style' => 'display:inline-block;   width:30%;'
        ); 
    echo form_label($body_shape_text, 'body_shape_text', $data) .   br(2);
    echo form_error('color');
    echo form_error('num_of_chassis');
    echo form_error('body_shape');
    $data = array(
        'name'  => 'color',
        'id'    => 'color',
        'value' => set_value('color', $db_color),
        'style' => 'width:30%;'
    );if($disabled === TRUE)$data['disabled'] = 'disabled';
    echo form_input($data)."&nbsp;&nbsp;";
    $data = array(
        'name'  => 'num_of_chassis',
        'id'    => 'num_of_chassis',
        'value' => set_value('num_of_chassis', $db_num_of_chassis),
        'style' => 'width:30%;'
    );if($disabled === TRUE)$data['disabled'] = 'disabled';
    echo form_input($data)."&nbsp;&nbsp;";
    $data = array(
        'name'  => 'body_shape',
        'id'    => 'body_shape',
        'value' => set_value('body_shape', $db_body_shape),
        'style' => 'width:30%;'
    );if($disabled === TRUE)$data['disabled'] = 'disabled';
    echo form_input($data). br(2);
    ////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////
    $data = array(
            'style' => 'display:inline-block; width:47%;'
        ); 
    echo form_label($manuf_country_text, 'manuf_country_text', $data)."&nbsp;&nbsp;";
    $data = array(
            'style' => 'display:inline-block; width:45%;'
        ); 
    echo form_label($date_first_reg_text, 'date_first_reg_text', $data).    br(2);
    echo form_error('manuf_country');
    echo form_error('manuf_year');
    echo form_error('date_first_reg');
    echo form_error('year_first_reg');
    $data = array(
        'name'  => 'manuf_country',
        'id'    => 'manuf_country',
        'value' => set_value('manuf_country', $db_manuf_country),
        'style' => 'width:34%;'
    );if($disabled === TRUE)$data['disabled'] = 'disabled';
    echo form_input($data)."&nbsp;&nbsp;";
    $data = array(
        'name'  => 'manuf_year',
        'id'    => 'manuf_year',
        'value' => set_value('manuf_year', $db_manuf_year),
        'style' => 'width:10%;'
    );if($disabled === TRUE)$data['disabled'] = 'disabled';
    echo form_input($data)."&nbsp;&nbsp;";
    $data = array(
        'name'  => 'date_first_reg',
        'id'    => 'date_first_reg',
        'value' => set_value('date_first_reg', $db_date_first_reg),
        'style' => 'width:34%;'
    );if($disabled === TRUE)$data['disabled'] = 'disabled';
    echo form_input($data)."&nbsp;&nbsp;";
    $data = array(
        'name'  => 'year_first_reg',
        'id'    => 'year_first_reg',
        'value' => set_value('year_first_reg', $db_year_first_reg),
        'style' => 'width:10%;'
    );if($disabled === TRUE)$data['disabled'] = 'disabled';
    echo form_input($data). br(2);
    ////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////
    $data = array(
            'style' => 'display:inline-block; width:30%;'
        ); 
    echo form_label($seat_places_text, 'seat_places_text', $data)."&nbsp;&nbsp;&nbsp;";
    $data = array(
            'style' => 'display:inline-block; width:30%;'
        ); 
    echo form_label($carrying_capacity_text, 'carrying_capacity_text', $data)."&nbsp;&nbsp;&nbsp;";
    $data = array(
            'style' => 'display:inline-block; width:30%;'
        ); 
    echo form_label($mass_text, 'mass_text', $data) . br(2);
    echo form_error('seat_places');
    echo form_error('carrying_capacity');
    echo form_error('mass');
    $data = array(
        'name'  => 'seat_places',
        'id'    => 'seat_places',
        'value' => set_value('seat_places', $db_seat_places),
        'style' => 'width:30%;'
    );if($disabled === TRUE)$data['disabled'] = 'disabled';
    echo form_input($data)."&nbsp;&nbsp;";
    $data = array(
        'name'  => 'carrying_capacity',
        'id'    => 'carrying_capacity',
        'value' => set_value('carrying_capacity', $db_carrying_capacity),
        'style' => 'width:30%;'
    );if($disabled === TRUE)$data['disabled'] = 'disabled';
    echo form_input($data)."&nbsp;&nbsp;";
    $data = array(
        'name'  => 'mass',
        'id'    => 'mass',
        'value' => set_value('mass', $db_mass),
        'style' => 'width:30%;'
    );if($disabled === TRUE)$data['disabled'] = 'disabled';
    echo form_input($data). br(2);
    ////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////
    $data = array(
            'style' => 'display:inline-block; width:22%;'
        ); 
    echo form_label($max_speed_text, 'max_speed_text', $data)."&nbsp;&nbsp;";
    $data = array(
            'style' => 'display:inline-block; width:22%;'
        ); 
    echo form_label($engine_type_text, 'engine_type_text', $data)."&nbsp;&nbsp;&nbsp;&nbsp;";
    $data = array(
            'style' => 'display:inline-block; width:22%'
        ); 
    echo form_label($power_text, 'power_text', $data)."&nbsp;&nbsp;&nbsp;&nbsp;";
    $data = array(
            'style' => 'display:inline-block; width:22%'
        ); 
    echo form_label($cubic_capacity_text, 'cubic_capacity_text', $data).    br(2);
    echo form_error('max_speed');
    echo form_error('engine_type');
    echo form_error('power');
    echo form_error('cubic_capacity');
    $data = array(
        'name'  => 'max_speed',
        'id'    => 'max_speed',
        'value' => set_value('max_speed', $db_max_speed),
        'style' => 'width:22%;'
    );if($disabled === TRUE)$data['disabled'] = 'disabled';
    echo form_input($data)."&nbsp;&nbsp;";
    $data = array(
        'name'  => 'engine_type',
        'id'    => 'engine_type',
        'value' => set_value('engine_type', $db_engine_type),
        'style' => 'width:22%;'
    );if($disabled === TRUE)$data['disabled'] = 'disabled';
    echo form_input($data)."&nbsp;&nbsp;";
    $data = array(
        'name'  => 'power',
        'id'    => 'power',
        'value' => set_value('power', $db_power),
        'style' => 'width:22%;'
    );if($disabled === TRUE)$data['disabled'] = 'disabled';
    echo form_input($data)."&nbsp;&nbsp;";
    $data = array(
        'name'  => 'cubic_capacity',
        'id'    => 'cubic_capacity',
        'value' => set_value('cubic_capacity', $db_cubic_capacity),
        'style' => 'width:22%;'
    );if($disabled === TRUE)$data['disabled'] = 'disabled';
    echo form_input($data). br(2);
    ////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////
    $data = array(
            'style' => 'display:inline-block; width:22%;'
        ); 
    echo form_label($length_text, 'length_text', $data)."&nbsp;&nbsp;";
    $data = array(
            'style' => 'display:inline-block; width:22%;'
        ); 
    echo form_label($width_text, 'width_text', $data)."&nbsp;&nbsp;&nbsp;&nbsp;";
    $data = array(
            'style' => 'display:inline-block; width:22%;'
        ); 
    echo form_label($height_text, 'height_text', $data)."&nbsp;&nbsp;&nbsp;&nbsp;";
    $data = array(
            'style' => 'display:inline-block; width:22%;'
        ); 
    echo form_label($lowest_point_text, 'lowest_point_text', $data).    br(2);
    echo form_error('length');
    echo form_error('width');
    echo form_error('height');
    echo form_error('lowest_point');
    $data = array(
        'name'  => 'length',
        'id'    => 'length',
        'value' => set_value('length', $db_length),
        'style' => 'width:22%;'
    );if($disabled === TRUE)$data['disabled'] = 'disabled';
    echo form_input($data)."&nbsp;&nbsp;";
    $data = array(
        'name'  => 'width',
        'id'    => 'width',
        'value' => set_value('width', $db_width),
        'style' => 'width:22%;'
    );if($disabled === TRUE)$data['disabled'] = 'disabled';
    echo form_input($data)."&nbsp;&nbsp;";
    $data = array(
        'name'  => 'height',
        'id'    => 'height',
        'value' => set_value('height', $db_height),
        'style' => 'width:22%;'
    );if($disabled === TRUE)$data['disabled'] = 'disabled';
    echo form_input($data)."&nbsp;&nbsp;";
    $data = array(
        'name'  => 'lowest_point',
        'id'    => 'lowest_point',
        'value' => set_value('lowest_point', $db_lowest_point),
        'style' => 'width:22%;'
    );if($disabled === TRUE)$data['disabled'] = 'disabled';
    echo form_input($data). br(2);
    ////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////
    $data = array(
            'style' => 'display:inline-block; width:22%;'
        ); 
    echo form_label($num_of_wheels_text, 'num_of_wheels_text', $data)."&nbsp;&nbsp;";
    $data = array(
            'style' => 'display:inline-block; width:22%;'
        ); 
    echo form_label($tyres_front_text, 'tyres_front_text', $data)."&nbsp;&nbsp;&nbsp;&nbsp;";
    $data = array(
            'style' => 'display:inline-block; width:22%;'
        ); 
    echo form_label($tyres_back_text, 'tyres_back_text', $data)."&nbsp;&nbsp;&nbsp;&nbsp;";
    $data = array(
            'style' => 'display:inline-block; width:22%;'
        ); 
    echo form_label($brake_type_text, 'brake_type_text', $data).    br(2);
    echo form_error('num_of_wheels');
    echo form_error('tyres_front');
    echo form_error('tyres_back');
    echo form_error('brake_type');
    $data = array(
        'name'  => 'num_of_wheels',
        'id'    => 'num_of_wheels',
        'value' => set_value('num_of_wheels', $db_num_of_wheels),
        'style' => 'width:22%;'
    );if($disabled === TRUE)$data['disabled'] = 'disabled';
    echo form_input($data)."&nbsp;&nbsp;";
    $data = array(
        'name'  => 'tyres_front',
        'id'    => 'tyres_front',
        'value' => set_value('tyres_front', $db_tyres_front),
        'style' => 'width:22%;'
    );if($disabled === TRUE)$data['disabled'] = 'disabled';
    echo form_input($data)."&nbsp;&nbsp;";
    $data = array(
        'name'  => 'tyres_back',
        'id'    => 'tyres_back',
        'value' => set_value('tyres_back', $db_tyres_back),
        'style' => 'width:22%;'
    );if($disabled === TRUE)$data['disabled'] = 'disabled';
    echo form_input($data)."&nbsp;&nbsp;";
    $data = array(
        'name'  => 'brake_type',
        'id'    => 'brake_type',
        'value' => set_value('brake_type', $db_brake_type),
        'style' => 'width:22%;'
    );if($disabled === TRUE)$data['disabled'] = 'disabled';
    echo form_input($data). br(2);
    ////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////
    $data = array(
            'style' => 'display:inline-block; width:22%;'
        ); 
    echo form_label($num_of_axles_text, 'num_of_axles_text', $data). "&nbsp;&nbsp;";
    $data = array(
            'style' => 'display:inline-block; width:22%;;'
        ); 
    echo form_label($driving_axles_text, 'driving_axles_text', $data).  br(2);
    echo form_error('num_of_axles');
    echo form_error('driving_axles');
    $data = array(
        'name'  => 'num_of_axles',
        'id'    => 'num_of_axles',
        'value' => set_value('num_of_axles', $db_num_of_axles),
        'style' => 'width:22%;'
    );if($disabled === TRUE)$data['disabled'] = 'disabled';
    echo form_input($data)."&nbsp;&nbsp;";
    $data = array(
        'name'  => 'driving_axles',
        'id'    => 'driving_axles',
        'value' => set_value('driving_axles', $db_driving_axles),
        'style' => 'width:22%;'
    );if($disabled === TRUE)$data['disabled'] = 'disabled';
    echo form_input($data). br(2);
    ////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////
    $data = array(
            'style' => 'display:inline-block; width:22%; text-align:center;'
        ); 
    echo form_label($hook_text, 'hook_text', $data). "&nbsp;&nbsp;";
    $data = array(
            'style' => 'display:inline-block; width:22%; text-align:center;'
        ); 
    echo form_label($anchor_text, 'anchor_text', $data).    br(2);
    $data = array(
        'name'        => 'hook',
        'id'          => 'hook',
        'value'       => 'accept',
        'checked'     => set_value('hook', $db_hook),
        'style'       => 'width:22%',
    );if($disabled === TRUE)$data['disabled'] = 'disabled';
    echo form_checkbox($data);
    $data = array(
        'name'        => 'anchor',
        'id'          => 'anchor',
        'value'       => 'accept',
        'checked'     => set_value('anchor', $db_anchor),
        'style'       => 'width:22%',
    );if($disabled === TRUE)$data['disabled'] = 'disabled';
    echo form_checkbox($data). br(2);
    ////////////////////////////////////////////////////////////////
    $data = array(
        'name'  =>'submit_form_vehicles_licences',
        'id'    =>'submit_form_vehicles_licences',
        'value' => $submit_button,
        'style' =>'width:25%;'
        );if($disabled === TRUE)$data['disabled'] = 'disabled';
    echo form_submit($data).  br(2);
    ////////////////////////////////////////////////////////////////
    echo form_close();   
?>
    <br /> <hr /> <br />
    <!-- Gas form -->
    <h1>GORIVO</h1>
<?php
    echo form_open(); 
    ////////////////////////////////////////////////////////////////
    $data = array(
            'style' => 'display:inline-block; width:22%; text-align:center;'
        ); 
    echo form_label($gasoline_text, 'gasoline_text', $data). "&nbsp;&nbsp;";
    $data = array(
            'style' => 'display:inline-block; width:22%; text-align:center;'
        ); 
    echo form_label($diesel_text, 'diesel_text', $data). "&nbsp;&nbsp;&nbsp;&nbsp;";
    $data = array(
            'style' => 'display:inline-block; width:22%; text-align:center;'
        ); 
    echo form_label($gas_text, 'gas_text', $data). "&nbsp;&nbsp;&nbsp;&nbsp;";
    $data = array(
            'style' => 'display:inline-block; width:22%; text-align:center;'
        ); 
    echo form_label($electricity_text, 'electricity_text', $data).  br(2);
    $data = array(
        'name'        => 'gasoline',
        'id'          => 'gasoline',
        'value'       => 'accept',
        'checked'     => $db_gasoline,
        'style'       => 'width:22%',
    );if($disabled === TRUE)$data['disabled'] = 'disabled';
    echo form_checkbox($data);
    $data = array(
        'name'        => 'diesel',
        'id'          => 'diesel',
        'value'       => 'accept',
        'checked'     => $db_diesel,
        'style'       => 'width:22%',
    );if($disabled === TRUE)$data['disabled'] = 'disabled';
    echo form_checkbox($data)."&nbsp;&nbsp;";
    $data = array(
        'name'        => 'gas',
        'id'          => 'gas',
        'value'       => 'accept',
        'checked'     => $db_gas,
        'style'       => 'width:22%',
    );if($disabled === TRUE)$data['disabled'] = 'disabled';
    echo form_checkbox($data)."&nbsp;&nbsp;";
    $data = array(
        'name'        => 'electricity',
        'id'          => 'electricity',
        'value'       => 'accept',
        'checked'     => $db_electricity,
        'style'       => 'width:22%',
    );if($disabled === TRUE)$data['disabled'] = 'disabled';
    echo form_checkbox($data). br(2);
    ////////////////////////////////////////////////////////////////
    $data = array(
        'name'  =>'submit_form_gas',
        'id'    =>'submit_form_gas',
        'value' => $submit_button,
        'style' =>'width:25%;'
        );if($disabled === TRUE)$data['disabled'] = 'disabled';
    echo form_submit($data).  br(2);
    ////////////////////////////////////////////////////////////////
    echo form_close();   
?>
    <br /> <hr /> <br />
    <!-- Lights form -->
    <h1>ŽARULJE</h1>
<?php
    echo form_open();  
    ////////////////////////////////////////////////////////////////
    $data = array(
            'style' => 'display:inline-block; width:30%;'
        ); 
    echo form_label($short_lights_text, 'short_lights_text', $data)."&nbsp;&nbsp;&nbsp;";
    $data = array(
            'style' => 'display:inline-block; width:30%;'
        ); 
    echo form_label($high_lights_text, 'high_lights_text', $data)."&nbsp;&nbsp;&nbsp;";
    $data = array(
            'style' => 'display:inline-block; width:30%;'
        ); 
    echo form_label($stop_lights_text, 'stop_lights_text', $data).  br(2);
    echo form_error('short_lights');
    echo form_error('high_lights');
    echo form_error('stop_lights');
    $data = array(
        'name'  => 'short_lights',
        'id'    => 'short_lights',
        'value' => set_value('short_lights', $db_short_lights),
        'style' => 'width:30%;'
    );if($disabled === TRUE)$data['disabled'] = 'disabled';
    echo form_input($data)."&nbsp;&nbsp;";
    $data = array(
        'name'  => 'high_lights',
        'id'    => 'high_lights',
        'value' => set_value('high_lights', $db_high_lights),
        'style' => 'width:30%;'
    );if($disabled === TRUE)$data['disabled'] = 'disabled';
    echo form_input($data)."&nbsp;&nbsp;";
    $data = array(
        'name'  => 'stop_lights',
        'id'    => 'stop_lights',
        'value' => set_value('stop_lights', $db_stop_lights),
        'style' => 'width:30%;'
    );if($disabled === TRUE)$data['disabled'] = 'disabled';
    echo form_input($data). br(2);
    ////////////////////////////////////////////////////////////////
    $data = array(
        'name'  =>'submit_form_lights',
        'id'    =>'submit_form_lights',
        'value' => $submit_button,
        'style' =>'width:25%;'
        );if($disabled === TRUE)$data['disabled'] = 'disabled';
    echo form_submit($data).  br(2);
    ////////////////////////////////////////////////////////////////
    echo form_close();   
?>
    <br /> <hr /> <br />
    <!-- Gallery form -->
    <h1>GALERIJA</h1>
<?php
    // Gallery start
    echo "<div id='gallery' class='gallery_container'>";
     
    $gallery    = array();
    $i          = 0;
    
    //- images
    foreach($pictures as $pic)
    {
        $gallery[$i]['id']      = $i;
        $gallery[$i]['path']    = base_url()."gallery/".$pic['path'];
        $gallery[$i]['name']    = $pic['name'];
        $gallery[$i]['desc']    = $pic['description'];
        
        echo    "<div id='image".$pic['idpictures']."' class='gallery_part'>".
                    "<div id='imagep1' class='gallery_img'>".
                        "<img id='image_click' data-id='".$gallery[$i]['id']."' data-name='".$gallery[$i]['name']."' data-desc='".$gallery[$i]['desc']."' class='image_click' src='".base_url()."gallery/".$pic['path']."' alt='".$pic['name']."'></img>".
                    "</div>".
                    "<div id='imagep2' class='gallery_img_btns' data-pciid='".$pic['idpictures']."' data-picname='".$pic['name']."' data-picdes='".$pic['description']."'>";
                            if($disabled === FALSE)
                            {
                                echo "<form id='picture_form' method='post' action='".base_url()."index.php/vehicles_licences/manipulate_picture/".$pic['idpictures']."'>".
                                    "<input id='pic_path' name='pic_path'   type='text' value='".$pic['path']."' class='hide_element'></input>".
                                        "<input id='btn_pic_thmb'   name='btn_pic_thmb'     type='submit' value='t'></input>".
                                        "<input id='btn_pic_delete' name='btn_pic_delete'   type='submit' value='x'></input>".
                                "</form>".        
                                    "<input id='btn_pic_edit'   name='btn_pic_edit'  class='btn_pic_edit'    type='button' value='e'></input>";
                            }
                   echo "</div>".
                    "<div id='imagep3' class='gallery_desc'>".
                        "<label id='imagedesc' name='imagedesc'>".$pic['description']."</label>".
                    "</div>".
                "</div>";
        
        $i++;
    }
    
    //- always show add new image at the end of gallery
    if($disabled === FALSE)
    {
        echo    "<div id='new_image' class='gallery_part'>".
                    "<div id='imagep1' class='gallery_img'>".
                        "<img src='".base_url()."/gallery/new.png'></img>".
                    "</div>".
                    "<div id='imagep2' class='gallery_img_btns'>".
                    "</div>".
                    "<div id='imagep3' class='gallery_desc'>".
                        "<label id='imagedesc' name='imagedesc'></label>".
                    "</div>".
                "</div>";
    }
    // End of Gallery 
    echo "</div>"; 
?>   
  <form id="file_form" enctype="multipart/form-data" action="vehicles_licences/add_picture_to_gallery" method="POST">  
    <input class="hide-element" id="file_add_name"  name="file_add_name" type="text" style="display:none"/> 
    <input class="hide-element" id="file_add_desc"  name="file_add_desc" type="text" style="display:none"/> 
    <input class="hide_element" type="file" id="file" name="file" />
    <input class="hide_element" type="submit" value="Upload File" />
  </form>  
  
  <div id="dialog_add_picture">
    <label id="titleLabel" name="titleLabel">DODAJ SLIKU</label></br>
    <hr/>
    <label id="nameLabel"  name="nameLabel" class="label-dialog">NAZIV:</label>    <input id="name_dlg_add" name="name_dlg_add" type="text" class="input-dialog"></input></br>
    <label id="descLabel"  name="descLabel" class="label-dialog">OPIS:</label>    <textarea id="desc_dlg_add" name="desc_dlg_add" rows="10" cols="10" class="input-dialog"></textarea></br>
    <hr/>
    <input id="2" type="button" OnClick="" value="POTVRDI"  class="dialog-pic-button"/>
    <input id="1" type="button" OnClick="" value="ODUSTANI" class="dialog-pic-button"/><br/> 
  </div>
    
   <div id="dialog_edit_picture">
    <form id="form_edit_picture" name="form_edit_picture" action="vehicles_licences/edit_picture" method="POST">   
        <label id="titleLabel_edit" name="titleLabel_edit">UREDI SLIKU</label></br>
        <hr/>
        <input id="picid_edit" name="picid_edit" type="text" class="hide_element"></input>
        <label id="nameLabel_edit"  name="nameLabel_edit" class="label-dialog">NAZIV:</label>    <input id="name_edit" name="name_edit" type="text" class="input-dialog"></input></br>
        <label id="descLabel_edit"  name="descLabel_edit" class="label-dialog">OPIS:</label>     <textarea id="desc_edit" name="desc_edit" rows="10" cols="10" class="input-dialog"></textarea></br>
        <hr/> 
        <input id="2" type="submit" OnClick="" value="POTVRDI"  class="dialog-pic-edit-button"/>
        <input id="1" type="submit" OnClick="" value="ODUSTANI" class="dialog-pic-edit-button"/><br/> 
     </form>
  </div>   

     <div id="dialog_gallery">
        <div class="gallery_dlg_btn_left"></div>
        <div class="gallery_dlg_img_part"><img class="dlg_gallery_image" style="width:800px; height: 500px;"></img></div>
        <div class="gallery_dlg_btn_right"></div>
        <div class="gallery_dlg_desc"><p id="par_gallery_dlg_desc"></p></div>
    </div>
    
    <?php
        // all pictures paths will be entered in hidden div!
        echo "<div id='all_gallery_data' class='hide_element'";
        foreach($gallery as $data)
        {
            echo "data-".$data['id']."='".$data['path']."' ";
            echo "data-name-".$data['id']."='".$data['name']."' ";
            echo "data-desc-".$data['id']."='".$data['desc']."' ";
        }
        echo "></div>";
    ?> 
    <?php
    } //- End of else
?>     
</div>