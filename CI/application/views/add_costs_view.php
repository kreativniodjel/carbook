<!-- Add costs form -->
<div id="add_vehicles_form" style="width:400px; margin:100px auto; border-radius: 5px;border:1px solid #909090; padding:20px">
    
    
<?php
    if($edit_mode === TRUE)
        echo "<h1>PROMJENA TROŠKA</h1>";
    else 
        echo "<h1>UNOS NOVOG TROŠKA</h1>";
    echo form_open();              
    ////////////////////////////////////////////////////////////////
    //- Date label and input field
    echo form_error('date');
    $data = array(
            'style' => 'display:inline-block; width:120px;'
        ); 
    echo form_label($date_text, 'date_text', $data);
    $data = array(
        'name'  => 'date',
        'id'    => 'date',
        'value' => set_value('date', $db_date),
        'style' => 'width:60%;'
    );
    echo form_input($data).br(2);
    ////////////////////////////////////////////////////////////////
    //- Year label and input field
   /* $data = array(
            'style' => 'width:60px;'
        ); 
    echo form_label($year_text, 'year_text', $data)."&nbsp;&nbsp;";;
    $data = array(
        'name'  => 'year',
        'id'    => 'year',
        'value' => set_value('year', $db_year),
        'style' => 'width:12%;'
    );
    echo form_input($data).  br(2);*/
    ////////////////////////////////////////////////////////////////
    //- Km label and input field
    echo form_error('km');
    $data = array(
            'style' => 'display:inline-block; width:120px;'
        ); 
    echo form_label($km_text, 'km_text', $data);
    $data = array(
        'name'  => 'km',
        'id'    => 'km',
        'value' => set_value('km', $db_km),
        'style' => 'width:60%;'
    );
    echo form_input($data).  br(2);
     ////////////////////////////////////////////////////////////////
    //- Type of cost label field and dropdown box
    $data = array(
            'style' => 'display:inline-block; width:120px;'
        ); 
    echo form_label($type_text, 'type_text', $data);
    echo form_dropdown('type_options', $type_options, $curr_type_options, "style ='width:60%'"). br(2);
    ////////////////////////////////////////////////////////////////
    //- Place of cost label and input field
    $data = array(
            'style' => 'display:inline-block; width:120px;'
        ); 
    echo form_error('place_of_cost');
    echo form_label($place_of_cost_text, 'place_of_cost_text', $data);
    $data = array(
        'name'  => 'place_of_cost',
        'id'    => 'place_of_cost',
        'value' => set_value('place_of_cost', $db_place_of_cost),
        'style' => 'width:60%;'
    );
    echo form_input($data).  br(2);    
    ////////////////////////////////////////////////////////////////
    //- Note label field and textarea
    echo form_error('note');
    $data = array(
            'style' => 'display:inline-block; vertical-align:top; width:120px;'
        ); 
    echo form_label($note_text, 'note_text', $data);
    $data = array(
        'name'  => 'note',
        'id'    => 'note',
        'value' => set_value('note', $db_note),
        'style' => 'width:60%;'
    );
    echo form_textarea($data).  br(2);      
    ////////////////////////////////////////////////////////////////
    //- Quantity label and input field
    echo form_error('quantity');
    $data = array(
            'style' => 'display:inline-block;  width:120px;'
        ); 
    echo form_label($quantity_text, 'quantity_text', $data);
    $data = array(
        'name'  => 'quantity',
        'id'    => 'quantity',
        'value' => set_value('quantity', $db_quantity),
        'style' => 'width:60%;'
    );
    echo form_input($data).  br(2);       
    ////////////////////////////////////////////////////////////////
    //- Price label and input field
    echo form_error('price');
    $data = array(
            'style' => 'display:inline-block;  width:120px;'
        ); 
    echo form_label($price_text, 'price_text', $data);
    $data = array(
        'name'  => 'price',
        'id'    => 'price',
        'value' => set_value('price', $db_price),
        'style' => 'width:60%;'
    );
    echo form_input($data).  br(2); 
    ////////////////////////////////////////////////////////////////
    //- Alarm km label and input field
    echo form_error('alarm_km');
    $data = array(
            'style' => 'display:inline-block;  width:120px;'
        ); 
    echo form_label($alarm_km_text, 'alarm_km_text', $data);
    $data = array(
        'name'  => 'alarm_km',
        'id'    => 'alarm_km',
        'value' => set_value('alarm_km', $db_alarm_km),
        'style' => 'width:60%;'
    );
    echo form_input($data).  br(2); 
    ////////////////////////////////////////////////////////////////
    //- Alarm date label and input field
    echo form_error('alarm_date');
    $data = array(
            'style' => 'display:inline-block;  width:120px;'
        ); 
    echo form_label($alarm_date_text, 'alarm_date_text', $data);
    $data = array(
        'name'  => 'alarm_date',
        'id'    => 'alarm_date',
        'value' => set_value('alarm_date', $db_alarm_date),
        'style' => 'width:60%;'
    );
    echo form_input($data).br(2); 
    ////////////////////////////////////////////////////////////////
    //- Alarm year label and input field
   /* $data = array(
            'style' => 'width:60px;'
        ); 
    echo form_label($year_text, 'alarm_year_text', $data)."&nbsp;&nbsp;";
    $data = array(
        'name'  => 'alarm_year',
        'id'    => 'alarm_year',
        'value' => set_value('alarm_year', $db_year_alarm),
        'style' => 'width:12%;'
    );
    echo form_input($data).  br(2); */
    ////////////////////////////////////////////////////////////////
    //- Costs form submit buttons
    $data = array(
        'name'  =>'submit_button',
        'id'    =>'submit_button',
        'value' => $submit_button,
        'style' =>'width:25%;'
        );
    echo form_submit($data)."&nbsp;&nbsp;&nbsp;";
    // CANCEL BUTTON 
    $data = array(
        'name'  =>'cancel_button',
        'id'    =>'cancel_button',
        'value' => $cancel_button,
        'style' =>'width:25%;'
        );
    echo form_submit($data). br(2);
    ////////////////////////////////////////////////////////////////
    echo form_close();
?>
</div>