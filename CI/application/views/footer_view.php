
		    </td> <!-- /.page-content -->

			<?php if ($this->uri->total_segments() > 0) { ?>
		    <?php if( @$show_sidebar !== FALSE ) { ?>
		    <td class="page-sidebar">
		    	<div class="banner">
		    		<img src="<?=base_url('/assets/img/banners/sidebar'.rand(1,3).'.jpg')?>">
		    	</div>
		    </td>
		    <?php } ?>
		    <?php } ?>
		    
	    </table> <!-- .page-content-wrapper -->

		<?php if ($this->uri->total_segments() > 0) { ?>
        <!-- Page footer -->
	    <div class="page-footer-wrapper">
	    	<div class="banner">
		    	<img src="<?=base_url('/assets/img/banners/footer'.rand(1,3).'.jpg')?>">
	    	</div>
	    </div>
	    <?php } ?>

		<div id='templates_placeholder' style='opacity:0'>
		    <?php
		    	$this->load->library('Templates_loader');
		    	$templates = new Templates_loader;
		    	echo $templates->load();
		    ?>
		</div>
				
	    <!-- jQuery -->
        <script src="<?=base_url('/assets/js/libs/jquery-1.8.2.min.js')?>"></script>
        <script src="<?=base_url('/assets/js/libs/jquery.ui/jquery-ui-1.9.2.custom.min.js')?>"></script>
	    <link href="<?=base_url('/assets/js/libs/jquery.ui/smoothness/jquery-ui-1.9.2.custom.min.css')?>" rel='stylesheet' type="text/css">
	    <script src="<?=base_url('/assets/js/libs/jquery.ezmark.min.js')?>"></script>
	    <script src="<?=base_url('/assets/js/libs/jquery.customSelect.min.js')?>"></script>

	    <!-- Scripts -->
        <script src="<?=base_url('/assets/js/scripts/sanjin/abs-pos-in-table-cell.js')?>"></script>
        <script src="<?=base_url('/assets/js/scripts/sanjin/get-vendor-prefix.js')?>"></script>
        <script src="<?=base_url('/assets/js/scripts/sanjin/init.js')?>"></script>
        
        <script src="<?=base_url('/assets/js/scripts/marko/general.js')?>"></script>
        <script src="<?=base_url('/assets/js/scripts/marko/gallery.js')?>"></script>
        <script src="<?=base_url('/assets/js/scripts/marko/costs_table.js')?>"></script>
        <script src="<?=base_url('/assets/js/scripts/marko/profile.js')?>"></script>
        <script src="<?=base_url('/assets/js/scripts/marko/veh_table.js')?>"></script>
        <script src="<?=base_url('/assets/js/scripts/marko/reg-popup.js')?>"></script>

	    <!-- Require JS -->
        <script src="<?=base_url('/assets/js/libs/require.js')?>" data-main="<?=base_url('/assets/js/main')?>"></script>

	</body>
</html>

<!-- End of footer_view.php -->
